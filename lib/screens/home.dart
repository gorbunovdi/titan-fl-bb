import 'package:flutter/material.dart';

import '../common.dart';
import '../mocks.dart';

class HomeScreen extends StatelessWidget {
  final token;

  HomeScreen({Key key, this.token}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: ListView(
        children: <Widget>[
          getCarousel(),
          getSections(context),
          getCategories()
        ],
      ),
      bottomNavigationBar: getNavigation(context)
    );
  }

  Widget getCategories() {
    return FutureBuilder(
      future: fetchCategories(),
      builder: (_, snapshot) {
        if (snapshot.hasData) {
          return Container(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                createHeader('Categories'),
                Column(
                  children: snapshot.data.map<Widget>((category) {
                    return Container(
                      padding: const EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(children: <Widget>[
                            Icon(Icons.star),
                            Text(category['name'], style: TextStyle(fontSize: 18.0))
                          ]),
                          Divider()
                        ],
                      )
                    );
                  }).toList(),
                ),
              ],
            ),
          );
        } else if (snapshot.hasError) {
          return Text("${snapshot.error}");
        }

        // By default, show a loading spinner
        return LinearProgressIndicator();
      },
    );
  }

  Widget getCarousel() {
    return FutureBuilder(
      future: fetchCarousel(),
      builder: (_, snapshot) {
        if (snapshot.hasData) {
          return getImage(snapshot.data[0]['imageUrl']);
        } else if (snapshot.hasError) {
          return Text("${snapshot.error}");
        }

        // By default, show a loading spinner
        return LinearProgressIndicator();
      },
    );
  }

  Widget getSections(context) {
    return FutureBuilder(
      future: fetchSections(),
      builder: (_, snapshot) {
        if (snapshot.hasData) {
          final sections = snapshot.data.where((section) => section['products'].length != 0).toList();
          
          return Container(
            padding: const EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
            child: Column(
              children: sections.map<Widget>((section) {
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      section['name'], 
                      style: TextStyle(fontWeight: FontWeight.w500, fontSize: 17.0)
                    ),
                    getHorizontalList(section['products'], sections, context)
                  ]
                );
              }).toList(),
            )
          );
        } else if (snapshot.hasError) {
          return Text("${snapshot.error}");
        }

        // By default, show a loading spinner
        return LinearProgressIndicator();
      },
    );
  }

  Future fetchSections() {
    if (mocked) {
      return new Future(() => sections);
    }
    
    return fetchByUrlWithToken(token, 'https://backend-alpha.tim.gameloftstore.com/section/language/it');
  }

  Future fetchCarousel() {
    return fetchByUrlWithToken(token, 'https://backend-alpha.tim.gameloftstore.com/carousel/languageId/it');
  }

  Future fetchCategories() {
    return fetchByUrlWithToken(token, 'https://backend-alpha.tim.gameloftstore.com/catalogue/categories');
  }
}