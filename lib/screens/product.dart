import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:connectivity/connectivity.dart';

import '../common.dart';

void downloadProduct(context) async {
  print('downloadProduct');
  var connectivityResult = await (new Connectivity().checkConnectivity());
    print(connectivityResult);
  if (connectivityResult == ConnectivityResult.mobile) {
    showAlert(context, 'Downloading... (no)');
  } else {
    showAlert(context, 'Switch to mobile data');
  }
}

class ProductScreen extends StatelessWidget {
  final product;
  final sections;

  ProductScreen({Key key, @required this.product, this.sections}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: <Widget>[
          getImage(product['assets']['splash']),
          getGameHeader(),
          getGameInfo(),
          Divider(),
          getGameActions(context),
          getScreenshots(),
          getDescription(),
          getDownloadAndPlay(context),
          getTrending(context)
        ],
      ),
      bottomNavigationBar: getNavigation(context, currentIndex: 1)
    );
  }

  Widget getTrending(context) {
    var section = sections[0];
    return Container(
      padding: const EdgeInsets.all(10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          createHeader(section['name']),
          getHorizontalList(section['products'], sections, context)
        ],
      ),
    );
  }

  Widget getDownloadAndPlay(context) {
    return Container(
      padding: const EdgeInsets.all(10.0),
      decoration: BoxDecoration(
        color: Color(0xFFDDDDDD)
      ),
      child: Row(
        children: <Widget>[
          Column(children: <Widget>[
            getProductIcon(product, width: 60.0)
          ]),
          Expanded(
            child: Container(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text('Download and Play'),
                  Text(product['name'])
                ],
              ),
            ),
          ),
          Column(children: <Widget>[
            RaisedButton(
              child: Text('Download'),
              color: Colors.blue,
              textColor: Colors.white,
              onPressed: () => downloadProduct(context),
            )
          ],)
        ],
      ),
    );
  }

  Widget getDescription() {
    final description = product.containsKey('descriptions') ? product['descriptions']['long_description'] : '';

    return Container(
      padding: const EdgeInsets.all(10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          createHeader('Description'),
          Text(
            description,
            textAlign: TextAlign.justify,
          )
       ]
      ),
    );
  }

  Widget getScreenshots() {
    final screenshotsUrls = product['assets']['screenshots'] ?? List();
  
    Widget screenshots = Container(
      height: 120.0,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: screenshotsUrls.map<Widget>((url) {
          return getScreenshotIcon(url);
        }).toList(),
      )
    );

    return Container(
      padding: const EdgeInsets.all(10.0),
      decoration: BoxDecoration(
        color: Color(0xFFDDDDDD)
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          createHeader('Screenshots'),
          screenshots
       ]
      ),
    );
  }

  Widget getScreenshotIcon(url) {
    return Container(
      padding: const EdgeInsets.fromLTRB(0, 0, 10.0, 0),
      child: CachedNetworkImage(
        placeholder: CircularProgressIndicator(),
        imageUrl: url,
      ),
    );
  }

  Widget getGameActions(context) {
    return Container(
      padding: const EdgeInsets.all(10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(product['rating'] ?? '', style: TextStyle(fontWeight: FontWeight.bold),),
              Row(children: <Widget>[
                Icon(Icons.star, color: Colors.orange),
                Icon(Icons.star, color: Colors.orange),
                Icon(Icons.star, color: Colors.orange),
                Icon(Icons.star, color: Colors.grey),
              ],)
            ],
          ),
          RaisedButton(
            child: Text('Rate'),
            color: Colors.yellow,
            textColor: Colors.orange,
            onPressed: () => notImplemented(context),
          ),
          RaisedButton(
            child: Text('Download'),
            color: Colors.blue,
            textColor: Colors.white,
            onPressed: () => downloadProduct(context),
          ),
          IconButton(
            icon: Icon( Icons.favorite_border ),
            color: Colors.grey,
            onPressed: () => notImplemented(context),
          )
        ]
      ),
    );
  }

  Widget getGameInfo() {
    Widget category = Expanded(
      child: Row(
        children: <Widget>[
          Container(
            padding: const EdgeInsets.all(10.0),
            child: Icon(
              Icons.gps_fixed
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text('Category', style: TextStyle(
                fontStyle: FontStyle.italic,
                fontSize: 12
              ),),
              Text('Mocked Category'),
            ],
          ),
        ],
      )
    );

    Widget size = Container(
      padding: const EdgeInsets.all(10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text('Size', style: TextStyle(
            fontStyle: FontStyle.italic,
            fontSize: 12
          )),
          Text(product['size'])
        ],
      )
    );

    Widget age = Container(
      padding: const EdgeInsets.all(10.0),
      child: Text(product['age']),
    );


    return Container(
      padding: const EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
      child: Row(
        children: <Widget>[
          category,
          size,
          age
        ]
      ),
    );
  }

  Widget getGameHeader() {
    final description = Text(
      product.containsKey('descriptions') ? product['descriptions']['short_description'] : ''
    );
    final name = Text(product['name'], style: TextStyle(fontWeight: FontWeight.bold));
    
    return Container(
      padding: const EdgeInsets.all(10.0),
      child: Row(
        children: <Widget>[
          getProductIcon(product),
          Expanded(
            child: Container(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[ name, description ]
              ),
            )
          )
        ]
      ),
    );
  }
}