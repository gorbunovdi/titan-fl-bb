import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'screens/product.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:io';

Future fetchByUrlWithToken(token, url) async {
  print('fetchByUrlWithToken $url');
  final accessToken = token['accessToken'];
  final response = await http.get(
      url,
      headers: {
        HttpHeaders.authorizationHeader: 'Bearer $accessToken',
        HttpHeaders.userAgentHeader: 'Mozilla/5.0 (Linux; Android 8.0.0; Nexus 6P Build/OPR5.170623.007) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.98 Mobile Safari/537.36'
      }
    );

  if (response.statusCode == 200) {
    // If the call to the server was successful, parse the JSON
    return json.decode(response.body);
  } else {
    // If that call was not successful, throw an error.
    throw Exception('Failed to load token');
  }
}

Widget getImage(url) {
  final carouselImage = url;
  return CachedNetworkImage(
    placeholder: LinearProgressIndicator(),
    imageUrl: carouselImage
  );
}

Widget getHorizontalList(products, sections, context) {
  return Container(
    height: 160.0,
    child: ListView(
      scrollDirection: Axis.horizontal,
      children: products.map<Widget>((product) {
        return getGameTile(product, sections, context);
      }).toList(),
    )
  );
}

Widget getGameTile(product, sections, context) {
  return Container(
    padding: EdgeInsets.all(5.0),
    width: 120.0,
    child: Column(
      children: <Widget>[
        InkWell(
          child: getProductIcon(product),
          onTap: () => _navigateToProduct(context, product, sections)
        ),
        InkWell(
          child: Text(product['name'], overflow: TextOverflow.ellipsis, maxLines: 2),
          onTap: () => _navigateToProduct(context, product, sections)
        ),
        Text('Review Rate')
      ],
    ),
  );
}

Widget getProductIcon(product, { width: 100.0 }) {
  return CachedNetworkImage(
    placeholder: CircularProgressIndicator(),
    imageUrl: product['assets']['icon'],
    width: width,
  );
}

void _navigateToProduct(context, product, sections) {
  Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => ProductScreen(product: product, sections: sections,)),
  );
}

Widget getNavigation(context, { currentIndex = 0 }) {
  return BottomNavigationBar(
    type: BottomNavigationBarType.fixed,
    currentIndex: currentIndex,
    onTap: (_) => notImplemented(context),
    items: [
      BottomNavigationBarItem(
        icon: Icon(Icons.home),
        title: Text('Home'),
      ),
      BottomNavigationBarItem(
        icon: Icon(Icons.games),
        title: Text('Games'),
      ),
      BottomNavigationBarItem(
        icon: Icon(Icons.favorite),
        title: Text('Favorite')
      ),
      BottomNavigationBarItem(
        icon: Icon(Icons.person),
        title: Text('Profile')
      )
    ]
  );
}

Widget createHeader(String text) {
  return Text(
    text, 
    style: TextStyle(fontWeight: FontWeight.w500, fontSize: 17.0)
  );
}

void showAlert(context, text) {
  showDialog<void>(
    context: context,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text(text),
        actions: <Widget>[
          FlatButton(
            child: Text('Ok'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}

void notImplemented(context) {
  showAlert(context, 'Not implemented');
}