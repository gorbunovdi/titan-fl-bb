import 'package:flutter/material.dart';
import 'screens/home.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:io';

Future fetchToken() async {
  print('fetchToken');
  final response =
      await http.post(
        'https://auth-alpha.tim.gameloftstore.com/api/authenticate/msisdn',
        body: {
          'grantType': 'msisdn',
          'clientId': 'frontend:wrapper',
          'deviceId': '18fb4d6f-fadb-40f1-a5b4-88a84510e9de',
          'scopes': 'auth:user',
          'state': 'jhaksddhjasjhkdjhkaskj',
          'glDeviceIdentifier': 'alabalaportakala'
        },
        headers: {
          HttpHeaders.contentTypeHeader: 'application/x-www-form-urlencoded'
        }
      );

  if (response.statusCode == 200) {
    // If the call to the server was successful, parse the JSON
    return json.decode(response.body);
  } else {
    // If that call was not successful, throw an error.
    throw Exception('Failed to load token');
  }
}

void main() => runApp(TitanApp(token: fetchToken()));

class TitanApp extends StatelessWidget {
  final Future token;

  TitanApp({Key key, this.token}) : super(key: key);

  @override
  Widget build(BuildContext context) {
  // print(System.getProperty("http.agent"));
    return MaterialApp(
      title: 'Welcome to Titan',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Welcome to Titan'),
        ),
        // body: HomeScreen(),
        body: Center(
          child: FutureBuilder(
            future: token,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return HomeScreen(token: snapshot.data);
              } else if (snapshot.hasError) {
                return Text("${snapshot.error}");
              }

              // By default, show a loading spinner
              return LinearProgressIndicator();
            },
          ),
        ),
      ),
    );
  }
}