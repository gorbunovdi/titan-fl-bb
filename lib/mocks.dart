final mocked = false;

final sections = [
  {
    "name": "Trending Now",
    "type": "manual",
    "products": [
      {
        "name": "Real Football 2019",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "12+",
        "size": "N/A",
        "rating": "4.24",
        "descriptions": {
          "short_description":
              "Lotta per la gloria nella Coppa Internazionale 2018! Affronta i tuoi amici e dai il massimo!",
          "long_description":
              "Scopri il gioco di calcio più emozionante di sempre!\r\nVinci la Coppa Internazionale 2018 e diventa una leggenda!\r\nSfida giocatori da tutto il mondo!\r\nGestisci la squadra dei tuo sogni!\r\nRivivi partite leggendarie nella nuova modalità Sfida storica!\r\nMigliora le strutture del tuo club e sblocca il potenziale della tua squadra!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3452/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/3452/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/3452/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3452/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3452/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3452/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3452/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3452/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 3452,
        "html5_shortname": "",
        "version_id": 33196542,
        "package_names": ["com.gameloft.android.GAND.GloftR19F"]
      },
      {
        "name": "Asphalt Nitro",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "rating": "3.66",
        "descriptions": {
          "short_description":
              "Fai mangiare la polvere agli avversari! Divertiti con la miglior esperienza di guida per cellulari!\r\n",
          "long_description":
              "Guida lussuose auto e spingile oltre i loro limiti!\r\nSali sulle rampe ed esegui delle manovre in aria mentre fai delle acrobazie\r\nSfida gli avversari in differenti ed eccitanti modalità di gioco!\r\nLa modalità \"Guardie\" è di ritorno. Mettiti sulle tracce dei fuorilegge!\r\nAmmira gli incredibili paesaggi, realizzati con una grafica spettacolare! \r\nMettiti alla ricerca delle tante scorciatoie e taglia per primo il traguardo!\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2036/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/2036/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/2036/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2036/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2036/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2036/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2036/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2036/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 2036,
        "html5_shortname": "",
        "version_id": 33196524,
        "package_names": [
          "com.gameloft.android.FVGL.GloftANIM",
          "com.gameloft.android.FVGL.GloftDA97",
          "com.gameloft.android.FVGL.GloftDA40",
          "com.gameloft.android.GAND.GloftRF16",
          "com.gameloft.android.GloftANIM",
          "com.gameloft.android.GAND.GloftANIM",
          "com.gameloft.android.ALCH.GloftANIM"
        ]
      },
      {
        "name": "Cars - Sfide ruggenti",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "rating": "4.63",
        "descriptions": {
          "short_description":
              "Saetta McQueen e i suoi amici sono tornati per correre intorno al mondo in un torneo spettacolare!",
          "long_description":
              "Il gioco ufficiale di Cars della DisneyPixar, con tutto il divertimento dei film!\r\nCorse veloci, emozionanti e piene di adrenalina con divertenti comandi tattili!\r\nSblocca, potenzia e corri con 15 personaggi, tra cui Francesco, Holley, Cricchetto e Saetta McQueen!\r\nDivertimento infinito! Gioca in modalità Classica o affronta i boss in modalità Storia!\r\n5 capitoli, 70 eventi e varie tipologie di gioco, tra cui il minigioco dei trattori di Cricchetto!\r\nCorri su 29 piste diverse a Radiator Springs, a Londra e in Italia, con scorciatoie nascoste!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1732/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/1732/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/1732/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1732/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1732/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1732/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1732/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1732/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 1732,
        "html5_shortname": "",
        "version_id": 33196506,
        "package_names": [
          "com.gameloft.android.GAND.GloftCRSM",
          "com.gameloft.android.GloftCRSM",
          "com.gameloft.android.GAND.GloftCRPH",
          "com.gameloft.android.ALCH.GloftCRSM"
        ]
      },
      {
        "name": "Diamond Twister 2",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "",
        "size": "N/A",
        "rating": "2.67",
        "descriptions": {
          "short_description":
              "Torna a divertirti scambiando le gemme in 10 fantastici modi, con potenziamenti e una storia da star.\r\n",
          "long_description":
              "Giocabilità semplice e appassionante sia in modalità Storia o Storia infinita\r\n120 livelli ambientati in 8 luoghi da star\r\nIncontra nuovi personaggi lungo la strada mentre avanzi nella storia\r\n10 nuove meccaniche di gioco per scambiare le gemme e sbarazzarti della nebbia\r\nNuovi potenziamenti quali Spirale e Stasi utili per ottenere sempre più punti\r\nEffetti speciali grandiosi come esplosioni, tornado e altro ancora"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1215/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/1215/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/1215/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1215/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1215/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1215/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1215/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1215/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 1215,
        "html5_shortname": "",
        "version_id": 33196508,
        "package_names": [
          "com.gameloft.android.EU.GloftDIT2.ML",
          "com.gameloft.android.GAND.GloftGMHP",
          "com.gameloft.android.GAND.GloftDIT2",
          "com.gameloft.android.LATAM.GloftDIT2",
          "com.gameloft.android.GloftDIT2",
          "com.gameloft.android.SAMSUNG.GloftDIT2",
          "com.gameloft.android.LATAM.GloftX330"
        ]
      },
      {
        "name": "Disney Magic Kingdoms",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "rating": "4.53",
        "descriptions": {
          "short_description":
              "Crea il parco Disney dei tuoi sogni e condividi momenti unici con gli amatissimi personaggi Disney!",
          "long_description":
              "Attrazioni ispirate ai parchi a tema Disney straordinarie e senza tempo\r\nPersonaggi tratti da più di 90 anni di Disney\r\nUn'avvincente storia di eroi e cattivi\r\nRivivi i momenti più magici dei parchi con allegre e spettacolari parate\r\nCentinaia di missioni animate e divertenti, che danno vita al tuo regno!\r\nRaccogli oggetti a tema Disney e riporta nel regno i personaggi spariti"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2429/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/2429/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/2429/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2429/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2429/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2429/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2429/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2429/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 2429,
        "html5_shortname": "",
        "version_id": 33196518,
        "package_names": [
          "com.gameloft.android.GAND.GloftDMKP",
          "com.gameloft.android.ALCH.GloftDMKP"
        ]
      },
      {
        "name": "GT Racing 2 - Un'esperienza di guida totale",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "rating": "3.5",
        "descriptions": {
          "short_description":
              "Il famoso simulatore di corse torna con più auto e piste, e con una grafica migliorata.\r\n",
          "long_description":
              "Il seguito del più completo simulatore di corse su cellulare\r\nScegli tra 25 auto famose dell'universo di GT, tra cui Ferrari e Ford\r\n9 piste avvincenti in ambienti diversi: città, deserti, neve, eccetera \r\nUn'entusiasmante modalità Carriera ricca di eventi\r\nProva tante modalità di gioco, dalla gara classica a quella a eliminazione\r\nUn'esperienza di guida realistica, grazie al nuovo motore grafico\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1679/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/1679/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/1679/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1679/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1679/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1679/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1679/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1679/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 1679,
        "html5_shortname": "",
        "version_id": 33196520,
        "package_names": [
          "com.gameloft.android.GAND.GloftGT2M",
          "com.gameloft.android.BFLL.GloftGT2M",
          "com.gameloft.android.GloftGT2M"
        ]
      },
      {
        "name": "Gameloft Classics: Arcade",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "",
        "size": "N/A",
        "rating": "4.75",
        "descriptions": {
          "short_description":
              "Divertiti con questi 5 titoli classici Gameloft di successo, per la prima volta sugli smartphone!",
          "long_description":
              "Tutto il divertimento dei giochi originali, ora su smartphone!\r\nBLOCK BREAKER DELUXE 2 - Rompi-mattoncini con battaglie boss e altro ancora\r\nBUBBLE BASH 2 - Un coloratissimo scoppiabolle tropicale\r\nDIAMOND RUSH - Trova tesori nascosti tra rovine e templi antichi\r\nBRAIN CHALLENGE 3: IL NUOVO ALLENA-MENTE! - Allena la tua mente divertendoti\r\nMIAMI NIGHTS 2: THE CITY IS YOURS! - Trova fama, fortuna e amore"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3389/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/3389/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/3389/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3389/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3389/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3389/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3389/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3389/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 3389,
        "html5_shortname": "",
        "version_id": 33196512,
        "package_names": ["com.gameloft.android.GAND.GloftGLPH"]
      },
      {
        "name": "L'era glaciale - Le scrat-venture",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "rating": "3",
        "descriptions": {
          "short_description":
              "Gioca con Scrat in un platform in 2D!\r\nCorri, salta e combatti per trovare la Ghianda grandiosa!",
          "long_description":
              "Viaggia in tre ambienti diversi: il valico ghiacciato, la giungla dei dinosauri e Scratlantide.\r\nGuida Scrat in quest'epica avventura correndo, saltando, arrampicandoti e altro ancora!\r\nIntraprendi divertenti battaglie contro i boss, tra cui un cucciolo di avvoltoio e un dinosauro.\r\nIncontra le creature tipiche de L'ERA GLACIALE, come gli iraci, i ratti e i feroci piranha!\r\nScopri ambienti segreti e raccogli fino all'ultima ghianda!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2026/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/2026/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/2026/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2026/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2026/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2026/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2026/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2026/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 2026,
        "html5_shortname": "",
        "version_id": 33196526,
        "package_names": [
          "com.gameloft.android.GAND.GloftSCRT",
          "com.gameloft.android.ALCH.GloftSCRT",
          "com.gameloft.android.GloftSCRT"
        ]
      },
      {
        "name": "Iron Man 3 Mobile Premium",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "",
        "size": "N/A",
        "rating": "3",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1697/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/1697/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/1697/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1697/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1697/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1697/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1697/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1697/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 1697,
        "html5_shortname": "",
        "version_id": 33196530,
        "package_names": [
          "com.gameloft.android.GAND.GloftIRO3",
          "com.gameloft.android.GloftIRO3",
          "com.gameloft.android.LATAM.GloftIRO3",
          "com.gameloft.android.LATAM.BS46",
          "com.gameloft.android.LATAM.GloftBS19",
          "com.gameloft.android.LATAM.BU05",
          "com.gameloft.android.LATAM.BE02",
          "com.gameloft.android.ALCH.GloftIRO3"
        ]
      },
      {
        "name": "Little Big City 2",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "9+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Trasforma la tua isola tropicale in un'effervescente e bellissima metropoli! Collaborerai con il sindaco e i suoi bizzarri assistenti per costruire la città dei tuoi sogni: una potenza tecnologica e industriale, ma anche un'oasi ecologica!",
          "long_description":
              "Espandi i confini e incoraggia nuovi cittadini a stabilirsi nella tua città!\r\nProduci risorse uniche per ottenere fondi per la tua città!\r\nScopri i tesori nascosti dell'isola e usali a tuo vantaggio!\r\nStudia la strategia migliore per sviluppare la tua città!\r\nRisolvi tanti problemi diversi affinché tutto vada per il meglio in città!\r\nScegli se puntare sullo sviluppo culturale, industriale o tecnologico!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2721/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/2721/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/2721/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2721/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2721/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2721/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2721/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2721/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 2721,
        "html5_shortname": "",
        "version_id": 33196536,
        "package_names": [
          "com.gameloft.android.GAND.GloftLB2P",
          "com.gameloft.android.ALCH.GloftLB2P"
        ]
      },
      {
        "name": "Midnight Pool",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "",
        "size": "N/A",
        "rating": "4.62",
        "descriptions": {
          "short_description":
              "Il gioco di biliardo più realistico, con colpi speciali, una modalità Carriera e partite online!",
          "long_description":
              "Il gioco di biliardo più realistico!\r\nDivertiti e mettiti alla prova col più realistico gioco di biliardo di sempre.\r\nAffronta la nuova modalità Carriera con 100 missioni e 7 boss da battere.\r\nSfida gli amici online e dimostra al mondo intero chi è il migliore.\r\nVisita tanti locali e sfida i giocatori migliori in un'atmosfera anni '80 unica.\r\nColleziona moltissime stecche, ognuna con caratteristiche e stile unici."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2305/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/2305/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/2305/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2305/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2305/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2305/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2305/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2305/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 2305,
        "html5_shortname": "",
        "version_id": 33196540,
        "package_names": [
          "com.gameloft.android.GAND.GloftMPMP",
          "com.gameloft.android.ALCH.GloftMPMP"
        ]
      },
      {
        "name": "Modern Combat 4: Zero Hour",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "",
        "size": "N/A",
        "rating": "2.84",
        "descriptions": {
          "short_description":
              "Salva i leader mondiali da un gruppo di terroristi!\r\n",
          "long_description":
              "Una storia avvincente scritta da sceneggiatori professionisti\r\nDai la caccia al nemico in tutto il mondo, dal Sudafrica a Barcellona\r\nGrande varietà di gioco: eventi rapidi, uccisioni furtive, droni e molto altro!\r\n2 nuove modalità: Sopravvivenza e Infiltrazione\r\nUn mix di sequenze a scorrimento laterale, alla guida di veicoli e da cecchino\r\nScopri 9 livelli esplosivi\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1660/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/1660/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/1660/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1660/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1660/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1660/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1660/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1660/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 1660,
        "html5_shortname": "",
        "version_id": 33196544,
        "package_names": [
          "com.gameloft.android.GAND.GloftMC4M",
          "com.gameloft.android.LATAM.BK25",
          "com.gameloft.android.GloftMC4M",
          "com.gameloft.android.LATAM.BS47",
          "com.gameloft.android.GAND.GloftSCRT",
          "com.gameloft.android.ALCH.GloftMC4M",
          "com.gameloft.android.HEP.GloftMZHP"
        ]
      },
      {
        "name": "Motocross : Trial Extreme Mobile Premium",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "",
        "size": "N/A",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1268/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/1268/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/1268/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1268/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1268/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1268/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1268/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1268/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 1268,
        "html5_shortname": "",
        "version_id": 33196548,
        "package_names": [
          "com.gameloft.android.GAND.GloftMOTR",
          "com.gameloft.android.BFLL.GloftMOTR",
          "com.gameloft.android.GloftMOTR"
        ]
      },
      {
        "name": "N.O.V.A. Legacy",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "",
        "size": "N/A",
        "rating": "4.77",
        "descriptions": {
          "short_description":
              "N.O.V.A. Legacy è un FPS fantascientifico in una versione compatta da 20 MB.",
          "long_description":
              "Una qualità da console su cellulare.\r\nSconfiggi le forze aliene in varie modalità di gioco.\r\nMettiti alla prova nelle arene multigiocatore.\r\nMigliora le armi, come potenti fucili d'assalto e devastanti fucili al plasma.\r\nIl divertimento del N.O.V.A. originale, con un gameplay e una grafica migliori.\r\nTutto in una versione compatta da 20 MB."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2331/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/2331/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/2331/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2331/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2331/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2331/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2331/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2331/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 2331,
        "html5_shortname": "",
        "version_id": 33196546,
        "package_names": [
          "com.gameloft.android.GAND.GloftNOMP",
          "com.gameloft.android.GloftNOMP",
          "com.gameloft.android.ALCH.GloftNOMP"
        ]
      },
      {
        "name": "Rival Wheels: Moto Drag Racing",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "rating": "4.34",
        "descriptions": {
          "short_description":
              "Preparati per il gioco di corse in moto su rettilineo più competitivo e avvincente su cellulare!",
          "long_description":
              "Più di 15 moto su licenza dei costruttori più famosi.\r\nComandi semplici: tocca per cambiare marcia, ma fallo al momento giusto!\r\nDimensioni ridotte per permettere a tutti di scaricare il divertimento!\r\nUna qualità grafica estrema: ogni moto sembra un vero bolide da corsa!\r\nGareggia a Tokyo, L'Avana e New York."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3497/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/3497/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/3497/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3497/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3497/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3497/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3497/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3497/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 3497,
        "html5_shortname": "",
        "version_id": 33196538,
        "package_names": ["com.gameloft.android.GAND.GloftRWMP"]
      },
      {
        "name": "Spider-Man: Ultimate Power ",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "12+",
        "size": "N/A",
        "rating": "4.38",
        "descriptions": {
          "short_description":
              "Il Goblin, L'Uomo Sabbia e Venom hanno unito le forze e rapito Mary Jane! Combatti per i tetti e le strade di Manhattan dondolandoti con la ragnatela per salvare la tua amata... e New York!",
          "long_description":
              "Un gioco dell'Uomo Ragno tra picchiaduro e arcade! Combatti a Manhattan!\r\n\"\r\nDai la caccia ai malfattori, sconfiggili e manda in fumo i loro piani malvagi!\"\r\n\" \r\nCombatti e dondolati in modo frenetico e divertente con due tocchi delle dita!\"\r\nSblocca versioni leggendarie dell'Uomo Ragno, come Miles Morales e Iron Spider!\r\nPotenziamenti basati sulle abilità dell'Uomo Ragno, come gli scudi di ragnatela!\r\n\"\r\nUsa il costume nero dell'Uomo Ragno per danni micidiali e record incredibili!\"\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1763/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/1763/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/1763/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1763/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1763/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1763/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1763/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1763/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 1763,
        "html5_shortname": "",
        "version_id": 33196532,
        "package_names": [
          "com.gameloft.android.GAND.GloftSMIM",
          "com.gameloft.android.GloftSMIF",
          "com.gameloft.android.GloftSMIM",
          "com.gameloft.android.ALCH.GloftSMIM"
        ]
      },
      {
        "name": "The Avengers - Il gioco per cellulari",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "12+",
        "size": "N/A",
        "rating": "2.5",
        "descriptions": {
          "short_description":
              "I Vendicatori uniti per fermare l'invasione aliena di Loki!",
          "long_description":
              "Gioca con 4 Vendicatori (Hulk, Thor, Capitan America e Iron Man)\r\nOgni Vendicatore ha i propri attacchi e poteri speciali\r\nCombatti contro un esercito di nemici e boss\r\nGiocabilità varia con livelli in volo, combo, attacchi a distanza e potenziamenti da sbloccare\r\nLuoghi e ambientazioni ricreati fedelmente dal film"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1504/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/1504/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/1504/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1504/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1504/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1504/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1504/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1504/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 1504,
        "html5_shortname": "",
        "version_id": 32570315,
        "package_names": [
          "com.gameloft.android.GAND.GloftAVEN",
          "com.gameloft.android.GloftAVEN",
          "com.gameloft.android.LATAM.GloftAVEN",
          "com.gameloft.android.LATAM.BB62",
          "com.gameloft.android.LATAM.BF87",
          "com.gameloft.android.LATAM.BS45",
          "com.gameloft.android.SFCL.GloftAVEN",
          "com.gameloft.android.ALCH.GloftAVEN"
        ]
      }
    ]
  },
  {
    "name": "Recommended",
    "type": "manual",
    "products": [
      {
        "name": "Real Football 2017",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "12+",
        "size": "N/A",
        "rating": "2.25",
        "descriptions": {
          "short_description":
              "Novità incredibili per la stagione 2017, come le strutture del club e la modalità PvP.",
          "long_description":
              "Crea una squadra da sogno: ingaggia delle stelle e potenzia le loro abilità!\r\nGrafica stupefacente e curata nei dettagli, per ricreare un'atmosfera unica!\r\nIA migliorata: giocatori più intelligenti rendono l'esperienza più realistica!\r\nSfida gli amici oppure entra nella modalità PvP Arena mondiale!\r\nGuida la tua squadra in cima alle classifiche mondali!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2617/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2617/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2617/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2617/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2617/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2617/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2617/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2617/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2617,
        "html5_shortname": "",
        "version_id": 33831194,
        "package_names": [
          "com.gameloft.android.GAND.GloftRF17",
          "com.gameloft.android.ALCH.GloftRF17"
        ]
      },
      {
        "name": "Real Football 2019",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "12+",
        "size": "N/A",
        "rating": "4.24",
        "descriptions": {
          "short_description":
              "Lotta per la gloria nella Coppa Internazionale 2018! Affronta i tuoi amici e dai il massimo!",
          "long_description":
              "Scopri il gioco di calcio più emozionante di sempre!\r\nVinci la Coppa Internazionale 2018 e diventa una leggenda!\r\nSfida giocatori da tutto il mondo!\r\nGestisci la squadra dei tuo sogni!\r\nRivivi partite leggendarie nella nuova modalità Sfida storica!\r\nMigliora le strutture del tuo club e sblocca il potenziale della tua squadra!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3452/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/3452/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/3452/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3452/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3452/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3452/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3452/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3452/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 3452,
        "html5_shortname": "",
        "version_id": 33196542,
        "package_names": ["com.gameloft.android.GAND.GloftR19F"]
      },
      {
        "name": "Asphalt Nitro",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "rating": "3.66",
        "descriptions": {
          "short_description":
              "Fai mangiare la polvere agli avversari! Divertiti con la miglior esperienza di guida per cellulari!\r\n",
          "long_description":
              "Guida lussuose auto e spingile oltre i loro limiti!\r\nSali sulle rampe ed esegui delle manovre in aria mentre fai delle acrobazie\r\nSfida gli avversari in differenti ed eccitanti modalità di gioco!\r\nLa modalità \"Guardie\" è di ritorno. Mettiti sulle tracce dei fuorilegge!\r\nAmmira gli incredibili paesaggi, realizzati con una grafica spettacolare! \r\nMettiti alla ricerca delle tante scorciatoie e taglia per primo il traguardo!\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2036/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/2036/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/2036/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2036/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2036/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2036/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2036/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2036/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 2036,
        "html5_shortname": "",
        "version_id": 33196524,
        "package_names": [
          "com.gameloft.android.FVGL.GloftANIM",
          "com.gameloft.android.FVGL.GloftDA97",
          "com.gameloft.android.FVGL.GloftDA40",
          "com.gameloft.android.GAND.GloftRF16",
          "com.gameloft.android.GloftANIM",
          "com.gameloft.android.GAND.GloftANIM",
          "com.gameloft.android.ALCH.GloftANIM"
        ]
      },
      {
        "name": "2018 Head Football World Cup",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Affronta le squadre più agguerrite del mondo in 2018 Head Football World Cup!\r\n",
          "long_description":
              "Giocatori personalizzabili e tanti tiri unici.\r\nEsperienze molto varie in due modalità di gioco."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3711/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3711/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3711/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3711/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3711/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3711/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3711/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3711/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3711,
        "html5_shortname": "",
        "version_id": 33294906,
        "package_names": ["com.baltorogames.headballworldcup"]
      },
      {
        "name": "2048 Candies",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description":
              "Scopri una nuova versione del famoso \"2048\"! Entra in un mondo di caramelle, glassa e biscotti!",
          "long_description":
              "Entra nel mondo di caramelle, glassa e biscotti.\r\nDivertiti combinandoli due a due e creando nuovi gusti, ancora più dolci.\r\nIl tuo obiettivo è quello di creare il migliore, rosso, dolcissimo, cuore glassato."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2774/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2774/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2774/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2774/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2774/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2774/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2774/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2774/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2774,
        "html5_shortname": "",
        "version_id": 32419361,
        "package_names": ["com.baltorogames.game2048Candy"]
      },
      {
        "name": "2048 Fruits",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Quando due frutti dello stesso tipo si uniscono, otterrai un frutto completamente nuovo.",
          "long_description":
              "Vinci quando sulla griglia di gioco appare una prugna. Sposta i frutti con un dito."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2328/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2328/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2328/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2328/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2328/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2328/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2328/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2328/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2328,
        "html5_shortname": "",
        "version_id": 33290470,
        "package_names": ["com.baltorogames.game2048Fruits"]
      },
      {
        "name": "3in1 Solitaire",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description": "Fai più punti che puoi.",
          "long_description":
              "Mettiti alla prova con un fantastico gioco di carte."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2390/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2390/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2390/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2390/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2390/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2390/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2390/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2390/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2390,
        "html5_shortname": "",
        "version_id": 33294944,
        "package_names": ["com.baltoro.solitaire3in1"]
      },
      {
        "name": "4096 Awesome Blossom",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "8+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Sai che aspetto ha il fiore più bello del mondo? Gioca e lo scoprirai!",
          "long_description":
              "Infinite ore di divertimento. 3 meravigliosi giardini da esplorare. Più di 20 obiettivi da completare! Grafica e animazioni da fumetto."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3176/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3176/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3176/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3176/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3176/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3176/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3176/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3176/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3176,
        "html5_shortname": "",
        "version_id": 33295462,
        "package_names": ["com.baltorogames.game4096Awesome"]
      },
      {
        "name": "7 Cells!",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "6+",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description":
              "Un divertente rompicapo in cui dovrai trovare coppie di blocchi e che ti terrà incollato allo schermo per giorni!\r\n\r\n",
          "long_description":
              "Facile da imparare e divertente da padroneggiare! Ideale per tutte le età. Varie modalità di gioco. Bellissima grafica in stile fumetto e musica rilassante!\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3346/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3346/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3346/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3346/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3346/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3346/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3346/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3346/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3346,
        "html5_shortname": "",
        "version_id": 33290468,
        "package_names": ["com.baltorogames.sevencell"]
      },
      {
        "name": "Airborne Grannies",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Queste sessantenni sono così pazze che faranno impallidire un sacco di giovinastri!\r\n",
          "long_description":
              "Fantastica grafica da fumetto ricca di umorismo. Tre nonnine stravaganti lottano per la fama in tre diversi mondi bellissimi.\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3329/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3329/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3329/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3329/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3329/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3329/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3329/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3329/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3329,
        "html5_shortname": "",
        "version_id": 33295854,
        "package_names": ["com.baltorogames.airbornegrannies"]
      },
      {
        "name": "Arcade Bowling",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Il gioco offre uno svago illimitato. Metti alla prova le tue abilità in varie modalità di gioco.",
          "long_description":
              "Metti alla prova le tue abilità in varie modalità di gioco: Partita veloce, Torneo e Multigiocatore, in cui potrai sfidare gli amici."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1999/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/1999/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/1999/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1999/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1999/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1999/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1999/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1999/default/screenshot/4.jpg"
          ]
        },
        "product_id": 1999,
        "html5_shortname": "",
        "version_id": 33253320,
        "package_names": ["com.baltorogames.arcadebowling"]
      },
      {
        "name": "Awesome Burger Bar",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "È ora di pranzo! Ondate di clienti vogliono provare i migliori hamburger della città! Dacci dentro!",
          "long_description":
              "60 obiettivi da completare, 40 statistiche da seguire e superare, 60 fantastici livelli da completare, infinite pietanze da preparare e servire."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3253/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3253/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3253/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3253/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3253/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3253/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3253/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3253/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3253,
        "html5_shortname": "",
        "version_id": 33290450,
        "package_names": ["com.baltorogames.awesomeburgerbar"]
      },
      {
        "name": "Awesome Pizza",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "È quasi ora di pranzo! Dimostra che la tua pizzeria si merita la fama!",
          "long_description":
              "90 livelli da esplorare, più di 140 piatti da preparare. Un numero infinito di miglioramenti per ingredienti e decorazioni. 47 obiettivi da completare."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3218/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3218/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3218/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3218/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3218/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3218/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3218/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3218/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3218,
        "html5_shortname": "",
        "version_id": 33290452,
        "package_names": ["com.baltorogames.awesomepizza"]
      },
      {
        "name": "Awesome Sushi Bar",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Benvenuto all'Awesome Sushi Bar! Gestisci un sushi bar e diventa un magnate della cucina giapponese! Cerca di consegnare gli ordini in tempo e fai attenzione a non mischiarli! Migliora il ristorante, decoralo e trova gli ingredienti migliori, in modo da poter offrire il meglio del sushi in città! Mettiti al lavoro!\r\n\r\n",
          "long_description":
              "60 livelli da esplorare, 50 piatti da preparare. Un numero infinito di miglioramenti per ingredienti e decorazioni del locale. 25 obiettivi da completare.\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3264/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3264/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3264/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3264/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3264/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3264/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3264/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3264/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3264,
        "html5_shortname": "",
        "version_id": 33295404,
        "package_names": ["com.baltorogames.awesomeSushiBar"]
      },
      {
        "name": "Baseball World Trophy 2017",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Il gioco di baseball più veloce e realistico per cellulari.\r\n\r\n",
          "long_description":
              "Mettiti alla prova in 3 modalità.\r\n8 squadre nazionali.\r\nGrafica, animazione ed effetti sonori splendidi.\r\nFacile da giocare, difficile da padroneggiare."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3315/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3315/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3315/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3315/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3315/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3315/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3315/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3315/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3315,
        "html5_shortname": "",
        "version_id": 33295978,
        "package_names": ["com.baltoro.baseball2016"]
      },
      {
        "name": "Battle Cards",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Una nuova versione del dungeon crawling con le carte dal sistema di gioco semplicissimo\r\n\r\n\r\n",
          "long_description":
              "Attua schemi d'attacco letali per combattere contro i boss e ottenere il bottino! Un emozionante mix di gioco di carte, rompicapo e roguelike.\r\n\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3893/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3893/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3893/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3893/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3893/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3893/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3893/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3893/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3893,
        "html5_shortname": "",
        "version_id": 33300418,
        "package_names": ["com.BaltoroGames.BattleCards"]
      },
      {
        "name": "Bowling Fever",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Vuoi divertirti? Avvia Bowling Fever e lasciati trasportare dalle emozioni!",
          "long_description":
              "Fantastiche piste di bowling da tutto il mondo.\r\nVari minigiochi da completare.\r\nTante modalità da giocare!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3239/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3239/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3239/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3239/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3239/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3239/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3239/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3239/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3239,
        "html5_shortname": "",
        "version_id": 33295874,
        "package_names": ["com.baltorogames.bowlingfever"]
      },
      {
        "name": "Bowling Nights",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Per giocare a bowling, non devi più uscire di casa.",
          "long_description":
              "Hai sempre sognato di essere un campione di bowling? Con \"Bowling Nights\" ce la farai sicuramente."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2245/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2245/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2245/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2245/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2245/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2245/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2245/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2245/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2245,
        "html5_shortname": "",
        "version_id": 33295424,
        "package_names": ["com.baltorogames.bowlingnights"]
      },
      {
        "name": "Bubble Blitz",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Da oggi, il gioco preferito di ogni alieno è disponibile anche sul nostro pianeta!",
          "long_description":
              "Scarica “Bubble Blitz” e scopri che i terrestri non sono gli unici a divertirsi con le bolle."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2206/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2206/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2206/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2206/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2206/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2206/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2206/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2206/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2206,
        "html5_shortname": "",
        "version_id": 33295638,
        "package_names": ["com.baltoro.bubbleblitz"]
      },
      {
        "name": "Bubble Bubble",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Lasciati trasportare in un universo di fantasia, con Bubble Bubble!",
          "long_description":
              "Lasciati trasportare in un universo pieno di fantasia e concediti un momento di gioco con Bubble Bubble."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2235/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2235/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2235/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2235/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2235/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2235/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2235/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2235/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2235,
        "html5_shortname": "",
        "version_id": 33296000,
        "package_names": ["com.baltoro.bubblebubble"]
      },
      {
        "name": "Bubble Cats Rescue",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Inizia a sparare alle bolle! Trasformati in un distruggi-bolle e intraprendi un viaggio esaltante.\r\n\r\n",
          "long_description":
              "Oltre 100 livelli da superare in 5 stupende ambientazioni!\r\nPersonaggi carinissimi e adorabili.\r\nMusica di sottofondo piacevole e rilassante."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3562/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3562/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3562/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3562/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3562/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3562/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3562/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3562/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3562,
        "html5_shortname": "",
        "version_id": 33290296,
        "package_names": ["com.baltorogames.bubble_cats_rescue"]
      },
      {
        "name": "Bubble Crush",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Sei un fan dei giochi di bolle? Allora devi provare questo titolo unico!\r\n",
          "long_description":
              "Tante modalità di gioco!\r\nFacile da imparare, difficile da padroneggiare!\r\nLivelli impegnativi e sempre più complessi!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3471/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3471/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3471/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3471/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3471/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3471/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3471/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3471/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3471,
        "html5_shortname": "",
        "version_id": 33295452,
        "package_names": ["com.baltorogames.zuma2017"]
      },
      {
        "name": "Bubble Frenzy",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "rating": "2.5",
        "descriptions": {
          "short_description":
              "Questa fiabesca avventura, piena di colori fantastici, ti appassionerà fin dal primo istante!",
          "long_description":
              "Entra nel mondo delle bollicine! Questa fiabesca avventura, piena di colori fantastici, ti appassionerà fin dal primo istante!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1887/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/1887/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/1887/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1887/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1887/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1887/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1887/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1887/default/screenshot/4.jpg"
          ]
        },
        "product_id": 1887,
        "html5_shortname": "",
        "version_id": 33295938,
        "package_names": ["com.baltoro.ama.bubblefrenzy"]
      },
      {
        "name": "Bubble Frenzy 2",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "L’oceano, il rumore delle onde e le bolle sono il modo ideale per rilassarsi.",
          "long_description":
              "Scoppia le bolle, ammira un mondo subacqueo e nuota per rilassarti."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2260/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2260/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2260/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2260/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2260/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2260/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2260/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2260/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2260,
        "html5_shortname": "",
        "version_id": 31962427,
        "package_names": ["com.baltoro.bubblefrenzy2"]
      },
      {
        "name": "Bubble Pop Mania",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "rating": "2.5",
        "descriptions": {
          "short_description":
              "Scopri il meraviglioso mondo delle bolle sul tuo cellulare.",
          "long_description":
              "Porta subito sul tuo telefono un mare di bolle impazzite, in un gioco coloratissimo che si può letteralmente definire un gioiello!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2985/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2985/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2985/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2985/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2985/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2985/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2985/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2985/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2985,
        "html5_shortname": "",
        "version_id": 33295400,
        "package_names": ["com.BaltoroGames.BubblePopMania"]
      },
      {
        "name": "Bubble Revolution",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Gioca a Bubble Revolutions e fai capire al tuo avversario chi comanda!",
          "long_description":
              "Usa armi cosmiche per combinare i colori e liberarti delle bolle. Affronta il nemico nelle modalità Avventura e Sopravvivenza."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2028/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2028/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2028/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2028/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2028/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2028/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2028/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2028/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2028,
        "html5_shortname": "",
        "version_id": 33253322,
        "package_names": ["com.baltorogames.candypopper2"]
      },
      {
        "name": "Buggy Off-Road Racing",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "rating": "2.5",
        "descriptions": {
          "short_description":
              "Buggy Off Road Racing ti porterà in un mondo dove ciò che conta sono la velocità e le tue capacità.",
          "long_description":
              "Fai del tuo meglio per guadagnare fama. Potrai scegliere tra i percorsi più belli del mondo, tantissimi livelli e auto incredibili!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2579/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2579/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2579/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2579/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2579/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2579/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2579/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2579/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2579,
        "html5_shortname": "",
        "version_id": 33295870,
        "package_names": ["com.baltorogames.buggy"]
      },
      {
        "name": "Candy Flip",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description": "Il tuo telefonino non è mai stato così dolce!",
          "long_description":
              "Riordina gli elementi in righe che ne comprendano almeno tre e guardali saltare nel tuo dolcissimo piatto."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2989/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2989/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2989/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2989/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2989/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2989/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2989/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2989/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2989,
        "html5_shortname": "",
        "version_id": 33295912,
        "package_names": ["com.baltorogames.candyflip"]
      },
      {
        "name": "Candy Flowers Paradise",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Prova il classico gioco di logica in questa nuova veste. Una grande sfida ti attende!",
          "long_description":
              "Prova il classico gioco di logica in questa nuova veste. Una grande sfida ti attende!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3175/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3175/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3175/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3175/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3175/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3175/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3175/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3175/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3175,
        "html5_shortname": "",
        "version_id": 33294958,
        "package_names": ["com.baltorogames.candyflowersparadise"]
      },
      {
        "name": "Candy Frenzy",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description": "Adori le caramelle? Allora prova questo gioco!",
          "long_description":
              "Esplora 3 incredibili mondi in stile fumetto. Prova 5 modalità di gioco. Rendi più facili le tue partite con potenziamenti strabilianti."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3217/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3217/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3217/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3217/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3217/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3217/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3217/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3217/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3217,
        "html5_shortname": "",
        "version_id": 33295466,
        "package_names": ["com.baltorogames.candyfrenzy"]
      },
      {
        "name": "Candy Ninja",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Ti aspettano moltissime sfide e tanto divertimento. Sei pronto a combattere?",
          "long_description":
              "60 livelli da esplorare. 8 modalità di gioco. 60 obiettivi da raggiungere. Tanti potenziamenti e spade da sbloccare."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3024/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3024/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3024/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3024/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3024/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3024/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3024/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3024/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3024,
        "html5_shortname": "",
        "version_id": 33295434,
        "package_names": ["com.baltorogames.candyninja"]
      },
      {
        "name": "Candy Pop Saga",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description":
              "Il tuo compito sarà quello di far sparire tutti i dolci presenti sullo schermo.",
          "long_description":
              "Il tuo compito sarà quello di far sparire tutti i dolci presenti sullo schermo. Per farlo, dovrai unire almeno tre pasticcini dello stesso tipo."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2172/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2172/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2172/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2172/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2172/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2172/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2172/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2172/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2172,
        "html5_shortname": "",
        "version_id": 33296084,
        "package_names": ["com.baltorogames.candypopsaga"]
      },
      {
        "name": "Candy Pop Mania",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description": "Benvenuto nel paradiso di caramelle!",
          "long_description":
              "Il tuo compito è quello di liberare la schermata da tutte le caramelle.\r\nDevi combinare almeno tre caramelle aventi lo stesso colore."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2349/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2349/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2349/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2349/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2349/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2349/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2349/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2349/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2349,
        "html5_shortname": "",
        "version_id": 33296064,
        "package_names": ["com.baltorogames.candypopmania"]
      },
      {
        "name": "Catacomb Runner 2",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description":
              "Fuggi più in fretta che puoi dai nemici che ti inseguono.",
          "long_description": "Evita le trappole che ti attendono."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2518/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2518/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2518/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2518/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2518/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2518/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2518/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2518/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2518,
        "html5_shortname": "",
        "version_id": 33296030,
        "package_names": ["com.baltorogames.catacombrunner2"]
      },
      {
        "name": "Crazy Birds Match",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "8+",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description":
              "Crazy Birds Match è un gioco di logica spassoso e rilassante, ma anche impegnativo.  Divertiti!\r\n",
          "long_description":
              "Facile da imparare e divertente da padroneggiare! Ideale per tutte le età. Varie modalità di gioco. Bellissima grafica da fumetto e musica rilassante!\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3442/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3442/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3442/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3442/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3442/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3442/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3442/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3442/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3442,
        "html5_shortname": "",
        "version_id": 33295476,
        "package_names": ["com.baltorogames.crazybirdsmatch"]
      },
      {
        "name": "Cricket 2016 - World Trophy",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Mettiti alla prova e migliora le tue abilità in modalità Partita veloce o gioca una partita intera.",
          "long_description":
              "Inizia la tua avventura in modalità Partita veloce per metterti alla prova in tutti i ruoli possibili in una squadra di cricket."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2735/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2735/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2735/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2735/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2735/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2735/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2735/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2735/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2735,
        "html5_shortname": "",
        "version_id": 33295756,
        "package_names": ["com.baltoro.cricket"]
      },
      {
        "name": "Cricket World Trophy 2017",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Gioca a CWT 2017! Vinci più corse possibili e fai vedere agli amici chi è il migliore.",
          "long_description":
              "Grafica ed effetti sonori unici e strabilianti. Controlli semplici e intuitivi con cui potrai goderti il gioco senza problemi. Tutte le squadre nazionali disponibili."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3081/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3081/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3081/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3081/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3081/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3081/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3081/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3081/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3081,
        "html5_shortname": "",
        "version_id": 33295640,
        "package_names": ["com.baltoro.cricket2017"]
      },
      {
        "name": "Cricket World Trophy 2018",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description":
              "Forma la squadra perfetta e falli diventare campioni mondiali di cricket!\r\n",
          "long_description":
              "Divertiti con modalità di gioco appassionanti, come tornei, partite veloci e incontri singoli!\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3656/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3656/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3656/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3656/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3656/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3656/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3656/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3656/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3656,
        "html5_shortname": "",
        "version_id": 33295442,
        "package_names": ["com.baltoro.cricket2018"]
      },
      {
        "name": "Deadly Fighter 2",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "rating": "3",
        "descriptions": {
          "short_description":
              "Prova l'esperienza delle battaglie di kung-fu nel gioco di lotta più intenso mai visto",
          "long_description":
              "Tantissimi livelli epici!\r\nAbilità di lotta uniche!\r\nGrafica e animazioni 3D strabilianti!\r\nMusica ed effetti sonori incredibili!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3343/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3343/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3343/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3343/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3343/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3343/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3343/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3343/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3343,
        "html5_shortname": "",
        "version_id": 33165943,
        "package_names": ["com.baltorogames.deadlyfighter2"]
      },
      {
        "name": "Diamond Clash",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description":
              "Non ci troverai frutta o verdura, ma solo diamanti!",
          "long_description":
              "Il tuo compito sarà quello di far sparire tutti i diamanti, sfruttando meno mosse possibili."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2178/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2178/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2178/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2178/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2178/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2178/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2178/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2178/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2178,
        "html5_shortname": "",
        "version_id": 33295986,
        "package_names": ["com.baltorogames.diamondclash"]
      },
      {
        "name": "Diamond Hunt",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description":
              "La tua avventura inizia qui: un viaggio fra misteri e cacce al tesoro!",
          "long_description":
              "Esplora 3 incredibili mondi da fumetto. Prova le 5 diverse modalità di gioco. Rendi più facili le tue partite con potenziamenti strabilianti.\r\n\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3347/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3347/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3347/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3347/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3347/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3347/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3347/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3347/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3347,
        "html5_shortname": "",
        "version_id": 33295914,
        "package_names": ["com.baltorogames.diamondshunt"]
      },
      {
        "name": "Diamond Splash",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Lo sapevi che ci sono dei tesori nascosti nel tuo cellulare?",
          "long_description":
              "Raccogli più diamanti che puoi e completa le missioni."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2386/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2386/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2386/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2386/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2386/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2386/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2386/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2386/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2386,
        "html5_shortname": "",
        "version_id": 33296036,
        "package_names": ["com.baltorogames.diamondsplashppd"]
      },
      {
        "name": "Diamonds Bay Paradise",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description":
              "Un grande rompicapo con un tocco d'innovazione! Trova la soluzione nel minor numero di mosse possibili.\r\n\r\n",
          "long_description":
              "Supera livelli incredibili sempre più difficili su 3 grandi isole pirata. Esplora 5 straordinari mondi in stile fumetto.\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3291/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3291/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3291/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3291/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3291/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3291/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3291/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3291/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3291,
        "html5_shortname": "",
        "version_id": 33295972,
        "package_names": ["com.baltorogames.diamondsbayparadise"]
      },
      {
        "name": "Doodle Brick Breaker",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Fai rimbalzare la pallina sulla piattaforma mobile per eliminare i mattoncini!",
          "long_description":
              "L’abilità è la tua unica arma! La versione definitiva di un grande classico! Fai rimbalzare la pallina sulla piattaforma mobile per eliminare i mattoncini! Doodle Brick Breaker è l’edizione moderna di un classico nel suo genere. Un sacco di trappole ti attendono!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2029/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2029/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2029/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2029/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2029/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2029/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2029/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2029/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2029,
        "html5_shortname": "",
        "version_id": 33290278,
        "package_names": ["com.baltorogames.doodlebrickbreaker"]
      },
      {
        "name": "Drag Boats Racing",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Gareggia nella Coppa del Mondo, corri al massimo e diventa una leggenda delle gare di motoscafi!\r\n",
          "long_description":
              "Grafica di alta qualità! Scegli fra 12 motoscafi personalizzabili! Raggiungi velocità incredibili! Personalizza e potenzia i tuoi motoscafi nel garage!\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3477/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3477/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3477/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3477/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3477/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3477/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3477/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3477/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3477,
        "html5_shortname": "",
        "version_id": 33295426,
        "package_names": ["com.baltoro.dragboatsracing"]
      },
      {
        "name": "Dragon Fight",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Un classico poligono di tiro con dettagli da gioco di combattimento!\r\n\r\n\r\n",
          "long_description":
              "Un classico gameplay da sparatutto con lightgun sul tuo schermo! Più di 40 livelli e una modalità infinita!\r\n\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3652/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3652/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3652/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3652/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3652/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3652/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3652/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3652/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3652,
        "html5_shortname": "",
        "version_id": 33300386,
        "package_names": ["com.BaltoroGames.DuckHunt"]
      },
      {
        "name": "Extreme Quad Bikes Pro",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description":
              "Se ami superare i tuoi limiti e apprezzi le emozioni forti, Extreme Quad Bikes è il gioco per te!",
          "long_description":
              "Dovrai affrontare avversari difficili e percorsi complicati. Preparati alla neve, alla sabbia e al fango."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2179/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2179/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2179/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2179/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2179/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2179/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2179/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2179/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2179,
        "html5_shortname": "",
        "version_id": 33295642,
        "package_names": ["baltoro.quadbikespro"]
      },
      {
        "name": "F-Next : Deadly Velocity",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description":
              "Un gioco di corse futuristiche 3D velocissimo e pieno d'azione da cui non riuscirai a staccarti.\r\n",
          "long_description":
              "Una grafica da console, una sensazione di velocità indescrivibile, navicelle personalizzabili, un'esperienza di corse appassionante e una colonna sonora pazzesca!\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3601/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3601/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3601/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3601/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3601/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3601/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3601/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3601/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3601,
        "html5_shortname": "",
        "version_id": 33300382,
        "package_names": ["com.BaltoroGames.Fnextdeadlyvelocity"]
      },
      {
        "name": "Firepower Knights",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Gli alieni sono qui! Preparati alla pugna! Appronta lo scudo, il cavallo e la tua ARMA DA FUOCO!",
          "long_description":
              "Potenzia il tuo cavaliere per trasformarlo in una macchina da guerra! Usa armi letali, che vanno dalle velocissime mitragliatrici ai lanciagranate!\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3798/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3798/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3798/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3798/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3798/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3798/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3798/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3798/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3798,
        "html5_shortname": "",
        "version_id": 33165945,
        "package_names": ["com.baltorogames.firepowerknights"]
      },
      {
        "name": "Fishing Mania",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description":
              "Scopri cosa ti aspetta ai confini dell'oceano con la tua combinazione personale di arpione e barca!",
          "long_description":
              "Grafica e audio sbalorditivi. Scegli tra 10 bellissime barche e 20 arpioni con varie caratteristiche! 20 fantastici pesci da scoprire!\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3381/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3381/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3381/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3381/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3381/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3381/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3381/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3381/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3381,
        "html5_shortname": "",
        "version_id": 33295860,
        "package_names": ["com.baltoro.fishingAdventure.AppActivity"]
      },
      {
        "name": "Flappy Duck 2",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "\"Flappy Duck2\" cambierà il tuo modo di vedere le anatre.",
          "long_description":
              "Devi aiutare l’anatra a volare il più lontano possibile."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2350/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2350/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2350/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2350/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2350/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2350/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2350/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2350/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2350,
        "html5_shortname": "",
        "version_id": 33296110,
        "package_names": ["com.ama.flappyduck2"]
      },
      {
        "name": "Flow Lines HEX",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Accoppia tutti i colori, coprendo tutto il tabellone per risolvere ogni puzzle.\r\n",
          "long_description":
              "Puzzle curati e di alta qualità, con difficoltà che spaziano dal facile al difficilissimo!\r\nGameplay di facile comprensione."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3799/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3799/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3799/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3799/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3799/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3799/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3799/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3799/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3799,
        "html5_shortname": "",
        "version_id": 33300402,
        "package_names": ["com.BaltoroGames.FlowLines"]
      },
      {
        "name": "Food Truck Tycoon",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "In questo gioco capirai se hai la stoffa per diventare un magnate dei furgoni del cibo!\r\n",
          "long_description":
              "60 livelli da esplorare, 50 piatti da preparare. Un numero infinito di miglioramenti per ingredienti e decorazioni del locale. 25 obiettivi da completare.\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3514/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3514/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3514/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3514/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3514/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3514/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3514/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3514/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3514,
        "html5_shortname": "",
        "version_id": 33295390,
        "package_names": ["com.baltorogames.foodtrucktycoon"]
      },
      {
        "name": "Football Bubbles 2014",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description":
              "Allenati per i campionati, a Football Bubbles 2014!",
          "long_description":
              "Ottieni il massimo numero di punti e diventa il miglior giocatore in campo. Scegli tra 3 modalità di gioco: Avventura, Sopravvivenza e Zen."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2135/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2135/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2135/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2135/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2135/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2135/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2135/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2135/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2135,
        "html5_shortname": "",
        "version_id": 33295630,
        "package_names": ["com.baltoro.footballbubbles2014"]
      },
      {
        "name": "Formula Racing PRO 2018",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Salta nel tuo veicolo ad alte prestazioni e accelera a velocità mozzafiato!\r\n\r\n",
          "long_description":
              "Più di 15 piste ultrarealistiche. Salta in oltre 10 tipi di auto unici della Formula Pro. Una simulazione realistica della fisica automobilistica.\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3584/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3584/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3584/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3584/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3584/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3584/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3584/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3584/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3584,
        "html5_shortname": "",
        "version_id": 33295472,
        "package_names": ["com.baltorogames.frp2018"]
      },
      {
        "name": "BALTORO Freeride Snowboarding",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2224/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2224/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2224/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2224/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2224/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2224/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2224/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2224/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2224,
        "html5_shortname": "",
        "version_id": 33295906,
        "package_names": ["baltorogames.freeridesnowboarding"]
      },
      {
        "name": "Go! Rabbit Go!",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Corri più lontano che puoi e raccogli il maggior numero di diamanti possibili.",
          "long_description":
              "Corri più lontano che puoi e raccogli il maggior numero di diamanti possibili, finché l'aria non finisce!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2652/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2652/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2652/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2652/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2652/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2652/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2652/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2652/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2652,
        "html5_shortname": "",
        "version_id": 33300482,
        "package_names": ["com.baltoro.gorabbitgo"]
      },
      {
        "name": "Go Kart Mania Pro",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description":
              "Semina gli avversari e aggiudicati un posto sul podio.",
          "long_description":
              "La pista ti aspetta! Mostra agli avversari che sei il migliore! Seminali e aggiudicati un posto sul podio! Il gioco GoKartmania Pro ti garantirà una scarica di adrenalina sportiva e grandi emozioni."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2211/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2211/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2211/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2211/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2211/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2211/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2211/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2211/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2211,
        "html5_shortname": "",
        "version_id": 33296014,
        "package_names": ["com.baltorogames.gokartmania3"]
      },
      {
        "name": "BALTORO Go! Fish Go!",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3177/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3177/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3177/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3177/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3177/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3177/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3177/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3177/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3177,
        "html5_shortname": "",
        "version_id": 33295736,
        "package_names": ["com.baltorogames.fishrunner"]
      },
      {
        "name": "BALTORO GoKarts Championships",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3540/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3540/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3540/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3540/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3540/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3540/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3540/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3540/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3540,
        "html5_shortname": "",
        "version_id": 33294936,
        "package_names": ["com.baltorogames.gokartschampionship"]
      },
      {
        "name": "BALTORO Headball Football",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3472/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3472/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3472/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3472/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3472/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3472/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3472/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3472/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3472,
        "html5_shortname": "",
        "version_id": 33294942,
        "package_names": ["com.baltorogames.headball"]
      },
      {
        "name": "Hell Jump 2",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "\"Hell Jump2\" ti farà saltare nel bel mezzo del divertimento.",
          "long_description":
              "Evita le trappole, raccogli le monete e non smettere mai di saltare."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2365/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2365/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2365/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2365/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2365/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2365/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2365/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2365/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2365,
        "html5_shortname": "",
        "version_id": 33296112,
        "package_names": ["baltorogames.helljump2"]
      },
      {
        "name": "BALTORO Hooooop!",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3290/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3290/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3290/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3290/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3290/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3290/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3290/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3290/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3290,
        "html5_shortname": "",
        "version_id": 33296044,
        "package_names": ["com.baltoro.hooooop"]
      },
      {
        "name": "Jet Pack Dash",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Devi usare tutta la tua destrezza e avere i riflessi pronti per completare in sicurezza la missione.",
          "long_description":
              "Nei corridoi ci sono diverse trappole e pericoli. Devi usare tutta la tua destrezza e avere i riflessi pronti per completare in sicurezza la missione."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2001/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2001/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2001/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2001/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2001/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2001/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2001/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2001/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2001,
        "html5_shortname": "",
        "version_id": 33296026,
        "package_names": ["com.baltorogames.jetpackdash"]
      },
      {
        "name": "BALTORO Jet Pack Dash 2",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2539/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2539/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2539/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2539/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2539/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2539/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2539/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2539/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2539,
        "html5_shortname": "",
        "version_id": 33295964,
        "package_names": ["com.baltorogames.jetpackdash2"]
      },
      {
        "name": "BALTORO Jet Pack Robot",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2238/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2238/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2238/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2238/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2238/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2238/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2238/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2238/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2238,
        "html5_shortname": "",
        "version_id": 33296094,
        "package_names": ["com.baltorogames.jetpackrobot"]
      },
      {
        "name": "BALTORO Jet Ski Rush",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "rating": "4.9",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2352/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2352/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2352/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2352/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2352/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2352/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2352/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2352/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2352,
        "html5_shortname": "",
        "version_id": 33295902,
        "package_names": []
      },
      {
        "name": "BALTORO Jewel Flip Saga",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2605/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2605/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2605/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2605/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2605/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2605/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2605/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2605/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2605,
        "html5_shortname": "",
        "version_id": 33300468,
        "package_names": ["com.baltorogames.jewelflipsaga"]
      },
      {
        "name": "BALTORO Knights and Robbers",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2273/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2273/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2273/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2273/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2273/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2273/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2273/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2273/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2273,
        "html5_shortname": "",
        "version_id": 33295976,
        "package_names": ["baltorogames.knightsrobbers"]
      },
      {
        "name": "BALTORO Ludomania",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3673/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3673/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3673/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3673/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3673/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3673/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3673/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3673/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3673,
        "html5_shortname": "",
        "version_id": 33290294,
        "package_names": ["com.baltorogames.ludomania"]
      },
      {
        "name": "Monster Truck Racing",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Monster truck al turbo! Corse adrenaliniche con fisica realistica!\r\n",
          "long_description":
              "Guida in 3 diversi luoghi sparsi in tutto il mondo, ognuno con il suo tema!\r\nScegli fra 12 monster truck unici ed estremi!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3539/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3539/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3539/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3539/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3539/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3539/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3539/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3539/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3539,
        "html5_shortname": "",
        "version_id": 33294916,
        "package_names": ["com.baltoro.monstertruckdrag"]
      },
      {
        "name": "Moto Racer Fury",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Moto Racer Fury è la miglior esperienza di corsa nel traffico che ci sia!\r\n",
          "long_description":
              "Tante modalità di gioco! Carriera, infinita, corsa libera o a tempo! Grafica ed effetti visivi ottimi e musica strabiliante!\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3443/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3443/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3443/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3443/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3443/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3443/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3443/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3443/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3443,
        "html5_shortname": "",
        "version_id": 33294960,
        "package_names": ["com.baltorogames.motoracerfury"]
      },
      {
        "name": "BALTORO Moto Racing Rivals",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3063/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3063/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3063/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3063/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3063/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3063/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3063/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3063/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3063,
        "html5_shortname": "",
        "version_id": 33295468,
        "package_names": ["com.baltorogames.motoracingrivals"]
      },
      {
        "name": "Motocross Mania",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Monta sulla tua moto e corri verso la vittoria! Ti attendono emozioni e un po' di follia!",
          "long_description":
              "Monta sulla tua moto e corri verso la vittoria! “Motocrossmania” non è dedicato solo ai fan del motocross: è un gioco per tutti!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1992/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/1992/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/1992/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1992/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1992/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1992/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1992/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1992/default/screenshot/4.jpg"
          ]
        },
        "product_id": 1992,
        "html5_shortname": "",
        "version_id": 33295968,
        "package_names": ["baltoro.motocrossracerpro"]
      },
      {
        "name": "Motocross Mania Pro",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Fango, polvere e numerosi ostacoli sulla strada garantiranno il tuo divertimento.",
          "long_description":
              "Puoi correre in scenari diversi, inoltre, per i punti guadagnati ricevi la moto sempre migliore. "
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2512/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2512/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2512/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2512/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2512/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2512/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2512/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2512/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2512,
        "html5_shortname": "",
        "version_id": 33295738,
        "package_names": ["baltorogames.motocrossmaniapro"]
      },
      {
        "name": "Motorbikes Pro 2015",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description": "Ogni giro ti darà una scarica di adrenalina.",
          "long_description":
              "Hai un solo obiettivo: taglia il traguardo per primo e lascia che gli altri mangino la polvere."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2217/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2217/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2217/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2217/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2217/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2217/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2217/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2217/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2217,
        "html5_shortname": "",
        "version_id": 33295406,
        "package_names": ["com.baltoro.motorbikespro2015"]
      },
      {
        "name": "Motorbikes PRO 2017",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Taglia le curve e sfreccia lungo le piste più impegnative in giro per il mondo!\r\n\r\n",
          "long_description":
              "Più di 15 piste motociclistiche ultrarealistiche. Salta in sella a più di 10 moto uniche e realistiche. Un simulatore dalla fisica ultrarealistica."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3384/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3384/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3384/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3384/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3384/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3384/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3384/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3384/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3384,
        "html5_shortname": "",
        "version_id": 33165941,
        "package_names": ["com.baltorogames.motorbikespro2017"]
      },
      {
        "name": "My Breakfast Bar",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "È quasi ora di colazione! Dimostra che il tuo bar è degno della sua fama!",
          "long_description":
              "Interminabili ore di divertimento! Simpaticissima grafica. Musica ed effetti sonori rilassanti. 45 livelli da completare! Migliora gli ingredienti e rendi il tuo bar più attraente!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3102/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3102/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3102/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3102/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3102/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3102/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3102/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3102/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3102,
        "html5_shortname": "",
        "version_id": 33295402,
        "package_names": ["com.baltorogames.mybreakfastbar"]
      },
      {
        "name": "My Magic Florist",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Gioca a My Magic Florist e aiuta la fatina a creare bouquet floreali per i suoi clienti.\r\n\r\n",
          "long_description":
              "Un luogo meraviglioso e pieno di fiori ti attende! Impara a progettare aiuole fiorite! Completa gli ordini per i clienti! Gestisci il tuo negozio di fiori!\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3894/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3894/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3894/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3894/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3894/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3894/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3894/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3894/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3894,
        "html5_shortname": "",
        "version_id": 33295392,
        "package_names": ["com.baltorogames.mymagicflorist"]
      },
      {
        "name": "My Salad Bar",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description":
              "Gestisci il tuo salad bar e diventa un magnate del cibo dietetico!",
          "long_description":
              "60 obiettivi da completare, 40 statistiche da seguire e superare, 60 fantastici livelli da completare, infinite pietanze da preparare e servire.\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3446/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3446/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3446/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3446/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3446/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3446/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3446/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3446/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3446,
        "html5_shortname": "",
        "version_id": 33295742,
        "package_names": ["com.baltorogames.awesomesaladbar"]
      },
      {
        "name": "Pancake Chef",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Non devi essere un vero chef per imparare a preparare i pancake per la colazione!\r\n\r\n",
          "long_description":
              "Un gioco di cucina a tema pancake ideale per i più piccoli.\r\nRendi perfetto ogni pancake scegliendo solo i topping migliori!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3538/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3538/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3538/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3538/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3538/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3538/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3538/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3538/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3538,
        "html5_shortname": "",
        "version_id": 33295384,
        "package_names": ["com.baltorogames.pancakechef"]
      },
      {
        "name": "Pet Shop Snacks",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Accudisci i gattini. Diventa oggi stesso il proprietario di un bar per animali!",
          "long_description":
              "Grafica da fumetto, musica ed effetti sonori stupendi. Decorazioni per il bar e innumerevoli miglioramenti per i tuoi ingredienti.\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3657/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3657/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3657/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3657/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3657/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3657/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3657/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3657/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3657,
        "html5_shortname": "",
        "version_id": 33294926,
        "package_names": ["com.baltorogames.petshopsnacks"]
      },
      {
        "name": "Pocket Foosball",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Il calcio balilla entra nella tua tasca! Gioca quando vuoi! Il brivido sportivo è garantito!",
          "long_description":
              "Divertiti a scoprire nuovi tavoli, ometti e palline!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2990/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2990/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2990/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2990/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2990/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2990/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2990/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2990/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2990,
        "html5_shortname": "",
        "version_id": 33295924,
        "package_names": ["com.baltorogames.pocketfoosball"]
      },
      {
        "name": "Pocket Sudoku",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Non solo devi risolvere i quesiti, devi anche farlo velocemente!",
          "long_description":
              "Pocket Sudoku è una vera sfida con diversi livelli di difficoltà. Non solo devi risolvere i quesiti, devi anche farlo velocemente!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2622/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2622/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2622/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2622/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2622/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2622/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2622/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2622/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2622,
        "html5_shortname": "",
        "version_id": 33300470,
        "package_names": ["com.baltoro.sudoku"]
      },
      {
        "name": "Porkmageddon",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Fai del tuo meglio, forma una squadra e dimostra che i maiali possono volare! ",
          "long_description":
              "Partecipa al divertimento: fai atterrare i maiali il più lontano possibile e... cerca di farlo con stile!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2547/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2547/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2547/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2547/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2547/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2547/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2547/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2547/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2547,
        "html5_shortname": "",
        "version_id": 33296118,
        "package_names": ["com.baltorogames.porkmageddon"]
      },
      {
        "name": "Prison Escape",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Le trappole metteranno alla prova la tua agilità, quindi dovrai imparare a correre come il vento!",
          "long_description":
              "Se non hai paura dei topi o del buio e vuoi sfuggire ai pericoli come un asso, avvia il gioco!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2051/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2051/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2051/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2051/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2051/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2051/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2051/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2051/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2051,
        "html5_shortname": "",
        "version_id": 33290276,
        "package_names": ["com.baltoro.escapealcatraz"]
      },
      {
        "name": "Puzzle Diamonds",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Con \"Puzzle Diamonds\" scoprirai una nuova dimensione di intrattenimento.",
          "long_description":
              "Sai quali tesori si nascondono nel tuo cellulare? Grazie al gioco \"Puzzle Diamonds\" scoprirai una nuova dimensione di intrattenimento."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2745/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2745/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2745/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2745/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2745/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2745/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2745/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2745/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2745,
        "html5_shortname": "",
        "version_id": 33295684,
        "package_names": ["com.baltorogames.puzzlediamonds"]
      },
      {
        "name": "Quick Golf",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Diventa un maestro del golf! Fai del tuo meglio per conquistare i vari mondi.\r\n\r\n",
          "long_description":
              "Oltre 40 livelli su 3 diversi pianeti.\r\nOggetti per la personalizzazione avanzata!\r\nPersonalizza e cambia il tuo gioco con vari oggetti!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3556/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3556/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3556/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3556/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3556/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3556/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3556/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3556/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3556,
        "html5_shortname": "",
        "version_id": 33294962,
        "package_names": ["com.baltorogames.quickgolf"]
      },
      {
        "name": "RC Cars Championships",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description": "Dovrai mettercela tutta per vincere.",
          "long_description":
              "Non farti ingannare dal loro aspetto innocente: sono davvero veloci."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2244/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2244/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2244/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2244/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2244/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2244/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2244/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2244/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2244,
        "html5_shortname": "",
        "version_id": 33295478,
        "package_names": ["com.baltoro.rccars"]
      },
      {
        "name": "RC Cars Grand Prix 2018",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Un entusiasmante gioco corse arcade ambientato nel mondo delle macchine radiocomandate realistiche\r\n",
          "long_description":
              "Gioca in modalità Carriera per sbloccare nuove piste! Una simulazione ultrarealistica della fisica delle auto radiocomandate! Grafica 3D realistica, musica ed effetti sonori strabilianti.\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3604/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3604/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3604/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3604/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3604/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3604/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3604/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3604/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3604,
        "html5_shortname": "",
        "version_id": 33295948,
        "package_names": ["com.baltorogames.rccars2"]
      },
      {
        "name": "Real Drift Racing",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "6+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Scatena il demone della velocità! Derapate, velocità e competizione: tutte in un solo gioco!\r\n\r\n\r\n",
          "long_description":
              "La più realistica simulazione 3D di gare in derapata su dispositivi portatili. Tante opzioni per modificare motore, trasmissione, sospensioni e molto altro.\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3311/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3311/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3311/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3311/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3311/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3311/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3311/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3311/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3311,
        "html5_shortname": "",
        "version_id": 33295842,
        "package_names": ["com.BaltoroGames.RealDriftRacing"]
      },
      {
        "name": "Run Mummy Run 2",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Raggiungi la fine del corridoio ed evita le trappole lungo la strada.",
          "long_description": "Il tuo compito è quello di aiutarla a scappare."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2261/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2261/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2261/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2261/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2261/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2261/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2261/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2261/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2261,
        "html5_shortname": "",
        "version_id": 33295758,
        "package_names": ["com.baltoro.runmummyrun2"]
      },
      {
        "name": "Rush Hour",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Non c'è molto da dire. Siamo solo alle porte di una catastrofe stradale. Però ci sei tu, giusto?\r\n\r\n",
          "long_description":
              "Una metropoli gigantesca prende vita grazie a una grafica incredibile!\r\nGestisci non solo le auto private, ma anche tram, ambulanze e simili!\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3648/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3648/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3648/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3648/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3648/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3648/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3648/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3648/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3648,
        "html5_shortname": "",
        "version_id": 33300384,
        "package_names": ["com.BaltoroGames.RushHour"]
      },
      {
        "name": "Santa's Jewels Flip",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Un sacco di addobbi natalizi e gioielli ti porteranno in un momento magico.",
          "long_description":
              "Ogni combo ti darà dei punti extra. Ricorda di sbloccare e usare i bonus, renderanno il gioco ancora più avvincente!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2661/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2661/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2661/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2661/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2661/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2661/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2661/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2661/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2661,
        "html5_shortname": "",
        "version_id": 33300484,
        "package_names": ["com.baltorogames.santasjewelflip"]
      },
      {
        "name": "Sea Wolf Attack",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Comanda un sottomarino che si aggira negli oceani in cerca di navi nemiche da affondare.\r\n",
          "long_description":
              "60 missioni da completare in tanti luoghi diversi! 10 sottomarini e 20 siluri da sbloccare! L'emozione dei giochi di una volta sul tuo cellulare!\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3551/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3551/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3551/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3551/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3551/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3551/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3551/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3551/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3551,
        "html5_shortname": "",
        "version_id": 33290472,
        "package_names": ["com.seawolf.AppActivity"]
      },
      {
        "name": "Slots Mania",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "8+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Il modo migliore di provare il brivido delle vere slot machine di Las Vegas.\r\n\r\n",
          "long_description":
              "Slot machine di alta qualità e completamente gratuite. Meravigliosi temi grafici disegnati a mano, tutti in HD.\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3309/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3309/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3309/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3309/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3309/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3309/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3309/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3309/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3309,
        "html5_shortname": "",
        "version_id": 33295386,
        "package_names": ["com.baltoro.slotsofthrones"]
      },
      {
        "name": "Snake Revolutions",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Lascia che il serpente entri nel tuo cellulare e godi di numerose ore di un divertimento eccellente.",
          "long_description":
              "Controlla il serpente, raccogli i punti e fallo crescere. Batti il tuo record e usa bonus e potenziamenti aggiuntivi."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2598/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2598/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2598/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2598/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2598/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2598/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2598/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2598/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2598,
        "html5_shortname": "",
        "version_id": 33300464,
        "package_names": ["com.baltoro.snake.wrap"]
      },
      {
        "name": "BALTORO Space Bugs",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2060/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2060/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2060/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2060/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2060/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2060/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2060/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2060/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2060,
        "html5_shortname": "",
        "version_id": 33290280,
        "package_names": ["baltorogames.spacebugs"]
      },
      {
        "name": "Space Defender",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Pilota una nave da difesa sperimentale e abbatti tutti gli alieni che puoi.\r\n",
          "long_description":
              "Effetti speciali e di luce strabilianti, innumerevoli livelli da superare e boss con schemi d'attacco complessi e trasformazioni pericolose. Tante ore di divertimento assicurato!\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3602/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3602/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3602/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3602/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3602/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3602/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3602/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3602/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3602,
        "html5_shortname": "",
        "version_id": 33295704,
        "package_names": ["com.baltorogames.spacedefender"]
      },
      {
        "name": "Starblaster",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Sei solo contro un intero esercito. Non hanno speranze.\r\n\r\n\r\n",
          "long_description":
              "Esplora la galassia mentre combatti contro l'armata del male! Riuscirai a schivare ogni singolo proiettile che ti viene incontro? Effetti speciali e di luci straordinari.\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3827/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3827/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3827/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3827/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3827/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3827/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3827/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3827/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3827,
        "html5_shortname": "",
        "version_id": 33295746,
        "package_names": ["com.baltorogames.starblaster"]
      },
      {
        "name": "Street Basketball",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "La pallacanestro su cellulare non è mai stata così bella!\r\n\r\n\r\n",
          "long_description":
              "19 personaggi diversi, ciascuno con il proprio aspetto e tiro speciale. Gameplay variato grazie alle due diverse modalità di gioco.\r\n\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3823/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3823/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3823/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3823/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3823/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3823/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3823/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3823/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3823,
        "html5_shortname": "",
        "version_id": 33295858,
        "package_names": ["com.baltorogames.streetbasketball"]
      },
      {
        "name": "Street Bowling",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "rating": "2.5",
        "descriptions": {
          "short_description":
              "Lancia la palla da professionista, mettiti alla prova e diventa un campione di bowling da strada!",
          "long_description":
              "Fantastiche piste di bowling da tutto il mondo. Vari minigiochi da completare. Tante modalità da giocare!\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3529/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3529/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3529/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3529/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3529/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3529/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3529/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3529/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3529,
        "html5_shortname": "",
        "version_id": 33295470,
        "package_names": ["com.baltorogames.streetbowling"]
      },
      {
        "name": "Street Racing Champions",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Scala la gerarchia delle bande locali e diventa il miglior pilota nelle gare in rettilineo!",
          "long_description":
              "Grafica realistica con animazioni ed effetti sonori eccellenti.\r\nPotenzia e personalizza le tue auto come vuoi."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3094/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3094/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3094/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3094/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3094/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3094/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3094/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3094/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3094,
        "html5_shortname": "",
        "version_id": 33294976,
        "package_names": ["com.baltoro.streetracingchampions"]
      },
      {
        "name": "Street Racing PRO",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Esibisci le tue abilità di guida e diventa il miglior corridore su strada!",
          "long_description":
              "Un gioco di corse arcade ricco di azione. Facile da giocare, difficile da padroneggiare!\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3308/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3308/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3308/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3308/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3308/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3308/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3308/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3308/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3308,
        "html5_shortname": "",
        "version_id": 33295436,
        "package_names": ["com.baltorogames.streetracingpro"]
      },
      {
        "name": "BALTORO Super Quad Bikes",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "rating": "4.9",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3030/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3030/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3030/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3030/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3030/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3030/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3030/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3030/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3030,
        "html5_shortname": "",
        "version_id": 33295602,
        "package_names": ["com.baltorogames.SuperQuadBikes"]
      },
      {
        "name": "Sushi Ninja Revenge",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Prendi la tua katana, avvia il gioco e divertiti!",
          "long_description":
              "Metti alla prova la tua abilità da Ninja ed evita le trappole esplosive."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2205/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2205/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2205/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2205/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2205/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2205/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2205/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2205/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2205,
        "html5_shortname": "",
        "version_id": 33296028,
        "package_names": ["baltorogames.sushininjarevenge"]
      },
      {
        "name": "Tank vs Tank",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Scegli il tuo carro, potenziane il cannone e scontrati con i tuoi avversari!\r\n",
          "long_description":
              "Un divertentissimo gioco di battaglie fra carri armati. Costruisci il tuo carro con decine di pezzi e gioca contro un'incredibile CPU!\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3923/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3923/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3923/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3923/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3923/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3923/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3923/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3923/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3923,
        "html5_shortname": "",
        "version_id": 33300458,
        "package_names": ["com.baltorogames.tankvstank"]
      },
      {
        "name": "Traffic Jam Control",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description":
              "Sai cosa serve per essere un controllore del traffico? Ti sembra interessante? Lo è!",
          "long_description":
              "Grafica 3D strabiliante. Infinite ore di divertimento. Esplora 17 grandi incroci con diverse difficoltà. Completa 17 obiettivi per dimostrare chi è il migliore!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3026/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3026/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3026/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3026/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3026/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3026/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3026/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3026/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3026,
        "html5_shortname": "",
        "version_id": 33295430,
        "package_names": ["com.BaltoroGames.TrafficJamControl"]
      },
      {
        "name": "BALTORO Traffic Racing",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3129/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3129/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3129/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3129/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3129/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3129/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3129/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3129/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3129,
        "html5_shortname": "",
        "version_id": 33295448,
        "package_names": ["com.baltorogames.trafficracing"]
      },
      {
        "name": "Truck Racing Championship",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Raggiungi il traguardo per primo e non lasciarti sorpassare!",
          "long_description":
              "Sali a bordo del tuo super-truck, accendi il suo motore potente e preparati a correre, in Truck Racing Championship."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2240/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2240/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2240/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2240/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2240/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2240/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2240/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2240/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2240,
        "html5_shortname": "",
        "version_id": 21273450,
        "package_names": ["com.baltoro.truckracing"]
      },
      {
        "name": "Ultimate Rally Championship 2",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Gareggia sulla neve del Nepal, sulla ghiaia dell’India e tra le sabbie dell’Australia.",
          "long_description":
              "Gareggia sulla neve del Nepal, sulle strade sterrate dell’India e tra le sabbie dell’Australia. Puoi selezionare una delle quattro modalità di gioco. In Corsa contro il tempo, il tuo avversario sarà il cronometro, in Hot Seat potrai scegliere il numero di rivali, mentre nelle Gare singole potrai allenarti prima di affrontare la sfida definitiva in modalità Campionato mondiale."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2057/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2057/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2057/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2057/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2057/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2057/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2057/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2057/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2057,
        "html5_shortname": "",
        "version_id": 33295920,
        "package_names": ["baltoro.urc2.system"]
      },
      {
        "name": "Ultimate Truck Racing",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "rating": "5",
        "descriptions": {
          "short_description":
              "Mettiti al volante dei monster truck e prova un'esaltante sferzata di adrenalina!\r\n\r\n\r\n",
          "long_description":
              "Guida in 3 quartieri, ciascuno con un tema unico!\r\nMettiti alla prova contro i più violenti re della strada!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3515/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3515/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3515/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3515/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3515/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3515/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3515/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3515/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3515,
        "html5_shortname": "",
        "version_id": 33294918,
        "package_names": ["com.baltoro.streetracingchampions"]
      },
      {
        "name": "Ultimate Winter Sports 2016",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description": "Adori gli sport invernali adrenalinici?",
          "long_description":
              "Ami gli sport invernali e l'adrenalina? Allora vieni subito a giocare a \"Ultimate Winter Sports 2016\"!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2701/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2701/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2701/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2701/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2701/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2701/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2701/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2701/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2701,
        "html5_shortname": "",
        "version_id": 33296120,
        "package_names": ["com.baltorogames.wintersport2016"]
      },
      {
        "name": "Vegas Casino Slots",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Vieni a divertirti anche tu e cerca di ottenere più punti possibili.",
          "long_description":
              "Dovrai ottenere più punti possibili. Goditi le emozioni del vero gioco d’azzardo e fai del tuo meglio."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2578/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2578/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2578/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2578/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2578/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2578/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2578/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2578/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2578,
        "html5_shortname": "",
        "version_id": 33295432,
        "package_names": ["com.baltoro.slots"]
      },
      {
        "name": "Word Masters",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "8+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Ottimo per gli amanti delle lingue, sia per allenarsi online, sia per giocare tutti insieme!\r\n",
          "long_description":
              "Multiplayer fino a 4 giocatori. Ottima CPU contro cui giocare. Tre diverse modalità di gioco e requisiti per la vittoria personalizzabili.\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3819/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3819/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3819/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3819/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3819/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3819/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3819/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3819/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3819,
        "html5_shortname": "",
        "version_id": 33296062,
        "package_names": ["com.baltorogames.wordmasters"]
      },
      {
        "name": "World Bowling Tour 2016",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description":
              "D'ora in poi non ti separerai più dal tuo passatempo preferito grazie a \"World Bowling Tour 2016\".",
          "long_description":
              "Gioca a bowling senza uscire di casa. D'ora in poi non ti separerai più dal tuo passatempo preferito grazie a \"World Bowling Tour 2016\"."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2746/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2746/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2746/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2746/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2746/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2746/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2746/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2746/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2746,
        "html5_shortname": "",
        "version_id": 33296122,
        "package_names": ["com.baltorogames.bowling2016"]
      },
      {
        "name": "World of Solitaire 6 in 1",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Il gioco 6in1 Solitaire ti offre un'infinità di giochi ed emozioni indimenticabili.",
          "long_description":
              "6in1 Solitaire ti offre un'infinità di giochi ed emozioni indimenticabili. Ora, tutti migliori solitari sono disponibili!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2684/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2684/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2684/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2684/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2684/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2684/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2684/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2684/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2684,
        "html5_shortname": "",
        "version_id": 33296024,
        "package_names": ["com.baltoro.solitaire6in1"]
      },
      {
        "name": "Block Breaker 3 Unlimited",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "",
        "size": "N/A",
        "rating": "2.92",
        "descriptions": {
          "short_description":
              "Affronta livelli ancora più complessi che vanno al di là di un semplice schermo pieno di mattoncini.\r\n",
          "long_description":
              "Il gioco di mattoncini definitivo: 100 innovativi livelli a scorrimento \r\nTonnellate di potenziamenti: gioca con palline multiple e pad multipli!\r\nCombatti contro i boss in battalgie epiche alla fine di ognuna delle 7 aree!\r\nCompra nuovi poteri e bonus nel negozio per potenziare il tuo pad.\r\n9 diverse modalità di gioco, come Multigiocatore 1 contro 1 e Block Master\r\nDivertimento senza fine, grazie al generatore di livelli"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1113/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/1113/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/1113/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1113/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1113/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1113/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1113/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1113/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 1113,
        "html5_shortname": "",
        "version_id": 33831196,
        "package_names": [
          "com.gameloft.android.GAND.GloftBB3U.ML",
          "com.gameloft.android.GAND.GloftBB3U.SC",
          "com.gameloft.android.GAND.GloftBB3U.CN",
          "com.gameloft.android.GAND.GloftBB3U_ML",
          "com.gameloft.android.GloftBB3U",
          "com.gameloft.android.GAND.GloftBB3U_EN",
          "com.gameloft.android.EU.GloftBB3U.ML",
          "com.gameloft.android.GAND.GloftBLB3.ML",
          "com.gameloft.android.LATAM.GloftBLB3",
          "com.gameloft.android.LATAM.GloftBB3U",
          "com.gameloft.android.GloftBLB3",
          "com.gameloft.android.GAND.GloftBB3U",
          "com.gameloft.android.EU.GloftBLB3",
          "com.gameloft.android.GAND.GloftBLB3",
          "com.gameloft.GAND.android.GloftBLB3",
          "com.gameloft.android.BFLL.GloftBLB3"
        ]
      },
      {
        "name": "Brain Challenge 4: Oltre ogni limite",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "6+",
        "size": "N/A",
        "rating": "5",
        "descriptions": {
          "short_description": "Allena la tua mente... e la tua anima!",
          "long_description":
              "Allena la tua mente e la tua anima con 41 minigiochi!\r\nImpara a conoscerti più a fondo con il nuovo test dell'intelligenza emotiva.\r\nSperimenta le nuove categorie Riflessi e Speciale.\r\nIl programma di allenamento più personalizzato che ci sia.\r\nNumerosi strumenti per monitorare i tuoi progressi."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1386/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/1386/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/1386/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1386/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1386/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1386/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1386/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1386/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 1386,
        "html5_shortname": "",
        "version_id": 33831198,
        "package_names": [
          "com.gameloft.android.GAND.GloftBRC4",
          "com.gameloft.android.LATAM.GloftBRC4",
          "com.gameloft.android.LATAM.X558",
          "com.gameloft.android.LATAM.X964",
          "com.gameloft.android.LATAM.B029",
          "com.gameloft.android.LATAM.BB61",
          "com.gameloft.android.LATAM.BD95",
          "com.gameloft.android.GloftBRC4",
          "com.gameloft.android.SFR.GloftBRC4",
          "com.gameloft.android.ALCH.GloftBRC4",
          "com.gameloft.android.EU.GloftBRC4.ML"
        ]
      },
      {
        "name": "Bubble Bash 3",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Lo sparabolle più divertente è tornato con 120 nuovi livelli, fisica migliorata e sfide mai viste!",
          "long_description":
              "Un nuovo capitolo dell'amatissima serie di giochi rompicapo\r\nCimentati in 120 livelli con nuove sfide, nuovi bonus e nuova fisica\r\nEsplora 8 ambientazioni con paesaggi da sogno come Saint-Tropez e le Hawaii\r\nSblocca personaggi e lanciatori colorati e divertenti"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1565/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/1565/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/1565/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1565/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1565/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1565/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1565/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1565/default/screenshot/4.jpg"
          ]
        },
        "product_id": 1565,
        "html5_shortname": "",
        "version_id": 33831200,
        "package_names": [
          "com.gameloft.android.GAND.GloftBUB3",
          "com.gameloft.android.LATAM.GloftBU3P",
          "com.gameloft.android.GAND.GloftBU3P",
          "com.gameloft.android.LATAM.GloftX642",
          "com.gameloft.android.GloftBUB3"
        ]
      },
      {
        "name": "Bubble Bash Mania",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Viaggia dalle isole calde di Saint-Tropez fino alla gelida Ice Station! Abbina le bolle dello stesso colore ed eliminale! Leva l'ancora e scoppia le bolle come un vero campione in questo nuovo rompicapo sparabolle divertente e avventuroso!",
          "long_description":
              "Scopri 200 livelli pieni di bolle da scoppiare in 10 aree divertenti!\r\nSpara alle bolle, raggiungi il punteggio richiesto e libera il pesce rosso!\r\nCollegati a Facebook per affrontare gli amici o aiutarli!\r\nProva i potenziamenti speciali delle bolle per superare dei puzzle avvincenti!\r\nGioca con comandi tattili semplici: tocca per mirare e rilascia per sparare!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1773/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/1773/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/1773/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1773/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1773/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1773/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1773/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1773/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 1773,
        "html5_shortname": "",
        "version_id": 33831202,
        "package_names": [
          "com.gameloft.android.GAND.GloftBBPM",
          "com.gameloft.android.LATAM.GloftBBPM",
          "com.gameloft.android.GloftBBPM"
        ]
      },
      {
        "name": "Night Surfer",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Un folle gioco di corsa in un ambiente fantasy.",
          "long_description":
              "Aiuta Alex a correre, schivare ostacoli e sfuggire ai demoni per conquistare la notte scura."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2241/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2241/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2241/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2241/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2241/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2241/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2241/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2241/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2241,
        "html5_shortname": "",
        "version_id": 31962661,
        "package_names": ["com.ama.nightsurfer"]
      },
      {
        "name": "Cars - Sfide ruggenti",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "rating": "4.63",
        "descriptions": {
          "short_description":
              "Saetta McQueen e i suoi amici sono tornati per correre intorno al mondo in un torneo spettacolare!",
          "long_description":
              "Il gioco ufficiale di Cars della DisneyPixar, con tutto il divertimento dei film!\r\nCorse veloci, emozionanti e piene di adrenalina con divertenti comandi tattili!\r\nSblocca, potenzia e corri con 15 personaggi, tra cui Francesco, Holley, Cricchetto e Saetta McQueen!\r\nDivertimento infinito! Gioca in modalità Classica o affronta i boss in modalità Storia!\r\n5 capitoli, 70 eventi e varie tipologie di gioco, tra cui il minigioco dei trattori di Cricchetto!\r\nCorri su 29 piste diverse a Radiator Springs, a Londra e in Italia, con scorciatoie nascoste!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1732/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/1732/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/1732/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1732/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1732/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1732/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1732/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1732/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 1732,
        "html5_shortname": "",
        "version_id": 33196506,
        "package_names": [
          "com.gameloft.android.GAND.GloftCRSM",
          "com.gameloft.android.GloftCRSM",
          "com.gameloft.android.GAND.GloftCRPH",
          "com.gameloft.android.ALCH.GloftCRSM"
        ]
      },
      {
        "name": "Double Dragon I",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "12+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Double Dragon è un innovativo e popolarissimo gioco di lotta cooperativo uscito nel 1987.",
          "long_description":
              "Due modalità di gioco: \"Arcade\" e \"Storia\", controlli personalizzabili, obiettivi e classifiche (Game Center)."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3254/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3254/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3254/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3254/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3254/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3254/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3254/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3254/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3254,
        "html5_shortname": "",
        "version_id": 33294964,
        "package_names": ["com.dotemu.ddragon1"]
      },
      {
        "name": "Double Dragon II",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "12+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Double Dragon è un innovativo e popolarissimo gioco di lotta cooperativo.\r\n\r\n",
          "long_description":
              "Due modalità di gioco: \"Arcade\" e \"Storia\", controlli personalizzabili, obiettivi e classifiche (Game Center).\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3255/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3255/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3255/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3255/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3255/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3255/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3255/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3255/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3255,
        "html5_shortname": "",
        "version_id": 33294950,
        "package_names": ["com.dotemu.ddragon2"]
      },
      {
        "name": "Double Dragon III",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "12+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Double Dragon è un innovativo e popolarissimo gioco di lotta cooperativo.\r\n\r\n",
          "long_description":
              "Due modalità di gioco: \"Arcade\" e \"Storia\", controlli personalizzabili, obiettivi e classifiche (Game Center).\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3256/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3256/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3256/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3256/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3256/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3256/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3256/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3256/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3256,
        "html5_shortname": "",
        "version_id": 33294948,
        "package_names": ["com.dotemu.ddragon3"]
      },
      {
        "name": "DOTEMU Pang Adventures",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3803/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3803/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3803/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3803/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3803/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3803/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3803/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3803/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3803,
        "html5_shortname": "",
        "version_id": 33290460,
        "package_names": ["com.dotemu.pangadventures"]
      },
      {
        "name": "R-Type I",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "6+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Accendi il tuo smartphone e preparati a tornare indietro... a R-Type!\r\n\r\n",
          "long_description":
              "Modalità touch migliorata, 3 livelli di difficoltà.\r\n\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3260/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3260/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3260/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3260/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3260/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3260/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3260/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3260/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3260,
        "html5_shortname": "",
        "version_id": 33295750,
        "package_names": ["com.dotemu.rtype"]
      },
      {
        "name": "R-Type II",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "L'Impero Bydo è tornato! Sconfiggilo di nuovo con dei potenziamenti e due nuovi tipi di armi!\r\n\r\n",
          "long_description":
              "Comandi personalizzabili. Gioca a schermo intero o mantieni la risoluzione di gioco originale.\r\n\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3261/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3261/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3261/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3261/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3261/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3261/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3261/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3261/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3261,
        "html5_shortname": "",
        "version_id": 33294954,
        "package_names": ["com.dotemu.rtype2"]
      },
      {
        "name": "Raiden Legacy",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "6+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "RAIDEN LEGACY, una raccolta di quattro titoli della famosa serie arcade RAIDEN, ora sul tuo cellulare!\r\n\r\n",
          "long_description":
              "Filtro video (per una grafica ricca e armoniosa), 3 diverse modalità: Arcade, Missione e Addestramento.\r\n\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3262/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3262/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3262/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3262/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3262/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3262/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3262/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3262/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3262,
        "html5_shortname": "",
        "version_id": 33295866,
        "package_names": ["com.dotemu.raidenlegacy"]
      },
      {
        "name": "Danger Dash",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Scappa dalle tigri feroci in località esotiche! Batti i tuoi amici e diventa un vero avventuriero!\r\n",
          "long_description":
              "Scappa dalle tigri feroci e diventa un vero avventuriero... se ne hai il coraggio!\r\nAffronta il pericolo in 3 ambientazioni: la giungla, la città perduta e il tempio misterioso.\r\nCorri, scivola e migliora le tue abilità per schivare gli ostacoli e sbloccare nuovi livelli.\r\nRaccogli potenziamenti per arrivare più lontano e ottenere punteggi più alti!\r\nRaccogli i manufatti per sbloccare i personaggi Chuck Ace, Miranda Rosa e Vento Impetuoso.\r\nEntra nella classifica online, controlla i punteggi dei tuoi amici e fai mangiar loro la polvere!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1707/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/1707/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/1707/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1707/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1707/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1707/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1707/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1707/default/screenshot/4.jpg"
          ]
        },
        "product_id": 1707,
        "html5_shortname": "",
        "version_id": 21100788,
        "package_names": [
          "com.gameloft.android.GAND.GloftJDMP",
          "com.gameloft.android.GloftJDMP",
          "com.gameloft.android.GloftGT2M"
        ]
      },
      {
        "name": "Diamond Twister 2",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "",
        "size": "N/A",
        "rating": "2.67",
        "descriptions": {
          "short_description":
              "Torna a divertirti scambiando le gemme in 10 fantastici modi, con potenziamenti e una storia da star.\r\n",
          "long_description":
              "Giocabilità semplice e appassionante sia in modalità Storia o Storia infinita\r\n120 livelli ambientati in 8 luoghi da star\r\nIncontra nuovi personaggi lungo la strada mentre avanzi nella storia\r\n10 nuove meccaniche di gioco per scambiare le gemme e sbarazzarti della nebbia\r\nNuovi potenziamenti quali Spirale e Stasi utili per ottenere sempre più punti\r\nEffetti speciali grandiosi come esplosioni, tornado e altro ancora"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1215/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/1215/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/1215/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1215/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1215/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1215/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1215/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1215/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 1215,
        "html5_shortname": "",
        "version_id": 33196508,
        "package_names": [
          "com.gameloft.android.EU.GloftDIT2.ML",
          "com.gameloft.android.GAND.GloftGMHP",
          "com.gameloft.android.GAND.GloftDIT2",
          "com.gameloft.android.LATAM.GloftDIT2",
          "com.gameloft.android.GloftDIT2",
          "com.gameloft.android.SAMSUNG.GloftDIT2",
          "com.gameloft.android.LATAM.GloftX330"
        ]
      },
      {
        "name": "DIE HARD - UN BUON GIORNO PER MORIRE",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Il gioco ufficiale per cellulari del nuovo film di Die Hard! Hippy ya ye, figlio di compagna!\r\n",
          "long_description":
              "Il gioco di DIE HARD - UN BUON GIORNO PER MORIRE, il quinto film della serie\r\nStoria e dialogo fedeli all'originale: rivivi la tensione e l'intrigo del film\r\nEntra nei panni di John e Jack McClane e rivivi le scene più emozionanti\r\nGiocabilità variegata! Corri e spara, sequenze a tempo e inseguimenti in auto\r\nEsplora 8 livelli enormi ispirati al film, dal tribunale di Mosca a Prypiat\r\nElimina i nemici con 7 armi, come pistola, fucile e lanciagranate\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1657/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/1657/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/1657/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1657/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1657/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1657/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1657/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1657/default/screenshot/4.jpg"
          ]
        },
        "product_id": 1657,
        "html5_shortname": "",
        "version_id": 33295664,
        "package_names": ["com.gameloft.android.GAND.GloftDIH5"]
      },
      {
        "name": "Disney Magic Kingdoms",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "rating": "4.53",
        "descriptions": {
          "short_description":
              "Crea il parco Disney dei tuoi sogni e condividi momenti unici con gli amatissimi personaggi Disney!",
          "long_description":
              "Attrazioni ispirate ai parchi a tema Disney straordinarie e senza tempo\r\nPersonaggi tratti da più di 90 anni di Disney\r\nUn'avvincente storia di eroi e cattivi\r\nRivivi i momenti più magici dei parchi con allegre e spettacolari parate\r\nCentinaia di missioni animate e divertenti, che danno vita al tuo regno!\r\nRaccogli oggetti a tema Disney e riporta nel regno i personaggi spariti"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2429/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/2429/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/2429/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2429/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2429/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2429/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2429/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2429/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 2429,
        "html5_shortname": "",
        "version_id": 33196518,
        "package_names": [
          "com.gameloft.android.GAND.GloftDMKP",
          "com.gameloft.android.ALCH.GloftDMKP"
        ]
      },
      {
        "name": "Dragon Mania",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "rating": "5",
        "descriptions": {
          "short_description":
              "Alleva i draghi più temibili. Allenali in battaglie contro gli amici e i vichinghi cattivi!",
          "long_description":
              "Draghi originali con abilità uniche e diversi attributi elementali.\r\nCombina i draghi per creare nuove specie! Colleziona e alleva tantissimi draghi!\r\nIncontra i draghi più temibili. Preparali a sconfiggere i vichinghi cattivi!\r\nAllena i draghi per affrontare gli amici in un'arena epica!\r\n\"Epici scontri con i re vichinghi, da solo o con i draghi dei tuoi amici.\r\n\""
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1656/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/1656/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/1656/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1656/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1656/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1656/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1656/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1656/default/screenshot/4.jpg"
          ]
        },
        "product_id": 1656,
        "html5_shortname": "",
        "version_id": 33831208,
        "package_names": [
          "com.gameloft.android.GAND.GloftPDMP",
          "com.gameloft.android.LATAM.GloftPDMF",
          "com.gameloft.android.GloftPDMR"
        ]
      },
      {
        "name": "Dungeon Hunter 3",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Un'avventura epica in un’ambientazione “dark fantasy” come i migliori RPG d'azione",
          "long_description":
              "Un'atmosfera \"dark fantasy\", grafica 3D isometrica e animazioni incredibili\r\nCrea il tuo personaggio: 3 classi, ciascuna con 2 tipi di specializzazione\r\nPiù di 100 oggetti, armi e armature che potrai trovare o comprare dal mercante\r\nUn gioco d'azione con battaglie in tempo reale per il divertimento di tutti\r\nGioca con amici che ti aiuteranno a salvare il tuo mondo... e la tua anima\r\n5 fate ti guideranno nella tua missione offrendoti protezione, potenza e consigli"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1396/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/1396/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/1396/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1396/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1396/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1396/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1396/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1396/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 1396,
        "html5_shortname": "",
        "version_id": 33831212,
        "package_names": [
          "com.gameloft.android.GAND.GloftDHU3",
          "com.gameloft.android.ANMP.GloftDHU3",
          "com.gameloft.android.GAND.GloftDH3M",
          "com.gameloft.android.SFCL.GloftDHU3"
        ]
      },
      {
        "name": "Dungeon Hunter Curse of Heaven",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Tuffati nell'RPG d'azione fantasy più intenso e magico!",
          "long_description":
              "Batti orde di mostri magici, guerrieri con spade, ombre magiche e tanto altro.\r\n24 missioni per scoprire di più sui tuoi poteri e sulla morte dei tuoi cari.\r\nAttraversa portali magici verso il mondo delle ombre, pieno di sfide e premi.\r\nCrea un esercito, potenzia la fortezza e batti i nemici in modalità Conquista.\r\nAffronta i dungeon giornalieri per ricevere nuove sfide e premi ogni giorno.\r\nOttieni più di 300 spade, aste, balestre e bastoni unici e usane 2 alla volta!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2952/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/2952/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/2952/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2952/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2952/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2952/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2952/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2952/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 2952,
        "html5_shortname": "",
        "version_id": 33196514,
        "package_names": [
          "com.gameloft.android.GAND.GloftDUMP",
          "com.gameloft.android.GAND.GloftDG73"
        ]
      },
      {
        "name": "Ferrari GT 3: World Track ",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Vivi il sogno di gareggiare in posti fantastici di tutto il mondo con 57 modelli diversi di Ferrari.",
          "long_description":
              "L'acclamata e fantastica serie di corse ritorna con il suo gioco migliore!\r\nGuida 57 modelli autentici di Ferrari, come la FF o la 458 Italia.\r\nGareggia in 10 luoghi sparsi per il mondo, da Miami a Hong Kong.\r\nCompleta tante sfide: batti gli avversari, derapa e tanto altro! \r\nScopri tutto sul Cavallino Rampante con i consigli dell'enciclopedia Ferrari."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1539/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/1539/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/1539/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1539/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1539/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1539/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1539/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1539/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 1539,
        "html5_shortname": "",
        "version_id": 33195256,
        "package_names": [
          "com.gameloft.android.GAND.GloftFG3P",
          "com.gameloft.android.GAND.GloftFGT3",
          "com.gameloft.android.LATAM.GloftFG3P",
          "com.gameloft.android.SAMSUNG.GloftFG3P",
          "com.gameloft.android.GloftFGT3"
        ]
      },
      {
        "name": "Adventure Time - Heroes of Ooo",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "La Terra di Ooo è in grave pericolo! Sconfiggi i cattivi e riporta l'ordine nelle Terre di Ooo.",
          "long_description":
              "Attraversa quattro labirinti segreti, affrontando mostri e raccogliendo tesori. Sconfiggi gli abitanti malvagi e riporta l'ordine nelle Terre di Ooo."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2674/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2674/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2674/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2674/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2674/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2674/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2674/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2674/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2674,
        "html5_shortname": "",
        "version_id": 33295410,
        "package_names": ["com.globalfun.adventuretime"]
      },
      {
        "name": "Adventure Time Raider",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Jake si trasforma in un mantello e con Finn vola attraverso strani mondi dello spazio e del tempo.",
          "long_description":
              "Ice King ha creato uno strappo nel continuum spazio-temporale e, quando la principessa Bubblegum scopre che è lui il responsabile, lui la rapisce!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2741/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2741/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2741/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2741/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2741/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2741/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2741/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2741/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2741,
        "html5_shortname": "",
        "version_id": 33295836,
        "package_names": ["com.globalfun.adta.google"]
      },
      {
        "name": "Agent Gumball",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description": "Il mio nome è Gumball, AGENTE GUMBALL!",
          "long_description":
              "Sfodera i tuoi gadget da spia! Recluta gli amici di Gumball e mandali in missioni segrete. Il mio nome è Gumball, AGENTE GUMBALL!\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3664/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3664/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3664/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3664/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3664/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3664/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3664/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3664/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3664,
        "html5_shortname": "",
        "version_id": 33296008,
        "package_names": ["com.turner.agentgumball"]
      },
      {
        "name": "Ben10: Evoluzione Aliena",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description": "Salva Undertown dal malvagio Psyphon!\r\n",
          "long_description":
              "Psyphon e la sua malvagia orda aliena stanno cercando di distruggere Undertown! Riuscirai ad aiutare Ben a sconfiggere Psyphon?\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3653/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3653/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3653/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3653/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3653/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3653/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3653/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3653/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3653,
        "html5_shortname": "",
        "version_id": 33295730,
        "package_names": ["com.globalfun.ben10ae.premium"]
      },
      {
        "name": "Ben 10 Potere dell'Omnitrix",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description": "Salva Undertown dal malvagio Psyphon!",
          "long_description":
              "Psyphon ha scatenato la sua orda aliena su Undertown nel tentativo di distruggerla una volta per tutte. Saprai aiutare Ben a sconfiggerlo?"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3004/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3004/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3004/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3004/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3004/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3004/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3004/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3004/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3004,
        "html5_shortname": "",
        "version_id": 33295422,
        "package_names": ["com.globalfun.vilgax.premium"]
      },
      {
        "name": "Ben 10: Omniverse",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description":
              "La missione di Ben consiste nel trovare e disattivare i dispositivi il più velocemente possibile.",
          "long_description":
              "Il malvagio Psyphon ha collocato delle cariche esplosive in ogni angolo della città e minaccia di farle esplodere."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2700/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2700/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2700/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2700/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2700/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2700/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2700/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2700/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2700,
        "html5_shortname": "",
        "version_id": 33295646,
        "package_names": ["com.globalfun.ben10omniverse"]
      },
      {
        "name": "Ben 10: Power of the Omnitrix",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description": "Trasformati in alieni e combatti!",
          "long_description":
              "Trasformati in alieno e fatti strada tra eserciti di droni, mutanti e cavalieri immortali, in questo picchiaduro ricco d'azione."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3020/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3020/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3020/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3020/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3020/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3020/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3020/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3020/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3020,
        "html5_shortname": "",
        "version_id": 33294972,
        "package_names": ["Ben10.Omnitrix"]
      },
      {
        "name": "Ben 10 Ultimate Alien: Ultimate Defender",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Il terribile Aggregor sta cercando di diventare onnipotente, acquisendo i poteri degli altri alieni!\r\n\r\n",
          "long_description":
              "Le dinamiche di gioco in stile retrò ti faranno divertire mentre progredisci nei numerosi livelli. Ben 10 The Ulimate Alien è tornato!\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3339/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3339/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3339/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3339/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3339/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3339/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3339/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3339/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3339,
        "html5_shortname": "",
        "version_id": 33295418,
        "package_names": ["Ben10.UA"]
      },
      {
        "name": "Ben 10: Undertown Chase",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Aiuta Ben a seminare Khyber in BEN 10 OMNIVERSE: INSEGUIMENTO A UNDERTOWN!!",
          "long_description":
              "Aiuta Ben e Rook a sfuggire alle grinfie del cacciatore più pericoloso della galassia in BEN 10 OMNIVERSE: INSEGUIMENTO A UNDERTOWN!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2982/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2982/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2982/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2982/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2982/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2982/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2982/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2982/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2982,
        "html5_shortname": "",
        "version_id": 33295740,
        "package_names": ["com.turner.ben10chase"]
      },
      {
        "name": "Ben 10 - Vendetta di Vilgax",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "rating": "5",
        "descriptions": {
          "short_description":
              "Affronta i minigiochi, assumi nuove forme aliene e affronta la vendetta di Vilgax!",
          "long_description":
              "Affronta Vilgax, il tuo acerrimo nemico, e combatti in 4 esclusivi mondi e su 15 livelli pieni di azione, bioidi e droni."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2787/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2787/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2787/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2787/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2787/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2787/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2787/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2787/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2787,
        "html5_shortname": "",
        "version_id": 33294966,
        "package_names": ["Ben10.Vilgax"]
      },
      {
        "name": "Gare in barca",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description":
              "Hai a disposizione un sacco di entusiasmanti funzioni e armi che puoi usare nelle tue missioni!",
          "long_description":
              "Usa i missili per distruggere le barche dei pirati. Hai a disposizione un sacco di entusiasmanti funzioni e armi che puoi usare nelle tue missioni!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2517/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2517/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2517/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2517/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2517/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2517/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2517/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2517/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2517,
        "html5_shortname": "",
        "version_id": 33295950,
        "package_names": ["com.fazzidice.br.gmlft"]
      },
      {
        "name": "Brake Racer",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description": "Divora la strada nel magnifico gioco racing!",
          "long_description":
              "Gioco di corse arcade. Metti alla prova le tue abilità in questo classico!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2804/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2804/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2804/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2804/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2804/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2804/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2804/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2804/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2804,
        "html5_shortname": "",
        "version_id": 33295388,
        "package_names": ["com.ak.racing.gl"]
      },
      {
        "name": "Brick Breaker Blitz",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              " Brick Breaker Blitz è uno dei più popolari giochi arcade con grafiche spettacolari.",
          "long_description":
              "Questo gioco contiene 40 livelli di varie forme e dimensioni, come orologi che si muovono, alberi di Natale, fiori volteggianti e tanti altri."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2604/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2604/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2604/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2604/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2604/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2604/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2604/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2604/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2604,
        "html5_shortname": "",
        "version_id": 33300466,
        "package_names": ["com.dumadugames.brickbreakerblitz"]
      },
      {
        "name": "CN Superstar Soccer: Goal!!!",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Diventa un supercampione del pallone, con i tuoi personaggi preferiti di Cartoon Network!\r\n\r\n",
          "long_description":
              "Divertiti a giocare a pallone in campi da gioco diversi. Raccogli i potenziamenti per conquistare un vantaggio. Gioca in modalità torneo per vincere la Superstar Cup!\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3722/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3722/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3722/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3722/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3722/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3722/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3722/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3722/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3722,
        "html5_shortname": "",
        "version_id": 33296010,
        "package_names": ["com.turner.superstarsoccer2"]
      },
      {
        "name": "CR7 Football 2018",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "CR7 Cristiano Ronaldo! Divertentissime partite di calcio, rapide e impegnative! Giochiamo!\r\n\r\n\r\n\r\n",
          "long_description":
              "CR7 Cristiano Ronaldo! Gioca! Si comincia! Il meglio del calcio!\r\n\r\n\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3788/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3788/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3788/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3788/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3788/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3788/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3788/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3788/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3788,
        "html5_shortname": "",
        "version_id": 33295428,
        "package_names": ["com.globalfun.cr7football2018"]
      },
      {
        "name": "CR7 Penalty Flick Soccer",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Cristiano Ronaldo. Tira... e segna! WOOAAAAHHHHH! La folla impazzisce!!!\r\n\r\n\r\n",
          "long_description":
              "Affina le tue abilità in modalità di esercitazione e spianati la strada a suon di \"calci\" nella fase a gironi, in modo da raggiungere le fasi finali."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3716/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3716/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3716/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3716/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3716/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3716/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3716/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3716/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3716,
        "html5_shortname": "",
        "version_id": 33290298,
        "package_names": ["com.globalfun.cr7penalty.premium"]
      },
      {
        "name": "Cannonball 8000",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description":
              "Partecipa all'avventurosa gara Cannonball 8000! Piloti di Cannonball, accendete i motori!",
          "long_description":
              "Corri contromano evitando le auto in arrivo e non farti prendere dagli sbirri!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2899/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2899/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2899/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2899/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2899/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2899/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2899/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2899/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2899,
        "html5_shortname": "",
        "version_id": 33295620,
        "package_names": ["com.globalfun.cannonball"]
      },
      {
        "name": "Quiz delle Capitali",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Quanto bene conosci il nostro pianeta e le sue nazioni? Diventa un professionista!",
          "long_description":
              "Abbina le bandiere alle rispettive nazioni, impara dove si trovano i monumenti famosi e scopri le valute del mondo."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2324/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2324/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2324/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2324/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2324/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2324/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2324/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2324/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2324,
        "html5_shortname": "",
        "version_id": 33296104,
        "package_names": ["org.supergonk.capitals"]
      },
      {
        "name": "Campione Checkpoint",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description": "72 piccole sfide uniche!",
          "long_description":
              "Controlla le auto con 2 dita! 72 piccole sfide uniche! Un ventaglio di macchine vecchio stile, ognuna con colori personalizzabili! Diversi tipi di terreno! 3 arene infinite!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2369/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2369/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2369/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2369/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2369/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2369/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2369/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2369/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2369,
        "html5_shortname": "",
        "version_id": 31962105,
        "package_names": ["com.protostar.checkpointgl"]
      },
      {
        "name": "Crazy racing",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Manovra il tuo veicolo attraverso un traffico folle, destreggiandoti tra le corsie.",
          "long_description":
              "Prova la modalità arcade infinita piena d'azione o la modalità Avventura con 30 livelli. Tante nuove funzioni, armi e auto da sbloccare."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2368/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2368/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2368/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2368/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2368/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2368/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2368/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2368/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2368,
        "html5_shortname": "",
        "version_id": 33295760,
        "package_names": ["com.fazzidice.cr.gmlft"]
      },
      {
        "name": "Penalty Cristiano Ronaldo",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description":
              "Riuscirai a vincere la Coppa del Mondo con la tua nazionale? Tutto si decide ai calci di rigore!",
          "long_description":
              "Gioca a un round di calci di rigore per vincere la partita finale. È un gioco divertente e avvincente!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2790/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2790/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2790/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2790/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2790/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2790/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2790/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2790/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2790,
        "html5_shortname": "",
        "version_id": 33295634,
        "package_names": ["com.globalfun.penalty.ronaldo"]
      },
      {
        "name": "CR7 Football 2016",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description":
              "Con Ronaldo nella tua squadra, dribbla, tira e segna per passare il turno e vincere!",
          "long_description":
              "Con Ronaldo nella tua squadra, dribbla, tira e segna per passare il turno! Riuscirai a vincere la Coppa con la tua Nazionale?"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2811/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2811/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2811/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2811/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2811/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2811/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2811/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2811/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2811,
        "html5_shortname": "",
        "version_id": 33294914,
        "package_names": ["com.globalfun.worldfootball2014"]
      },
      {
        "name": "Don't get Crashed",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Manovra il tuo veicolo fra le corsie per schivare le auto in arrivo.",
          "long_description":
              "Manovra il tuo veicolo fra le corsie, tocca lo schermo per raggiungere quella più sicura: ti serviranno dei riflessi fulminei!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2554/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2554/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2554/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2554/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2554/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2554/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2554/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2554/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2554,
        "html5_shortname": "",
        "version_id": 33295916,
        "package_names": ["com.fazzidice.dgc.gmlft"]
      },
      {
        "name": "El Che",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "9+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Guida Ernesto \"Che\" Guevara e i suoi uomini nella rivoluzionaria liberazione di Cuba.\r\n\r\n",
          "long_description":
              "Guida Ernesto \"Che\" Guevara e i suoi uomini nella rivoluzionaria liberazione di Cuba dalla dittatura di Batista.\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3259/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3259/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3259/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3259/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3259/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3259/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3259/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3259/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3259,
        "html5_shortname": "",
        "version_id": 33295606,
        "package_names": ["com.globalfun.elche"]
      },
      {
        "name": "Find Candy",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Accattivante grafica da cartone animato e gameplay dinamico.",
          "long_description":
              "Abbina 3 caramelle colorate dello stesso tipo per farle sparire!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2789/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2789/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2789/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2789/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2789/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2789/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2789/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2789/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2789,
        "html5_shortname": "",
        "version_id": 33300488,
        "package_names": ["ua.netlizard.find_candy"]
      },
      {
        "name": "Frisbee® Forever",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Scatenati nei mondi di Frisbee® Forever! Sei un vero mago del Frisbee®?",
          "long_description":
              "Scatenati nei mondi di Frisbee® Forever! Sei un vero mago del Frisbee®?"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3269/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3269/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3269/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3269/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3269/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3269/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3269/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3269/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3269,
        "html5_shortname": "",
        "version_id": 33165939,
        "package_names": ["com.kiloo.frisbeeforever"]
      },
      {
        "name": "GLOBALFUN Fruit Worm Bubble",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2374/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2374/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2374/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2374/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2374/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2374/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2374/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2374/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2374,
        "html5_shortname": "",
        "version_id": 33295958,
        "package_names": ["com.mobartis.fruitwormbubble.demo"]
      },
      {
        "name": "GLOBALFUN Generator Rex",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3210/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3210/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3210/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3210/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3210/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3210/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3210/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3210/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3210,
        "html5_shortname": "",
        "version_id": 33294956,
        "package_names": ["Generator.Rex"]
      },
      {
        "name": "Ghost Toasters: Regular Show",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Diventa l'abbattifantasmi numero uno in GHOST TOASTER!",
          "long_description":
              "Salva il parco da un'invasione in questo SPARATUTTO PLATFORM! Prendi le armi ultratecnologiche e fai fuori i fantasmi killer!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2976/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2976/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2976/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2976/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2976/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2976/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2976/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2976/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2976,
        "html5_shortname": "",
        "version_id": 33300438,
        "package_names": ["com.turner.ghosttoasters"]
      },
      {
        "name": "Gravity Ninja",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Sconfiggi le leggi della gravità per impedire al ninja di cadere.",
          "long_description": "Un gioco d'azione arcade."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2513/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2513/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2513/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2513/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2513/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2513/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2513/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2513/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2513,
        "html5_shortname": "",
        "version_id": 33296116,
        "package_names": ["com.fazzidice.gn.gmlft"]
      },
      {
        "name": "Robin Hood the Prince",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Divertiti con una storia nuova in 10 coinvolgenti capitoli, con boss ed un nuovo arsenale.",
          "long_description":
              "Aiuta Robin mentre racconta le sue avventure in una terra magica piena di geni malvagi, nemici indemoniati e un sultano pazzo."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2764/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2764/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2764/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2764/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2764/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2764/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2764/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2764/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2764,
        "html5_shortname": "",
        "version_id": 33295846,
        "package_names": ["com.globalfun.robinhood3.premium"]
      },
      {
        "name": "Difesa della Grecia",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Come un vero generale, puoi scegliere i soldati per la battaglia e gli dei per l'alleanza.",
          "long_description":
              "A ogni turno, il nemico colpisce da una direzione diversa, il che ti costringerà ad adottare una strategia diversa ogni volta!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2650/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2650/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2650/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2650/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2650/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2650/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2650/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2650/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2650,
        "html5_shortname": "",
        "version_id": 33296076,
        "package_names": ["com.appsministry.gd"]
      },
      {
        "name": "Gumball Racing",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Delle corse nello straordinario mondo di Gumball!\r\n\r\n\r\n",
          "long_description":
              "Delle corse nello straordinario mondo di Gumball!\r\n\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3295/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3295/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3295/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3295/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3295/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3295/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3295/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3295/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3295,
        "html5_shortname": "",
        "version_id": 33296034,
        "package_names": ["com.globalfun.gumballracing.premium"]
      },
      {
        "name": "GLOBALFUN Hero Stick Craft",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2251/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2251/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2251/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2251/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2251/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2251/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2251/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2251/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2251,
        "html5_shortname": "",
        "version_id": 33296016,
        "package_names": ["com.phamtastic.herostickcraft"]
      },
      {
        "name": "Babbo Natale saltellante",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Sfida la gravità ed evita che Babbo Natale cada sulle letali e pericolose punte ghiacciate.",
          "long_description":
              "L'obiettivo di questo gioco di Babbo Natale è far evitare al nostro buontempone le punte ghiacciate mentre se va in giro saltellando."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2643/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2643/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2643/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2643/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2643/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2643/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2643/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2643/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2643,
        "html5_shortname": "",
        "version_id": 33300476,
        "package_names": ["com.fazzidice.js.gmlft"]
      },
      {
        "name": "Lines",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "\"Lines\" è un gioco zen astratto, in cui la forma è importante quanto la funzione.",
          "long_description":
              "Il colore che domina vince. Lines è il gioco ideale da giocare in viaggio o quando si ha qualche momento libero. "
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2233/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2233/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2233/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2233/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2233/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2233/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2233/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2233/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2233,
        "html5_shortname": "",
        "version_id": 33290284,
        "package_names": ["com.gamious.lines"]
      },
      {
        "name": "Masha e l'Orso: Ricerca e salvataggio",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description":
              "La coraggiosa Masha è saltata sulla mongolfiera per salvare i conigli che attendono il suo aiuto.",
          "long_description":
              "Aiuta Masha a salvare i conigli, in questo adorabile gioco stile cartone animato! Con tante bellissime immagini tratte da Masha e Orso."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2663/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2663/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2663/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2663/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2663/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2663/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2663/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2663/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2663,
        "html5_shortname": "",
        "version_id": 33295670,
        "package_names": ["com.appsministry.mashagame"]
      },
      {
        "name": "Monster Blast",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description": "Polverizza i mostri!!",
          "long_description":
              "Nelle ambientazioni da fumetto di Monster Blast incontrerai il pericolo da vicino, ma ti divertirai comunque un sacco!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2983/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2983/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2983/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2983/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2983/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2983/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2983/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2983/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2983,
        "html5_shortname": "",
        "version_id": 33295618,
        "package_names": ["com.globalfun.monster.blast"]
      },
      {
        "name": "Newton Racing",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Il gioco di corse che ti permette di scegliere i veicoli che preferisci!",
          "long_description":
              "Il gioco di corse che ti permette di scegliere l'auto che preferisci, come ad esempio un Monster Truck!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2705/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2705/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2705/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2705/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2705/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2705/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2705/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2705/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2705,
        "html5_shortname": "",
        "version_id": 33300486,
        "package_names": ["com.ak.newtonrace.gl"]
      },
      {
        "name": "Owen’s Odyssey",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Owen's Odyssey è la storia di un ragazzo che, per ripararsi da una tempesta, si rifugia in un luogo pericoloso",
          "long_description":
              "Controlli touch intuitivi, battaglie contro i boss, monete, segreti, difficoltà e 40 livelli"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2246/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2246/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2246/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2246/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2246/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2246/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2246/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2246/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2246,
        "html5_shortname": "",
        "version_id": 31962707,
        "package_names": ["com.stencyl.owen"]
      },
      {
        "name": "Pengu Wars",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description": "Diventa un eroe delle guerre Pengu!",
          "long_description":
              "Un gioco avvincente e pieno d'azione con tanti livelli emozionanti in stile arcade!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2382/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2382/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2382/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2382/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2382/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2382/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2382/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2382/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2382,
        "html5_shortname": "",
        "version_id": 33296114,
        "package_names": ["com.dutyfarm.penguwars"]
      },
      {
        "name": "Powerpuff Girls: Mojo Madness",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "rating": "5",
        "descriptions": {
          "short_description": "Salva la civiltà prima di andare a dormire!",
          "long_description":
              "Il malvagio Mojo Jojo ha escogitato un piano per dominare il mondo e tocca alle Superchicche impedirglielo!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2548/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2548/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2548/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2548/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2548/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2548/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2548/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2548/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2548,
        "html5_shortname": "",
        "version_id": 33295438,
        "package_names": ["com.globalfun.powerpuff.premium"]
      },
      {
        "name": "RoShamBo Fighters",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "RoShamBo Fighters: gioco di combattimenti arcade in 2D in stile sasso-carta-forbici.",
          "long_description":
              "Dopo anni di allenamento alla Scuola della roccia, sei pronto a scatenare il tuo leggendario Kamehameha e il Pugno del drago sui tuoi nemici!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2325/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2325/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2325/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2325/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2325/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2325/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2325/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2325/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2325,
        "html5_shortname": "",
        "version_id": 31962697,
        "package_names": ["com.phamtastic.roshamboefigs"]
      },
      {
        "name": "Ronaldo: Kick 'N' Run",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "CR7 Cristiano Ronaldo! Divertentissime partite di calcio, rapide e impegnative! Giochiamo!\r\n\r\n\r\n\r\n",
          "long_description":
              "CR7 Cristiano Ronaldo! Inizia la partita: calcia e corri! Giochiamo! Grafica e giocabilità spettacolari\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3527/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3527/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3527/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3527/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3527/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3527/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3527/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3527/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3527,
        "html5_shortname": "",
        "version_id": 33165937,
        "package_names": ["com.hugogames.daybrook"]
      },
      {
        "name": "Road to Kill",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "9+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Carri armati, jet militari: c'è davvero di tutto!\r\n\r\n",
          "long_description":
              "Un gioco in stile arcade divertentissimo ed estremamente coinvolgente.\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3273/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3273/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3273/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3273/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3273/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3273/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3273/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3273/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3273,
        "html5_shortname": "",
        "version_id": 33295988,
        "package_names": ["com.fazzidice.roadtokill.premium"]
      },
      {
        "name": "Safe Cracker",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description":
              "128 livelli d'azione rompicapo sparsi su 16 strati diversi.",
          "long_description":
              "128 livelli rompicapo sparsi su 16 strati diversi e il caveau più sicuro del mondo"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2259/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2259/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2259/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2259/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2259/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2259/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2259/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2259/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2259,
        "html5_shortname": "",
        "version_id": 33296100,
        "package_names": ["org.supergonk.safecrackerpremium"]
      },
      {
        "name": "Scooby Doo - Shaggy & The Ghost Blocks",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Scooby Doo! Mi servi tu! Shaggy è intrappolato in una casa infestata!\r\n\r\n",
          "long_description":
              "Scappa da ogni livello costruendoti una via di fuga! Usa i blocchi per costruire una struttura e aiutare Shaggy a uscire.\r\n\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3470/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3470/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3470/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3470/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3470/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3470/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3470/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3470/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3470,
        "html5_shortname": "",
        "version_id": 33294974,
        "package_names": ["Shaggy.Builder"]
      },
      {
        "name": "Scooby Doo! Saving Shaggy",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description":
              "Sconfiggi i demoni che strisciano nel buio, mentre cerchi la chiave per liberare il tuo amico Shaggy.",
          "long_description":
              "Fatti strada in 3 mondi diversi e raccogli i dolcetti di Scooby in ogni livello per ottenere un grande bonus."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2644/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2644/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2644/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2644/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2644/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2644/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2644/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2644/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2644,
        "html5_shortname": "",
        "version_id": 33294946,
        "package_names": ["com.globalfun.scooby.premium"]
      },
      {
        "name": "Shifters",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description":
              "Arrivano gli Shifter sul tuo telefonino: si spingono a vicenda, saltando e sorridendo!\r\n\r\n\r\n",
          "long_description":
              "Arrivano gli Shifter sul tuo telefonino: si spingono a vicenda mentre saltano e sorridono!\r\n\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3464/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3464/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3464/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3464/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3464/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3464/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3464/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3464/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3464,
        "html5_shortname": "",
        "version_id": 33295898,
        "package_names": ["com.globalfun.shifters"]
      },
      {
        "name": "GLOBALFUN Siesta Slots",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "9+",
        "size": "N/A",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3250/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3250/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3250/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3250/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3250/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3250/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3250/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3250/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3250,
        "html5_shortname": "",
        "version_id": 33300356,
        "package_names": ["com.siesta7.siestaslots"]
      },
      {
        "name": "Ski Safari: Adventure Time",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Adventure Time incontra Ski Safari! Lanciati sulla pista da sci con Finn o BMO!\r\n\r\n",
          "long_description":
              "Salta a bordo con Jake, LSP, Marceline, la principessa Gommarosa, Gunter e molti altri ancora!\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3473/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3473/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3473/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3473/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3473/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3473/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3473/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3473/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3473,
        "html5_shortname": "",
        "version_id": 33295696,
        "package_names": ["com.turner.atskisafari"]
      },
      {
        "name": "Slugterra: Acqua Tetra",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Aiuta Eli Shane a difendere le 99 caverne e a diventare il miglior slugslinger!",
          "long_description":
              "In questo incredibile mondo, le munizioni sono vive e solo i più rapidi possono sopravvivere. Raccogli un arsenale di piccoli animaletti chiamati slug."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2632/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2632/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2632/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2632/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2632/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2632/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2632/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2632/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2632,
        "html5_shortname": "",
        "version_id": 33300474,
        "package_names": [
          "com.appsministry.gameloft.slugterra",
          "com.appsministry.appsborg.slugterra"
        ]
      },
      {
        "name": "Steven Universe: Attacco alla Luce",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Questo sì che è un GdR con i fiocchi e i controfiocchi! Preparati all'azione con ATTACCO ALLA LUCE!",
          "long_description":
              "Unisci le forze e gioca con Garnet, Pearl, Amethyst e Steven per bloccare una potentissima arma Gemma, in Attack the Light!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3116/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3116/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3116/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3116/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3116/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3116/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3116/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3116/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3116,
        "html5_shortname": "",
        "version_id": 33295616,
        "package_names": ["com.turner.stevenrpg"]
      },
      {
        "name": "Subway Surfers",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "rating": "5",
        "descriptions": {
          "short_description":
              "Aiuta Jake, Tricky e Fresh a sfuggire allo scontroso ispettore e al suo cane!\r\n\r\n",
          "long_description":
              "Corri più veloce che puoi! Schiva i treni in arrivo! Aiuta Jake, Tricky e Fresh a sfuggire allo scontroso controllore e al suo cane!\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3340/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3340/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3340/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3340/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3340/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3340/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3340/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3340/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3340,
        "html5_shortname": "",
        "version_id": 33290290,
        "package_names": ["com.kiloo.subwaysurf"]
      },
      {
        "name": "Distruggi le zollette di zucchero",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description":
              "Tieni sveglia la mente con questo divertente e intuitivo rompicapo.",
          "long_description":
              "Partendo da semplici rompicapi monocolore, presto ti ritroverai a creare forme e oggetti multicolori completamente in 3D."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2551/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2551/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2551/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2551/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2551/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2551/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2551/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2551/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2551,
        "html5_shortname": "",
        "version_id": 33295908,
        "package_names": ["com.iphonegamesapps.sugarcubessmash"]
      },
      {
        "name": "Subway Surfers Halloween",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "rating": "2.5",
        "descriptions": {
          "short_description":
              "Aiuta Jake, Tricky e Fresh a sfuggire allo scontroso ispettore e al suo cane!\r\n\r\n",
          "long_description":
              "Aiuta Jake, Tricky e Fresh a sfuggire allo scontroso controllore e al suo cane!\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3453/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3453/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3453/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3453/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3453/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3453/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3453/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3453/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3453,
        "html5_shortname": "",
        "version_id": 33290456,
        "package_names": ["com.kiloo.subwaysurf"]
      },
      {
        "name": "Sweet Candy",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Delle caramelle colorate scoppieranno quando ne allineerai tre dello stesso colore.",
          "long_description":
              "Non hai limiti di tempo, devi solo completare l'operazione entro un determinato numero di mosse. Ottieni 3 stelle in ogni livello per sbloccarne di nuovi!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2749/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2749/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2749/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2749/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2749/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2749/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2749/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2749/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2749,
        "html5_shortname": "",
        "version_id": 33296078,
        "package_names": ["com.fazzidice.sc.gmlf.premium"]
      },
      {
        "name": "Tank VS Aliens ",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Questo gioco è semplice e facile da giocare, lo adorerai!",
          "long_description":
              "Grafica splendida, musiche travolgenti, comandi fluidi e livelli di divertimento a non finire"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2265/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2265/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2265/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2265/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2265/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2265/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2265/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2265/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2265,
        "html5_shortname": "",
        "version_id": 33296004,
        "package_names": ["com.fazzidice.tva.premium"]
      },
      {
        "name": "GLOBALFUN The Amazing Thief",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2326/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2326/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2326/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2326/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2326/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2326/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2326/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2326/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2326,
        "html5_shortname": "",
        "version_id": 31962687,
        "package_names": ["com.approw.amazingthief"]
      },
      {
        "name": "GLOBALFUN The Smurfs: the four seasons",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3518/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3518/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3518/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3518/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3518/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3518/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3518/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3518/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3518,
        "html5_shortname": "",
        "version_id": 33295450,
        "package_names": ["com.taptaptales.thesmurfsgamesforkids.oem"]
      },
      {
        "name": "GLOBALFUN Tom & Jerry: Mouse Maze",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "rating": "4.91",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2616/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2616/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2616/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2616/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2616/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2616/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2616/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2616/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2616,
        "html5_shortname": "",
        "version_id": 33290458,
        "package_names": ["com.globalfun.tj2015.premium"]
      },
      {
        "name": "Tom & Jerry: Labirinto – Christmas Edition",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Tom e Jerry sono tornati, in questa sfida frenetica fra gatto e topo!\r\n\r\n",
          "long_description":
              "Aiuta Jerry a raccogliere tutto il formaggio senza farsi prendere da Tom!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3521/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3521/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3521/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3521/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3521/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3521/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3521/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3521/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3521,
        "html5_shortname": "",
        "version_id": 33295444,
        "package_names": []
      },
      {
        "name": "Tom & Jerry: Labirinto – Deluxe",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Tom e Jerry sono tornati, in questa sfida frenetica fra gatto e topo!\r\n\r\n\r\n",
          "long_description":
              "Aiuta Jerry a correre attraverso 60 emozionanti livelli raccogliendo il formaggio, mentre cerca di evitare Tom, in questo classico gioco di gatto e topo.\r\n\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3701/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3701/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3701/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3701/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3701/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3701/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3701/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3701/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3701,
        "html5_shortname": "",
        "version_id": 33165935,
        "package_names": ["com.globalfun.tj2015.deluxe"]
      },
      {
        "name": "Tom & Jerry: Labirinto - Halloween",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Prova spaventose emozioni e brividi spettrali, in questa edizione speciale di Tom & Jerry di Halloween!\r\n\r\n",
          "long_description":
              "Prova dei brividi spettrali, in questa edizione speciale di Halloween!\r\n\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3455/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3455/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3455/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3455/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3455/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3455/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3455/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3455/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3455,
        "html5_shortname": "",
        "version_id": 33295456,
        "package_names": ["com.globalfun.tj2015.halloween"]
      },
      {
        "name": "GLOBALFUN Tom & Jerry: Mouse Maze - Summer Edition",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3303/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3303/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3303/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3303/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3303/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3303/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3303/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3303/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3303,
        "html5_shortname": "",
        "version_id": 33295614,
        "package_names": ["com.globalfun.tj2015.summer"]
      },
      {
        "name": "Viper Wars",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Il Robot Padrone è andato su tutte le furie e solo tu puoi fermarlo!\r\n\r\n",
          "long_description":
              "Il robot padrone è andato su tutte le furie e solo tu puoi fermarlo! Scegli la tua nave e salva la razza umana!\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3908/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3908/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3908/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3908/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3908/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3908/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3908/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3908/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3908,
        "html5_shortname": "",
        "version_id": 33165933,
        "package_names": ["com.globalfun.viperwars.premium"]
      },
      {
        "name": "Toon Cup",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Gioca a calcetto con Ben 10 e gli altri personaggi di Cartoon Network in questo animatissimo gioco.",
          "long_description":
              "Gioca a calcetto in squadra con il tuo personaggio di Cartoon Network preferito in questo animatissimo gioco."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2806/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2806/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2806/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2806/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2806/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2806/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2806/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2806/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2806,
        "html5_shortname": "",
        "version_id": 33300434,
        "package_names": ["Copa.Toon"]
      },
      {
        "name": "We Bare Bears - Quest per Nom Nom",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Nom Nom sta nuovamente facendo i suoi trucchetti!",
          "long_description":
              "Nom Nom sta nuovamente combinando qualcuno dei suoi trucchetti! Aiuta gli orsi ad inseguire Nom Nom tra i vari livelli, mentre evitano gli ostacoli.\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3603/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3603/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3603/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3603/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3603/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3603/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3603/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3603/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3603,
        "html5_shortname": "",
        "version_id": 33295840,
        "package_names": ["com.globalfun.wbb.premium"]
      },
      {
        "name": "Occhio alla Talpa",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description":
              "L'Occhio alla Talpa originale da sala giochi - ora sul tuo telefonino.",
          "long_description":
              "Talpe di diverso tipo, nuovi tipi di martelli, minigiochi e modalità di gioco extra"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2737/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2737/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2737/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2737/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2737/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2737/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2737/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2737/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2737,
        "html5_shortname": "",
        "version_id": 33295980,
        "package_names": ["kiloo.whac.pay"]
      },
      {
        "name": "What's up Snoopy",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description": "Divertiti con gli amici di Snoopy!\r\n",
          "long_description":
              "Unisciti ai Peanuts e metti alla prova le tue abilità in una raccolta di avvincenti minigiochi in What's Up, Snoopy?\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3826/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3826/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3826/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3826/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3826/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3826/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3826/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3826/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3826,
        "html5_shortname": "",
        "version_id": 33294908,
        "package_names": ["com.turner.peanuts"]
      },
      {
        "name": "Worldly",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Worldly è un modo divertente di conoscere il nostro pianeta e le sue nazioni!",
          "long_description":
              "Worldly è un modo divertente di conoscere il nostro pianeta e le sue nazioni! Vola in giro per il mondo in un viaggio di scoperta."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2628/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2628/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2628/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2628/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2628/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2628/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2628/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2628/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2628,
        "html5_shortname": "",
        "version_id": 33300472,
        "package_names": ["org.supergonk.worldlypremium"]
      },
      {
        "name": "3D XRush",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description":
              "Questo gioco simile a Tron spingerà le tue abilità al massimo!",
          "long_description":
              "Hai la capacità di pilotare il tuo caccia X-wing nell'ambiente ostile di questi pianeti?"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2703/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2703/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2703/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2703/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2703/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2703/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2703/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2703/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2703,
        "html5_shortname": "",
        "version_id": 33295676,
        "package_names": ["com.fazzidice.xrush3d.gmlft.premium"]
      },
      {
        "name": "GLOBALFUN Ziggy Kids",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2731/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2731/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2731/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2731/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2731/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2731/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2731/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2731/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2731,
        "html5_shortname": "",
        "version_id": 33295886,
        "package_names": ["com.iphonegamesapps.ziggykids"]
      },
      {
        "name": "Ziggy Road",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Stai per entrare in una città trafficata. Affrettati a riprendere il controllo dell'auto in fuga!",
          "long_description":
              "Sfortunatamente, un guasto alla tua auto le impedisce di fermarsi! Affrettati a riprendere il controllo dell'auto in fuga e evita gli ostacoli zigzagando!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2607/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2607/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2607/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2607/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2607/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2607/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2607/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2607/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2607,
        "html5_shortname": "",
        "version_id": 33296048,
        "package_names": ["com.iphonegamesapps.ziggyroad"]
      },
      {
        "name": "Zombie Race",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "8+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Questo mondo post-apocalittico si sta riempiendo di zombie e solo in pochi sopravvivranno!",
          "long_description":
              "Questo mondo post-apocalittico si sta riempiendo di zombie e solo i più abili e valorosi sopravvivranno!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3216/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3216/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3216/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3216/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3216/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3216/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3216/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3216/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3216,
        "html5_shortname": "",
        "version_id": 33294978,
        "package_names": ["com.ak.zombie.gl"]
      },
      {
        "name": "Zombie Rising",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "9+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "È l'apocalisse degli zombie! Uccidere o essere ucciso è l'unico modo per sopravvivere!\r\n",
          "long_description":
              "Un gioco in stile arcade divertentissimo ed estremamente coinvolgente.\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3287/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3287/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3287/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3287/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3287/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3287/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3287/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3287/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3287,
        "html5_shortname": "",
        "version_id": 33295894,
        "package_names": ["com.fazzidice.zombierising.gmlft"]
      },
      {
        "name": "Zoom Bug",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description":
              "Elimina tutte le palline prima che raggiungano un cratere nero.",
          "long_description":
              "Azione rapida nella città delle fate! Elimina tutte le palline prima che raggiungano un cratere nero."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2765/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2765/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2765/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2765/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2765/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2765/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2765/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2765/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2765,
        "html5_shortname": "",
        "version_id": 33295682,
        "package_names": ["ua.netlizard.zoombug"]
      },
      {
        "name": "GLOBALFUN iO",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2216/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2216/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2216/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2216/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2216/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2216/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2216/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2216/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2216,
        "html5_shortname": "",
        "version_id": 33290282,
        "package_names": ["com.gamious.io"]
      },
      {
        "name": "GT Racing 2 - Un'esperienza di guida totale",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "rating": "3.5",
        "descriptions": {
          "short_description":
              "Il famoso simulatore di corse torna con più auto e piste, e con una grafica migliorata.\r\n",
          "long_description":
              "Il seguito del più completo simulatore di corse su cellulare\r\nScegli tra 25 auto famose dell'universo di GT, tra cui Ferrari e Ford\r\n9 piste avvincenti in ambienti diversi: città, deserti, neve, eccetera \r\nUn'entusiasmante modalità Carriera ricca di eventi\r\nProva tante modalità di gioco, dalla gara classica a quella a eliminazione\r\nUn'esperienza di guida realistica, grazie al nuovo motore grafico\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1679/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/1679/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/1679/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1679/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1679/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1679/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1679/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1679/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 1679,
        "html5_shortname": "",
        "version_id": 33196520,
        "package_names": [
          "com.gameloft.android.GAND.GloftGT2M",
          "com.gameloft.android.BFLL.GloftGT2M",
          "com.gameloft.android.GloftGT2M"
        ]
      },
      {
        "name": "Gameloft Classics: Action",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Riscopri dei classici per cellulari di Gameloft, al loro debutto sugli smartphone! Divertiti con GANGSTAR 2: KINGS OF L.A., HERO OF SPARTA, SOUL OF DARKNESS, MOTOCROSS TRIAL EXTREME e ZOMBIE INFECTION, tutti nel loro splendore retrò.",
          "long_description":
              "Tutto il divertimento dei classici action di Gameloft, ora su smartphone!\r\nGANGSTAR 2: KINGS OF L.A.: auto, soldi, notorietà... Il crimine, paga!\r\nHERO OF SPARTA: combatti contro creature mitologiche in questa epica odissea!\r\nSOUL OF DARKNESS: sconfiggi l'oscurità usando la spada e la magia.\r\nMOTOCROSS TRIAL EXTREME: divertiti con le acrobazie più folli con la tua moto!\r\nZOMBIE INFECTION: salva New York dagli zombi assetati di sangue."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3454/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/3454/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/3454/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3454/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3454/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3454/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3454/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3454/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 3454,
        "html5_shortname": "",
        "version_id": 33831216,
        "package_names": ["com.gameloft.android.GAND.GloftGLCA"]
      },
      {
        "name": "Gameloft Classics: Arcade",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "",
        "size": "N/A",
        "rating": "4.75",
        "descriptions": {
          "short_description":
              "Divertiti con questi 5 titoli classici Gameloft di successo, per la prima volta sugli smartphone!",
          "long_description":
              "Tutto il divertimento dei giochi originali, ora su smartphone!\r\nBLOCK BREAKER DELUXE 2 - Rompi-mattoncini con battaglie boss e altro ancora\r\nBUBBLE BASH 2 - Un coloratissimo scoppiabolle tropicale\r\nDIAMOND RUSH - Trova tesori nascosti tra rovine e templi antichi\r\nBRAIN CHALLENGE 3: IL NUOVO ALLENA-MENTE! - Allena la tua mente divertendoti\r\nMIAMI NIGHTS 2: THE CITY IS YOURS! - Trova fama, fortuna e amore"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3389/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/3389/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/3389/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3389/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3389/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3389/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3389/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3389/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 3389,
        "html5_shortname": "",
        "version_id": 33196512,
        "package_names": ["com.gameloft.android.GAND.GloftGLPH"]
      },
      {
        "name": "Gameloft Classics: Shooters",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Fai un tuffo nel passato con 5 sparatutto retrò mai usciti prima su smartphone!",
          "long_description":
              "Gli sparatutto intramontabili di Gameloft, ricchi di azione e ora su smartphone!\r\nN.O.V.A. NEAR ORBIT VANGUARD ALLIANCE - Difendi l'umanità in un FPS sci-fi\r\nZOMBIEWOOD - Diventa una celebrità in una serie di film d'azione pieni di zombi\r\nWILD WEST GUNS - Esplora una vasta frontiera e domina il West dei pistoleri\r\nALIEN QUARANTINE - Combatti per sopravvivere a orde infinite di orrori spaziali!\r\nMODERN COMBAT 2: BLACK PEGASUS - Azione tattica a squadre in tutto il mondo"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3591/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3591/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3591/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3591/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3591/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3591/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3591/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3591/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3591,
        "html5_shortname": "",
        "version_id": 33831220,
        "package_names": ["com.gameloft.android.GAND.GloftGCSH"]
      },
      {
        "name": "Gangstar City",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Forma la banda di gangster più tosta e diventa il boss della malavita dell'America occidentale.",
          "long_description":
              "La famosa serie Gangstar torna in un simulatore di bande criminali per cellulari\r\nEntra nel mondo della malavita di Los Angeles in 4 mappe diverse\r\nPersonalizza il tuo covo, apri delle attività e organizza grandi feste\r\nRecluta i gangster per formare la banda più tosta e proteggere il tuo territorio\r\nAccetta lavori sporchi per dominare la città e diventare il boss più temuto\r\nSblocca decine di potenziamenti per te, il tuo covo e i membri della tua gang"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1516/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/1516/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/1516/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1516/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1516/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1516/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1516/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1516/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 1516,
        "html5_shortname": "",
        "version_id": 33831224,
        "package_names": [
          "com.gameloft.android.GAND.GloftGATP",
          "com.gameloft.android.ALCH.GloftGATP"
        ]
      },
      {
        "name": "Green Farm 3",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Riscopri l'avventura nella tua fattoria e diventa il contadino più famoso nel nuovo Green Farm!",
          "long_description":
              "Riscopri l'avventura nella nuova fattoria di Green Farm!\r\nRiporta il maniero alla sua antica gloria con l'aiuto di amici e vicini!\r\nCompleta tante missioni, con raccolti, mietiture, fai da te e altro ancora.\r\nLa comunità è tutto! Gli amici ti daranno una mano.\r\nComandi più semplici rendono la gestione della fattoria ancora più divertente!\r\nDivertiti in un mondo colorato e scopri tanti divertenti personaggi."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1666/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/1666/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/1666/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1666/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1666/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1666/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1666/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1666/default/screenshot/4.jpg"
          ]
        },
        "product_id": 1666,
        "html5_shortname": "",
        "version_id": 33831228,
        "package_names": [
          "com.gameloft.android.GAND.GloftGF2M",
          "com.gameloft.android.GAND.GloftGH3P",
          "com.gameloft.android.ALCH.GloftGF2M"
        ]
      },
      {
        "name": "1941 Frozen Front",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description":
              "Guida le forze tedesche nella campagna orientale, o difendi la Madre Russia sul fronte sovietico.",
          "long_description":
              "Scegli le tattiche belliche migliori e guadagnati i gradi di un abile comandante con FROZEN FRONT!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2603/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2603/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2603/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2603/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2603/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2603/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2603/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2603/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2603,
        "html5_shortname": "",
        "version_id": 33294912,
        "package_names": ["com.hg.frozenfront"]
      },
      {
        "name": "Aces of the Luftwaffe",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Fai la differenza, mentre la guerra continua a devastare l'Europa!",
          "long_description":
              "Fai la differenza, mentre la guerra continua a devastare l'Europa! Sconfiggi ondate di aerei e carri armati dell'Asse finché non raggiungi i boss nemici."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2377/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2377/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2377/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2377/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2377/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2377/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2377/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2377/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2377,
        "html5_shortname": "",
        "version_id": 26454296,
        "package_names": ["com.hg.aotlpremium"]
      },
      {
        "name": "Alchemic Maze",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "rating": "4.91",
        "descriptions": {
          "short_description":
              "Fai scorrere il liquido nel labirinto inclinando il telefono. Esplora i misteri dell’alchimia!",
          "long_description":
              "Fai scorrere il liquido nel labirinto inclinando il telefono. Esplora i misteri dell’alchimia!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3619/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3619/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3619/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3619/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3619/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3619/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3619/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3619/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3619,
        "html5_shortname": "",
        "version_id": 33165961,
        "package_names": ["com.herocraft.game.full.alchemicmaze"]
      },
      {
        "name": "Beholder",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "12+",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description":
              "Lavori per uno stato distopico e oppressivo e dovrai spiare i tuoi vicini\r\n\r\n",
          "long_description":
              "Lavori per uno stato distopico e oppressivo e dovrai spiare i tuoi vicini.\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3814/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3814/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3814/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3814/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3814/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3814/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3814/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3814/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3814,
        "html5_shortname": "",
        "version_id": 33300404,
        "package_names": ["com.xendex.beholder"]
      },
      {
        "name": "Birds On A Wire",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description":
              "Divertiti aiutando gli uccellini a fuggire, in questo gioco arcade stile Zuma.\r\n",
          "long_description":
              "Divertiti in modo melodioso aiutando gli uccellini a fuggire, in questo gioco arcade stile Zuma.\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3902/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3902/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3902/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3902/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3902/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3902/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3902/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3902/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3902,
        "html5_shortname": "",
        "version_id": 33300422,
        "package_names": ["com.herocraft.game.birdsonwire"]
      },
      {
        "name": "Boulder Rush",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Il martello e il piccone sono pronti: è ora di scavare!\r\n\r\n",
          "long_description":
              "Diventa un simpatico nano e salva tutti gli animali in pericolo!\r\n\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3319/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3319/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3319/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3319/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3319/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3319/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3319/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3319/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3319,
        "html5_shortname": "",
        "version_id": 33300366,
        "package_names": ["com.tivola.dwarf.offline"]
      },
      {
        "name": "Bridge Constructor Medieval",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Tuffati nel medioevo e prova il seguito del famoso gioco!",
          "long_description":
              "Tuffati nel medioevo e prova il seguito del famoso gioco!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2996/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2996/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2996/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2996/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2996/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2996/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2996/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2996/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2996,
        "html5_shortname": "",
        "version_id": 33295460,
        "package_names": ["com.headupgames.bridgeconstructormedieval"]
      },
      {
        "name": "Bridge Constructor Playground",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Bridge Constructor Playground permette ai giocatori di tutte le età di diventare dei costruttori di ponti provetti!",
          "long_description":
              "Bridge Constructor Playground permette ai giocatori di tutte le età di diventare dei costruttori di ponti provetti!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2974/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2974/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2974/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2974/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2974/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2974/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2974/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2974/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2974,
        "html5_shortname": "",
        "version_id": 33300436,
        "package_names": ["com.headupgames.bridgeconstructorplayground"]
      },
      {
        "name": "Bridge Constructor Stunts",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "La serie di successo Bridge Constructor, la numero 1 su Google Play Store.",
          "long_description":
              "La serie di successo Bridge Constructor, la numero 1 su Google Play Store."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3000/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3000/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3000/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3000/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3000/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3000/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3000/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3000/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3000,
        "html5_shortname": "",
        "version_id": 33295936,
        "package_names": ["com.headupgames.bridgeconstructorstunts"]
      },
      {
        "name": "CaRRage",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "9+",
        "size": "N/A",
        "rating": "4.91",
        "descriptions": {
          "short_description":
              "Un avvincente gioco di corse arcade in un universo post-apocalittico, controllato da piloti folli e senza scrupoli\r\n",
          "long_description":
              "Un'avvincente gioco di corse arcade in un universo postapocalittico, governato da piloti folli e senza scrupoli.\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3903/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3903/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3903/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3903/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3903/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3903/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3903/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3903/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3903,
        "html5_shortname": "",
        "version_id": 33165957,
        "package_names": ["com.herocraft.game.premium.carrage"]
      },
      {
        "name": "Casino Crime",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Gestisci un casinò della malavita e svuota i portafogli dei giocatori!",
          "long_description":
              "Inizia la tua carriera criminale nell'affascinante mondo del gioco d'azzardo! Dai una mano alla fortuna nel tuo casinò e ripulisci gli avventori!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2689/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2689/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2689/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2689/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2689/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2689/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2689/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2689/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2689,
        "html5_shortname": "",
        "version_id": 33295762,
        "package_names": ["com.hg.casinocrime"]
      },
      {
        "name": "Chess and Mate",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "descriptions": {
          "short_description": "L'app di scacchi per tutta la famiglia.",
          "long_description": "L'app di scacchi per tutta la famiglia."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3125/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3125/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3125/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3125/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3125/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3125/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3125/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3125/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3125,
        "html5_shortname": "",
        "version_id": 33296140,
        "package_names": ["com.tivola.chess.free"]
      },
      {
        "name": "Clouds & Sheep 2",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Il tuo morbidoso gregge preferito è tornato, ed è più vivace che mai!",
          "long_description":
              "Il tuo morbidoso gregge preferito è tornato, ed è più vivace che mai!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3001/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3001/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3001/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3001/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3001/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3001/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3001/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3001/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3001,
        "html5_shortname": "",
        "version_id": 33295416,
        "package_names": ["com.hg.cloudsandsheep2"]
      },
      {
        "name": "Clouds & Sheep",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Nutrile, gioca con loro, forma delle coppie per generare dei cuccioli e diventa il miglior pastore!",
          "long_description":
              "Prenditi cura di un adorabile gregge di pecorelle. Nutrile, gioca con loro, forma delle coppie per generare dei cuccioli e diventa il miglior pastore di sempre!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1644/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/1644/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/1644/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1644/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1644/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1644/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1644/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1644/default/screenshot/4.jpg"
          ]
        },
        "product_id": 1644,
        "html5_shortname": "",
        "version_id": 33295848,
        "package_names": [
          "com.hg.cloudsandsheepnobilling",
          "com.hg.cloudsandsheep"
        ]
      },
      {
        "name": "DogHotel - My boarding kennel",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "descriptions": {
          "short_description": "Apri subito la tua pensione per cani! ",
          "long_description":
              "Apri subito la tua pensione per cani! Accudisci tutti i cagnolini affidati alle tue cure."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3108/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3108/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3108/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3108/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3108/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3108/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3108/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3108/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3108,
        "html5_shortname": "",
        "version_id": 33296134,
        "package_names": ["com.tivola.doghotel.underground"]
      },
      {
        "name": "Dolphins of the Caribbean",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description": "FAVOLOSE VACANZE NEL CENTRO PER DELFINI",
          "long_description":
              "Sull'isola di Tirimoa, una simulazione di animali 3D si unisce a un'entusiasmante storia di pirati. Occupati dei delfini e trova il tesoro!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3189/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3189/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3189/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3189/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3189/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3189/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3189/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3189/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3189,
        "html5_shortname": "",
        "version_id": 33296082,
        "package_names": ["com.tivola.dolphins.full.offline"]
      },
      {
        "name": "Dream Job Venerinarian",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "descriptions": {
          "short_description": "GESTISCI UNA PICCOLA CLINICA VETERINARIA",
          "long_description": "GESTISCI UNA PICCOLA CLINICA VETERINARIA\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3188/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3188/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3188/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3188/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3188/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3188/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3188/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3188/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3188,
        "html5_shortname": "",
        "version_id": 33296152,
        "package_names": ["com.tivola.veterinary"]
      },
      {
        "name": "Dreamcage number 26",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description":
              "Un mondo strano pieno di gabbie diverse, ognuna con un proprio numero.\r\n",
          "long_description":
              "Un mondo strano pieno di gabbie diverse, ognuna con un proprio numero. Bello e suggestivo, ma anche inquietante e avvincente...\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3904/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3904/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3904/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3904/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3904/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3904/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3904/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3904/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3904,
        "html5_shortname": "",
        "version_id": 33300424,
        "package_names": ["air.com.sevenbulls.DreamcagePremium"]
      },
      {
        "name": "Dreamjob: Kid's Doctor",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Calati nel ruolo di una pediatra e occupati delle piccole e grandi bue dei tuoi pazienti.",
          "long_description":
              "Calati nel ruolo di una pediatra e occupati delle piccole e grandi bue dei tuoi pazienti."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3223/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3223/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3223/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3223/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3223/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3223/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3223/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3223/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3223,
        "html5_shortname": "",
        "version_id": 33296154,
        "package_names": ["de.tivola.mtjk"]
      },
      {
        "name": "Dynamite Fishing - World Games",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "9+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Esplora subito la più incredibile avventura di pesca sportiva!",
          "long_description":
              "Scopri subito la più incredibile avventura di pesca sportiva!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3112/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3112/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3112/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3112/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3112/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3112/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3112/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3112/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3112,
        "html5_shortname": "",
        "version_id": 33296136,
        "package_names": ["com.hg.dfwg"]
      },
      {
        "name": "Experiment",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Ci serve il tuo aiuto, in questo innovativo gioco match-3!",
          "long_description":
              "Serve il tuo aiuto, in questo originalissimo gioco! Un giorno Sophie, l'eroina di Experiment, si sveglia e si ritrova improvvisamente in una stanza vuota."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2998/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2998/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2998/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2998/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2998/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2998/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2998/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2998/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2998,
        "html5_shortname": "",
        "version_id": 33300440,
        "package_names": ["com.headupgames.experiment"]
      },
      {
        "name": "Fantasy Baby Animals",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description":
              "Dei cuccioli di animali magici hanno bisogno del tuo aiuto. Gioca con draghi, unicorni e fenici!",
          "long_description":
              "Dei cuccioli di animali magici hanno bisogno del tuo aiuto. Gioca con draghi, unicorni e fenici!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3122/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3122/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3122/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3122/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3122/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3122/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3122/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3122/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3122,
        "html5_shortname": "",
        "version_id": 33296080,
        "package_names": ["com.tivola.fantasy.offline"]
      },
      {
        "name": "Farm Invasion USA",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Difendi il tuo raccolto dall'alieno più folle dell'universo!",
          "long_description":
              "Difendi il tuo raccolto dall'alieno più folle dell'universo!\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1700/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/1700/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/1700/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1700/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1700/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1700/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1700/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1700/default/screenshot/4.jpg"
          ]
        },
        "product_id": 1700,
        "html5_shortname": "",
        "version_id": 33295852,
        "package_names": ["com.hg.farminvasion"]
      },
      {
        "name": "Foosball Cup World",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Foosball Cup - World: l'esperienza di calcio balilla definitiva!\r\n\r\n",
          "long_description":
              "Scegli la tua squadra e gli avversari, poi scegli tra Partita veloce, Sfida o Tornei."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3355/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3355/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3355/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3355/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3355/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3355/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3355/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3355/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3355,
        "html5_shortname": "",
        "version_id": 33295892,
        "package_names": ["com.ludusstudio.zariba.fcw"]
      },
      {
        "name": "HANDYGAMES Grandpa And The Zombies",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3320/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3320/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3320/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3320/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3320/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3320/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3320/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3320/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3320,
        "html5_shortname": "",
        "version_id": 33300368,
        "package_names": ["com.tivola.grandpa.offline"]
      },
      {
        "name": "Guns 'n' Glory Heroes",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Affila le lame, lucida l’armatura, studia le magie e CARICA LE PISTOLE!",
          "long_description":
              "Affila le lame, lucida l’armatura, studia le magie e CARICA LE PISTOLE! Gioca al gioco di strategia difensiva per eccellenza!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2865/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2865/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2865/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2865/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2865/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2865/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2865/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2865/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2865,
        "html5_shortname": "",
        "version_id": 33300490,
        "package_names": ["com.hg.gunsandglory3"]
      },
      {
        "name": "Guns N Glory",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Diventa il più ricco e temuto capo di una gang di banditi del Far West!",
          "long_description":
              "Come sarebbe il Far West senza i suoi carismatici cattivi?"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2329/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2329/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2329/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2329/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2329/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2329/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2329/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2329/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2329,
        "html5_shortname": "",
        "version_id": 33295932,
        "package_names": ["com.hg.gunsandglory"]
      },
      {
        "name": "Guns'N Glory 2",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Scegli da che parte stare e difendi le tue basi sui campi di battaglia della seconda guerra mondiale!",
          "long_description":
              "Bentornato a Guns'n'Glory!\r\nQuesta è la guerra, soldato!\r\nScegli da che parte stare e difendi le tue basi sul campo di battaglia!\r\nImpugna il bazooka e prepara le granate. Stai per entrare nell'appassionante seguito del grande successo di azione e strategia Guns'n'Glory!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1653/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/1653/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/1653/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1653/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1653/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1653/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1653/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1653/default/screenshot/4.jpg"
          ]
        },
        "product_id": 1653,
        "html5_shortname": "",
        "version_id": 33295868,
        "package_names": ["com.hg.gunsandglory2"]
      },
      {
        "name": "Guns'n'Glory Zombies",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "9+",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description":
              "QUATTRO RAGAZZE E UN CAGNOLINO CONTRO I MORTI VIVENTI!",
          "long_description":
              "Guns'n'Glory è tornato, più frenetico, folle e selvaggio che mai! Sei pronto a scatenare l’inferno?"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3156/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3156/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3156/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3156/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3156/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3156/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3156/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3156/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3156,
        "html5_shortname": "",
        "version_id": 33295888,
        "package_names": ["com.hg.gunsandglory4"]
      },
      {
        "name": "Horse Hotel",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Gestisci la tua pensione per cavalli e prenditi cura dei tuoi ospiti.\r\n\r\n\r\n",
          "long_description":
              "Gestisci la tua pensione per cavalli e prenditi cura con amore dei tuoi ospiti.\r\n\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3709/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3709/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3709/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3709/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3709/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3709/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3709/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3709/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3709,
        "html5_shortname": "",
        "version_id": 33295880,
        "package_names": ["com.tivola.horsehotel.offline"]
      },
      {
        "name": "Horse World 3D",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Spazzola e cavalca il tuo cavallo, quando vuoi!",
          "long_description":
              "Spazzola e cavalca il tuo cavallo, ogni volta che vuoi!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3224/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3224/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3224/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3224/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3224/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3224/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3224/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3224/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3224,
        "html5_shortname": "",
        "version_id": 33295706,
        "package_names": ["com.tivola.horseworld.offline"]
      },
      {
        "name": "Horse World: Salto ostacoli",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description":
              "Il tuo sogno si è avverato: prenditi cura dei cavalli e supera i percorsi a ostacoli!\r\n\r\n\r\n",
          "long_description":
              "Il tuo sogno si è avverato: prenditi cura dei cavalli e supera i percorsi a ostacoli!\r\n\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3588/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3588/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3588/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3588/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3588/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3588/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3588/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3588/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3588,
        "html5_shortname": "",
        "version_id": 33295752,
        "package_names": ["com.tivola.ShowJumping.offline"]
      },
      {
        "name": "Jolly Days Farm",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Tutti gli animali si divertono alla Jolly Days Farm!\r\n",
          "long_description":
              "Tutti gli animali si divertono, nella Jolly Days Farm!\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3468/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3468/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3468/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3468/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3468/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3468/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3468/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3468/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3468,
        "html5_shortname": "",
        "version_id": 33295412,
        "package_names": ["com.herocraft.game.full.jollydaysfarm"]
      },
      {
        "name": "Majesty: Fantasy Kingdom Sim",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "9+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Majesty: The Fantasy Kingdom Sim - Inizia una grande avventura in un piccolo telefono!\r\n",
          "long_description":
              "Majesty: The Fantasy Kingdom Sim - Inizia una grande avventura su un piccolo telefono!\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3905/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3905/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3905/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3905/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3905/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3905/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3905/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3905/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3905,
        "html5_shortname": "",
        "version_id": 33295896,
        "package_names": ["com.herocraft.game.majesty"]
      },
      {
        "name": "Ninja Hero Cats",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Unisciti alla battaglia contro i mostri acquatici provenienti da un'altra dimensione!",
          "long_description":
              "Unisciti alla fantastica squadra dei Ninja Hero Cats nella loro coraggiosa battaglia contro i mostri acquatici provenienti da un'altra dimensione!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3111/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3111/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3111/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3111/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3111/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3111/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3111/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3111/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3111,
        "html5_shortname": "",
        "version_id": 33296006,
        "package_names": ["com.hg.ninjaherocats"]
      },
      {
        "name": "PetWorld 3D: My Animal Shelter",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "9+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Tutti gli animali virtuali vogliono essere amati e adottati dai proprietari giusti.",
          "long_description":
              "Tutti gli animali virtuali vogliono essere amati e adottati dai proprietari giusti."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3153/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3153/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3153/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3153/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3153/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3153/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3153/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3153/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3153,
        "html5_shortname": "",
        "version_id": 33296088,
        "package_names": ["com.tivola.petworld.offline"]
      },
      {
        "name": "Pigeon Bomber",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Jack vola sopra la città e il suo obiettivo è defecare su tutto e su tutti.\r\n\r\n\r\n",
          "long_description":
              "Jack sta volando sopra la città e il suo obiettivo è di defecare su tutti e tutto."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3418/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3418/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3418/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3418/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3418/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3418/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3418/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3418/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3418,
        "html5_shortname": "",
        "version_id": 33300374,
        "package_names": ["com.blackmoondev.pigeonBomber"]
      },
      {
        "name": "Red Ball 3",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description":
              "Salva la ragazza in un puzzle a piattaforme a tutto tondo!\r\n",
          "long_description":
              "Salva la tua fidanzata, in questo gioco platform... a tutto tondo!\r\n\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3526/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3526/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3526/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3526/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3526/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3526/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3526/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3526/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3526,
        "html5_shortname": "",
        "version_id": 33296040,
        "package_names": ["com.herocraft.game.full.redball3"]
      },
      {
        "name": "Save the Puppies",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Parti all'avventura e cerca soluzioni a divertenti quesiti rompicapo in un mondo di fumetti colorati!",
          "long_description":
              "Trova i cuccioli perduti, parti all'avventura e cerca soluzioni a divertenti quesiti rompicapo in un mondo di fumetti colorati!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1635/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/1635/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/1635/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1635/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1635/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1635/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1635/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1635/default/screenshot/4.jpg"
          ]
        },
        "product_id": 1635,
        "html5_shortname": "",
        "version_id": 33295728,
        "package_names": ["com.hg.savethepuppies"]
      },
      {
        "name": "Space Expedition",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "9+",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description":
              "Preparati per l'avventura! Indossa la tuta spaziale e carica il tuo pacchetto jet!\r\n\r\n",
          "long_description":
              "Space Expedition è un emozionante gioco platform vecchio stile con un coloratissimo mix di vari generi.\r\n\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3589/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3589/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3589/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3589/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3589/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3589/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3589/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3589/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3589,
        "html5_shortname": "",
        "version_id": 33300380,
        "package_names": ["com.herocraft.game.full.spaceexpedition"]
      },
      {
        "name": "Stage Dive Legends",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Fai vedere a tutti chi è il re dello stage diving in questo folle tour mondiale!",
          "long_description":
              "Fai vedere a tutti chi è il re del crowd surfing in questo folle tour mondiale!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3141/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3141/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3141/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3141/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3141/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3141/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3141/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3141/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3141,
        "html5_shortname": "",
        "version_id": 33296144,
        "package_names": ["com.hg.sdl"]
      },
      {
        "name": "Ferma i robot",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "rating": "5",
        "descriptions": {
          "short_description":
              "I robot stanno conquistando il mondo, invadendo gli ultimi territori liberi.\r\n\r\n",
          "long_description":
              "I robot stanno conquistando il mondo, invadendo gli ultimi territori liberi.\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3459/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3459/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3459/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3459/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3459/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3459/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3459/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3459/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3459,
        "html5_shortname": "",
        "version_id": 33295440,
        "package_names": ["com.ludusstudio.zariba.stoptherobots"]
      },
      {
        "name": "Sunny Hillride",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "In Sunny Hillride, vivrai le folli avventure di una famiglia in vacanza!",
          "long_description":
              "Inizia la vacanza più folle della tua famiglia, con un viaggio molto turbolento!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2999/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2999/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2999/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2999/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2999/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2999/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2999/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2999/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2999,
        "html5_shortname": "",
        "version_id": 33300442,
        "package_names": ["com.headupgames.sunnyhillrideadfree"]
      },
      {
        "name": "Super Party Sports: Football",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Sei pronto per la partita di calcio più scatenata del mondo?",
          "long_description":
              "Sei pronto per la partita di calcio più scatenata del mondo?"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3142/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3142/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3142/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3142/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3142/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3142/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3142/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3142/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3142,
        "html5_shortname": "",
        "version_id": 33295900,
        "package_names": ["com.hg.spsfootball"]
      },
      {
        "name": "Tap Tap Builder",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Tap Tap Builder ti invita a costruire la città dei tuoi sogni e a diventarne il sindaco!\r\n",
          "long_description":
              "Tap Tap Builder ti invita a costruire la città dei tuoi sogni e a diventarne il sindaco!\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3622/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3622/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3622/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3622/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3622/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3622/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3622/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3622/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3622,
        "html5_shortname": "",
        "version_id": 33295996,
        "package_names": ["com.herocraft.game.premium.taptapbuilder"]
      },
      {
        "name": "HANDYGAMES Tattoo Tycoon",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Crea il tuo studio di tatuaggi e diventa un artista nella decorazione del corpo!",
          "long_description":
              "Crea il tuo studio di tatuaggi e diventa un artista nella decorazione del corpo! Crea i tuoi stili, espandi lo studio e rendilo famoso!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1686/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/1686/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/1686/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1686/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1686/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1686/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1686/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1686/default/screenshot/4.jpg"
          ]
        },
        "product_id": 1686,
        "html5_shortname": "",
        "version_id": 33253318,
        "package_names": ["com.hg.tattootycoon"]
      },
      {
        "name": "Teddy the Panda",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "descriptions": {
          "short_description": "Teddy il panda è così simpatico e coccoloso!",
          "long_description": "Teddy il panda è così simpatico e coccoloso!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3006/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3006/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3006/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3006/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3006/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3006/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3006/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3006/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3006,
        "html5_shortname": "",
        "version_id": 33300444,
        "package_names": ["com.tivola.panda.fingerprint"]
      },
      {
        "name": "The Enchanted Kingdom",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "The Enchanted Kingdom: Elisa's Adventure\"\" è la tua storia meravigliosa.",
          "long_description": "Un gioco match-3 magicamente colorato!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3861/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3861/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3861/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3861/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3861/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3861/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3861/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3861/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3861,
        "html5_shortname": "",
        "version_id": 33300410,
        "package_names": ["com.herocraft.game.kingdom"]
      },
      {
        "name": "The Inner World - The Puzzle",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "The Inner World - The Puzzle è un'esperienza puzzle completamente nuova!",
          "long_description":
              "The Inner World - The Puzzle è un'esperienza puzzle completamente nuova!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3226/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3226/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3226/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3226/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3226/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3226/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3226/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3226/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3226,
        "html5_shortname": "",
        "version_id": 33296156,
        "package_names": ["air.com.headupgames.theinnerworldpuzzle"]
      },
      {
        "name": "Townsmen",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "rating": "4",
        "descriptions": {
          "short_description":
              "Da umile insediamento a metropoli medievale: costruisci la città dei tuoi sogni!",
          "long_description":
              "Governa tutto l'impero dal tuo castello e assicurati che i tuoi cittadini siano felici e soddisfatti!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1690/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/1690/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/1690/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1690/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1690/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1690/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1690/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1690/default/screenshot/4.jpg"
          ]
        },
        "product_id": 1690,
        "html5_shortname": "",
        "version_id": 33294934,
        "package_names": ["com.hg.townsmen7"]
      },
      {
        "name": "Townsmen 6",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Costruisci dei villaggi e fai crescere l'economia, per condurre i cittadini alla vittoria sul re!",
          "long_description":
              "Immergiti nell'atmosfera della Rivoluzione francese. Costruisci dei villaggi e fai crescere l'economia, per condurre i cittadini alla vittoria sul re!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2693/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2693/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2693/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2693/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2693/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2693/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2693/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2693/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2693,
        "html5_shortname": "",
        "version_id": 33295732,
        "package_names": ["com.hg.townsmen6"]
      },
      {
        "name": "HANDYGAMES Wild Life - America",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3187/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3187/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3187/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3187/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3187/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3187/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3187/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3187/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3187,
        "html5_shortname": "",
        "version_id": 33295734,
        "package_names": ["com.tivola.wildlife.offline"]
      },
      {
        "name": "Wild Life Africa",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Prenditi cura di tutti gli elefanti, leoni e zebre nel tuo rifugio nella savana",
          "long_description":
              "Prenditi cura di tutti gli elefanti, i leoni e le zebre nel tuo rifugio nella savana."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3227/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3227/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3227/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3227/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3227/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3227/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3227/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3227/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3227,
        "html5_shortname": "",
        "version_id": 33300352,
        "package_names": [
          "com.tivola.wildlifeafrica.offline",
          "com.tivola.wildlife.offline"
        ]
      },
      {
        "name": "Zombie Blitz",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "12+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Il mondo è stato inghiottito dall'oscurità e invaso dai morti viventi.",
          "long_description":
              "Il mondo è stato inghiottito dall'oscurità e invaso dai morti viventi."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3126/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3126/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3126/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3126/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3126/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3126/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3126/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3126/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3126,
        "html5_shortname": "",
        "version_id": 33295878,
        "package_names": ["com.headupgames.zombieblitz"]
      },
      {
        "name": "Zombie Derby",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description":
              "In un'America post-apocalittica, i problemi si risolvono in due modi: con automobili e pistole.\r\n",
          "long_description":
              "In un'America post-apocalittica, i problemi vengono risolti in due modi: con le automobili e con le pistole.\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3461/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3461/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3461/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3461/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3461/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3461/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3461/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3461/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3461,
        "html5_shortname": "",
        "version_id": 33295612,
        "package_names": ["com.herocraft.game.full.zombiederby"]
      },
      {
        "name": "Zombie Derby 2",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "16+",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description":
              "Scopri un pericoloso mondo postapocalittico e delle spettacolari vetture potenziabili!",
          "long_description":
              "Prova Zombie Derby 2 e non ti annoierai mai più! Scopri un pericoloso mondo apocalittico popolato di zombi.\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3620/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3620/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3620/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3620/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3620/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3620/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3620/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3620/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3620,
        "html5_shortname": "",
        "version_id": 33295850,
        "package_names": ["com.herocraft.game.premium.zombiederby2"]
      },
      {
        "name": "8 in 1 Solitaire",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description": "8 giochi di solitario sono meglio di 1!\r\n",
          "long_description":
              "Gameplay semplice, Interfaccia pulita, Varie modalità di gioco, Esperienza di Solitario pura, Divertimento coinvolgente"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3180/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3180/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3180/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3180/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3180/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3180/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3180/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3180/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3180,
        "html5_shortname": "",
        "version_id": 22898323,
        "package_names": ["sk.inlogic.solitaire8in1"]
      },
      {
        "name": "Baseball Pro 2017",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Gioca a baseball con un gioco semplice e coinvolgente!",
          "long_description":
              "Grafica incredibile, Facile da giocare, Gioca un'esperienza di baseball completa, Nuove, fantastiche squadre!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3235/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3235/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3235/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3235/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3235/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3235/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3235/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3235/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3235,
        "html5_shortname": "",
        "version_id": 22783975,
        "package_names": ["sk.inlogic.baseball"]
      },
      {
        "name": "Baseball Pro 2018",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Nuova edizione del 2018! Gioca a baseball in un gioco semplice e avvincente!",
          "long_description":
              "Grafica incredibile, Facile da giocare, Gioca un'esperienza di baseball completa, Seconda edizione rivisitata!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3629/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3629/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3629/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3629/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3629/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3629/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3629/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3629/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3629,
        "html5_shortname": "",
        "version_id": 29754993,
        "package_names": ["sk.inlogic.baseball2018"]
      },
      {
        "name": "Baseball Pro 2019",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description": "Gioca a baseball in modo molto semplice!",
          "long_description":
              "Grafica stupefacente, Torneo, Gioca per le squadre più importanti del mondo!, Incantevole grafica vecchia scuola, Gioco arcade facile da imparare"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3932/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3932/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3932/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3932/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3932/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3932/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3932/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3932/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3932,
        "html5_shortname": "",
        "version_id": 33196598,
        "package_names": ["sk.inlogic.baseball2019"]
      },
      {
        "name": "Basketball Mania",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Una sfida di basket infinita! Devi essere veloce e preciso!",
          "long_description":
              "Gameplay avvincente, Comandi con un solo dito, 4 campi di gioco unici, 27 palle, Tiri speciali"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3550/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3550/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3550/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3550/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3550/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3550/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3550/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3550/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3550,
        "html5_shortname": "",
        "version_id": 29052901,
        "package_names": ["sk.inlogic.basketballmania"]
      },
      {
        "name": "Bingo Mania",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Se ti piace il bingo e vuoi provarne uno nuovo, questo Bingo è la scelta migliore.",
          "long_description":
              "Grafica meravigliosa, Premi invitanti, 4 livelli di difficoltà, Schede multiple, Non servono soldi veri"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3615/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3615/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3615/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3615/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3615/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3615/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3615/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3615/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3615,
        "html5_shortname": "",
        "version_id": 29340057,
        "package_names": ["sk.inlogic.bingo"]
      },
      {
        "name": "Birdy Run",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Essere uccellini non è facile. Specialmente se potrebbero caderti in testa delle casse!",
          "long_description":
              "Cerca di non farti schiacciare dalle casse e raccogli i semini deliziosi. Fortunatamente puoi anche arrampicarti sulle superfici verticali!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2593/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2593/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2593/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2593/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2593/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2593/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2593/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2593/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2593,
        "html5_shortname": "",
        "version_id": 32609035,
        "package_names": ["sk.inlogic.tower"]
      },
      {
        "name": "Blackjack Pro",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Il Blackjack è uno dei giochi da casinò più popolari al mondo. Tenta la sorte!\r\n",
          "long_description":
              "L'obiettivo del gioco è raggiungere 21 o un punteggio superiore al banco senza superare 21."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2810/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2810/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2810/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2810/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2810/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2810/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2810/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2810/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2810,
        "html5_shortname": "",
        "version_id": 33196666,
        "package_names": ["com.inlogic.blackjackpro"]
      },
      {
        "name": "BOU",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Ecco Bou, il tuo nuovo migliore amico! Prenditene cura e lui ti amerà.",
          "long_description":
              "cucciolo tenerissimo, emozioni reali, Un sacco di vestitini, personalizza Bou, grafica stupenda"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3341/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3341/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3341/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3341/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3341/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3341/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3341/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3341/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3341,
        "html5_shortname": "",
        "version_id": 24898990,
        "package_names": ["sk.inlogic.bou.android"]
      },
      {
        "name": "Bubble Blaster",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "9+",
        "size": "N/A",
        "descriptions": {
          "short_description": "Un classico gioco arcade scoppia-bolle!\r\n",
          "long_description":
              "Varie modalità di gioco, Un sacco di funzionalità e tanti ostacoli, Livelli con stelle da vincere, Obiettivi da raggiungere, Grafica fantastica"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3222/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3222/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3222/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3222/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3222/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3222/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3222/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3222/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3222,
        "html5_shortname": "",
        "version_id": 22784221,
        "package_names": ["sk.inlogic.bubbleblaster"]
      },
      {
        "name": "Candy Bubble Splash 2",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Le macchinette piene di caramelle ti stanno aspettando!",
          "long_description":
              "Scopri le funzioni d'aiuto delle macchinette e goditi molte modalità e sfide del gioco!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2234/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2234/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2234/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2234/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2234/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2234/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2234/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2234/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2234,
        "html5_shortname": "",
        "version_id": 21273632,
        "package_names": ["sk.inlogic.candybubblesplash2"]
      },
      {
        "name": "Candy Thieves 3D",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Un'avventura dinamica. Proteggi caramelle squisite da insolenti gnomi ladri!\r\n",
          "long_description":
              "30 trappole e 30 gnomi ladri unici., Gioca con gli amici su un solo dispositivo., 6 ambientazioni magnifiche con 6 boss unici.\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3314/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3314/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3314/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3314/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3314/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3314/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3314/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3314/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3314,
        "html5_shortname": "",
        "version_id": 23566009,
        "package_names": ["com.TrolleyBussGames.TeslaCastle"]
      },
      {
        "name": "Celebrity Slot Machine",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description": "Vuoi tentare la sorte? Gira e vinci!",
          "long_description":
              "grandi vincite, gameplay semplice, Famose celebrità, Grafica deliziosa, gioco bonus"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3811/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3811/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3811/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3811/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3811/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3811/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3811/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3811/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3811,
        "html5_shortname": "",
        "version_id": 31222603,
        "package_names": ["sk.inlogic.celebrityslotmachine"]
      },
      {
        "name": "Color Pin",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Metti alla prova le tue abilità in questo gioco. Concentrati e spara!",
          "long_description":
              "Test di attenzione, Tantissimi livelli colorati, Modalità infinita incredibile, Comandi semplici, Grafica fantastica"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3278/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3278/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3278/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3278/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3278/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3278/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3278/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3278/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3278,
        "html5_shortname": "",
        "version_id": 22782335,
        "package_names": ["sk.inlogic.colorpinpremium"]
      },
      {
        "name": "Color Swap",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Fai saltare la pallina attraverso gli ostacoli quando i colori si allineano.\r\n",
          "long_description":
              "Gameplay semplice e avvincente, Grafica minimalista, Varie modalità di gioco, Modalità infinita, più di 300 livelli, 50 palle sbloccabili"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3325/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3325/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3325/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3325/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3325/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3325/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3325/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3325/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3325,
        "html5_shortname": "",
        "version_id": 23566021,
        "package_names": ["com.Inlogic.colorswap"]
      },
      {
        "name": "Cross the Road",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description":
              "Aiuta graziosi animaletti a raggiungere l'altro lato!",
          "long_description":
              "Attraversa quante più vie, strade ferrate e fiumi possibili con il tuo animale evitando il traffico."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2580/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2580/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2580/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2580/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2580/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2580/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2580/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2580/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2580,
        "html5_shortname": "",
        "version_id": 31962173,
        "package_names": ["sk.inlogic.crossTheRoad"]
      },
      {
        "name": "Dark Assassin",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "12+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Corri in un castello infinito pieno di oro e nemici!",
          "long_description":
              "Obiettivi, Gameplay veloce, Grafica meravigliosa, Azione taglia e affetta, Corsa infinita"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3215/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3215/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3215/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3215/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3215/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3215/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3215/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3215/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3215,
        "html5_shortname": "",
        "version_id": 22746593,
        "package_names": ["sk.inlogic.darkassassin"]
      },
      {
        "name": "Dots Mania",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Scegli tra le modalità Contro il tempo, Mosse limitate o Infinita.",
          "long_description": "Li puoi unire in righe o colonne, o in quadrato."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2213/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2213/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2213/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2213/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2213/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2213/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2213/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2213/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2213,
        "html5_shortname": "",
        "version_id": 31962359,
        "package_names": ["sk.inlogic.dotsmania"]
      },
      {
        "name": "Drag Racing Pro",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "12+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Il gioco definitivo di corse su rettilineo!\r\n",
          "long_description":
              "Avvincente modalità Carriera, Sistema di potenziamento delle auto, 29 auto veloci, Gameplay coinvolgente, Grafica meravigliosa"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3212/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3212/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3212/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3212/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3212/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3212/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3212/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3212/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3212,
        "html5_shortname": "",
        "version_id": 22898251,
        "package_names": ["sk.inlogic.dragracingpro"]
      },
      {
        "name": "Drag Racing Rivals",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Fantastico gioco di corse dragster ad alta velocità in 3D.\r\n",
          "long_description":
              "25 rivali difficili da battere, Sistema di potenziamento delle auto, 17 auto veloci, Gameplay coinvolgente, Grafica 3D eccezionale\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3787/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3787/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3787/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3787/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3787/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3787/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3787/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3787/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3787,
        "html5_shortname": "",
        "version_id": 30489739,
        "package_names": ["sk.inlogic.dragracingrivals"]
      },
      {
        "name": "Fidget Spinner",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Questo gioco di fidget spinner ti aiuterà a liberarti dallo stress. Collezionali tutti!\r\n",
          "long_description":
              "Simulatore realistico, Rilassante e antistress, Decine di fidget spinner, Motore fisico autentico, Trottole personalizzabili\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3330/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3330/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3330/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3330/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3330/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3330/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3330/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3330/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3330,
        "html5_shortname": "",
        "version_id": 23947456,
        "package_names": ["sk.inlogic.fidgetspinner"]
      },
      {
        "name": "Flip",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Un gioco di salti dal ritmo frenetico dove devi raccogliere le stelle nel minor tempo possibile.\r\n",
          "long_description":
              "Motori fisici differenti, Acquisto di avatar, 22 livelli unici, 15 personaggi diversi, classifica locale e globale\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3821/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3821/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3821/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3821/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3821/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3821/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3821/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3821/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3821,
        "html5_shortname": "",
        "version_id": 31222777,
        "package_names": ["sk.inlogic.flip"]
      },
      {
        "name": "Floppy Wars",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Vola, evita gli ostacoli e arriva più lontano che puoi con diversi personaggi.\r\n",
          "long_description":
              "Vola con Floppy l'uccello attraverso l'Impero Oscuro più lontano che puoi. Prova tutti gli uccelli e raggiungi il più alto punteggio di stelle."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2723/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2723/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2723/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2723/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2723/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2723/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2723/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2723/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2723,
        "html5_shortname": "",
        "version_id": 32609421,
        "package_names": ["sk.inlogic.floppywars.android"]
      },
      {
        "name": "Football Cup 2018",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "9+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Scegli la tua squadra e diventa un campione!\r\n",
          "long_description":
              "Build piccolissima! Solo 19 MB!, Torneo mondiale!, Gioca un'intera stagione con una delle squadre fantasy!, Lunga modalità di allenamento\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3712/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3712/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3712/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3712/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3712/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3712/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3712/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3712/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3712,
        "html5_shortname": "",
        "version_id": 30489799,
        "package_names": ["eu.inlogic.footballcup2018"]
      },
      {
        "name": "INLOGIC Galaxy Shooter",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3623/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3623/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3623/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3623/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3623/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3623/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3623/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3623/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3623,
        "html5_shortname": "",
        "version_id": 29339983,
        "package_names": ["sk.inlogic.galaxyshooter"]
      },
      {
        "name": "Game Of Flows",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "9+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Unisci coppie di coloratissimi draghi con le linee.",
          "long_description":
              "Rompicapi difficili;Gameplay semplice;Divertimento avvincente;Grafica stupenda;9 pacchetti di 25 livelli"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3136/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3136/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3136/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3136/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3136/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3136/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3136/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3136/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3136,
        "html5_shortname": "",
        "version_id": 22898523,
        "package_names": ["sk.inlogic.gameofflows"]
      },
      {
        "name": "Giga Jump 2",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Salta più in alto che puoi e raccogli tutti gli animali!\r\n",
          "long_description":
              "Modalità infinita!, Tanti livelli!, Grafica bellissima, Musica allegra, Mondo immaginario e interattivo"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3516/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3516/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3516/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3516/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3516/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3516/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3516/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3516/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3516,
        "html5_shortname": "",
        "version_id": 28780643,
        "package_names": ["sk.inlogic.gigajump2"]
      },
      {
        "name": "Get 10 Men",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Abbina e crea guerrieri di tutte le età! Unisciti alla battaglia!",
          "long_description":
              "Molti tipi di guerrieri, Musiche meravigliose, Battaglia infinita, Combattere è facile, vincere è difficile, Vari livelli"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3692/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3692/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3692/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3692/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3692/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3692/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3692/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3692/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3692,
        "html5_shortname": "",
        "version_id": 30086209,
        "package_names": ["sk.inlogic.get10men"]
      },
      {
        "name": "Happy Pets Match",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Unisci triplette di teneri cuccioli in un sacco di livelli e aiuta un pulcino a mettere in ordine!\r\n",
          "long_description":
              "Grafica graziosa, Molti livelli, Potenziamenti combinabili, Combinazioni di diverse modalità di gioco, Livelli rigiocabili\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3829/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3829/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3829/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3829/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3829/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3829/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3829/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3829/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3829,
        "html5_shortname": "",
        "version_id": 31222763,
        "package_names": ["sk.inlogic.happypetsmatch"]
      },
      {
        "name": "INLOGIC Impossible Dash",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2786/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2786/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2786/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2786/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2786/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2786/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2786/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2786/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2786,
        "html5_shortname": "",
        "version_id": 32609141,
        "package_names": ["sk.inlogic.dash"]
      },
      {
        "name": "Hidden Objects: Thief's Mystery",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Aiuta la signora Smith a trovare gli oggetti nascosti e diventa il miglior ladro in città!",
          "long_description":
              "Punteggio basato sulle stelle, Vari minigiochi, Ricompense maestria, Arte stilosa, Sistema di suggerimenti"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2980/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2980/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2980/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2980/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2980/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2980/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2980/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2980/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2980,
        "html5_shortname": "",
        "version_id": 33196798,
        "package_names": ["sk.inlogic.hiddenobjects"]
      },
      {
        "name": "INLOGIC Jewel Block Puzzle",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3909/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3909/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3909/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3909/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3909/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3909/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3909/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3909/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3909,
        "html5_shortname": "",
        "version_id": 32312925,
        "package_names": ["sk.inlogic.jewelblockpuzzle"]
      },
      {
        "name": "Jelly Jewel",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Unisci triplette di gelatine colorate in un sacco di livelli!",
          "long_description":
              "Grafica dolce, 60 livelli, Potenziamenti combinabili, Combinazioni di diverse modalità di gioco, Livelli rigiocabili"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3272/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3272/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3272/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3272/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3272/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3272/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3272/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3272/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3272,
        "html5_shortname": "",
        "version_id": 22782639,
        "package_names": ["sk.inlogic.jellyjewel"]
      },
      {
        "name": "INLOGIC Jewel Craft",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2623/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2623/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2623/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2623/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2623/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2623/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2623/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2623/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2623,
        "html5_shortname": "",
        "version_id": 30489809,
        "package_names": ["sk.inlogic.jewelcraft"]
      },
      {
        "name": "INLOGIC Jewel Bubbles Mania",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3696/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3696/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3696/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3696/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3696/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3696/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3696/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3696/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3696,
        "html5_shortname": "",
        "version_id": 30086235,
        "package_names": ["sk.inlogic.jewelbubblesmania"]
      },
      {
        "name": "INLOGIC Jewel Ocean",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3093/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3093/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3093/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3093/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3093/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3093/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3093/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3093/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3093,
        "html5_shortname": "",
        "version_id": 22898395,
        "package_names": ["sk.inlogic.jewelocean"]
      },
      {
        "name": "Jungle Slot Machine",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "16+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Tenta la sorte e prova le slot machine, con un gioco bonus e dei tentativi gratuiti.\r\n\r\n",
          "long_description":
              "grandi vincite, doppia possibilità, tema tropicale, gameplay semplice, gioco bonus"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3289/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3289/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3289/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3289/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3289/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3289/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3289/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3289/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3289,
        "html5_shortname": "",
        "version_id": 23405249,
        "package_names": ["sk.inlogic.jungleslotmachine"]
      },
      {
        "name": "INLOGIC Kids Zoo Day 2",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3816/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3816/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3816/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3816/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3816/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3816/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3816/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3816/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3816,
        "html5_shortname": "",
        "version_id": 31222751,
        "package_names": ["sk.inlogic.kidszooday2"]
      },
      {
        "name": "Kids Animal Colouring",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Un album con meravigliosi animali da colorare\r\n",
          "long_description":
              "Orientamento schermo automatico, Immagini meravigliose, Vari strumenti per colorare, Adatto ai bambini della scuola dell'infanzia"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3870/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3870/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3870/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3870/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3870/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3870/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3870/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3870/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3870,
        "html5_shortname": "",
        "version_id": 31962751,
        "package_names": ["sk.inlogic.coloringbook"]
      },
      {
        "name": "Kids Zoo Day",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Puzzle di splendidi animali per grandi e piccini.",
          "long_description":
              "Gameplay rilassante, Valori educativi, Foto bellissime, Vari livelli di difficoltà, Adatto a bambini e genitori"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3671/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3671/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3671/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3671/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3671/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3671/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3671/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3671/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3671,
        "html5_shortname": "",
        "version_id": 30086053,
        "package_names": ["sk.inlogic.kidszooday"]
      },
      {
        "name": "INLOGIC Lucky Slots",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3531/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3531/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3531/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3531/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3531/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3531/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3531/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3531/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3531,
        "html5_shortname": "",
        "version_id": 28832787,
        "package_names": ["sk.inlogic.luckyslots"]
      },
      {
        "name": "Ludo Royal",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description": "Gioca una partita a Ludo!\r\n",
          "long_description":
              "Gioca con gli amici su un solo dispositivo!\r\nDesign bellissimo!\r\nCambia il tuo tabellone!\r\nIl classico gioco da tavolo di Ludo!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3830/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3830/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3830/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3830/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3830/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3830/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3830/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3830/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3830,
        "html5_shortname": "",
        "version_id": 31609117,
        "package_names": ["sk.inlogic.ludoroyal"]
      },
      {
        "name": "Magic Forest Match",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Unisci triplette di frutti colorati della foresta in un sacco di livelli e aiuta una fata!",
          "long_description":
              "Grafica magica, Molti livelli, Potenziamenti combinabili, Combinazioni di diverse modalità di gioco, Livelli rigiocabili"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3512/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3512/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3512/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3512/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3512/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3512/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3512/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3512/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3512,
        "html5_shortname": "",
        "version_id": 28780587,
        "package_names": ["sk.inlogic.magicforestmatch"]
      },
      {
        "name": "Mahjong Adventure",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Il classico puzzle game solitario orientale con nuove funzionalità!\r\n",
          "long_description":
              "Medaglie e altri premi per completare il livello più in fretta, Nuove tessere speciali da scoprire, Bellissima grafica orientale\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3800/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3800/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3800/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3800/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3800/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3800/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3800/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3800/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3800,
        "html5_shortname": "",
        "version_id": 33165953,
        "package_names": ["sk.inlogic.mahjongjourney"]
      },
      {
        "name": "Mahjong Mania 2",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Rifletti su ogni mossa e libera il campo dalle piramidi di tessere.\r\n",
          "long_description":
              "Considera attentamente ogni mossa e libera il tavolo pieno di tessere impilate in piramidi. Cerca le tessere uguali libere da abbonare per rimuoverle."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2679/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2679/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2679/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2679/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2679/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2679/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2679/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2679/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2679,
        "html5_shortname": "",
        "version_id": 32609313,
        "package_names": ["sk.inlogic.mahjongmania2.android"]
      },
      {
        "name": "Mahjong Stars",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Gioca a Mahjong. Il miglior gioco di relax al mondo con un'ambientazione meravigliosa.\r\n\r\n",
          "long_description":
              "Vari livelli di difficoltà, Design bellissimo, Sistema di valutazione con le stelle per mettere alla prova le tue abilità, Oltre 140 livelli\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3169/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3169/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3169/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3169/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3169/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3169/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3169/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3169/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3169,
        "html5_shortname": "",
        "version_id": 23405141,
        "package_names": ["sk.inlogic.mahjongstars.android"]
      },
      {
        "name": "MiniGolf Mania",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Minigolf con tanti livelli e tre sistemi di controllo basati sulla difficoltà.\r\n",
          "long_description":
              "Decine di livelli, Fisica simulata, Tre ambientazioni, Controlli basati sulla difficoltà, Grafica splendente\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3462/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3462/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3462/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3462/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3462/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3462/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3462/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3462/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3462,
        "html5_shortname": "",
        "version_id": 28780409,
        "package_names": ["sk.inlogic.minigolf"]
      },
      {
        "name": "INLOGIC ManiAA",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2708/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2708/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2708/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2708/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2708/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2708/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2708/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2708/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2708,
        "html5_shortname": "",
        "version_id": 32609437,
        "package_names": ["sk.inlogic.maniaa"]
      },
      {
        "name": "Moto Drag Racing",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Il gioco definitivo delle corse motociclistiche clandestine ad alta velocità.",
          "long_description":
              "Potenziamento delle motociclette, 24 moto veloci, Grafica stupenda"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3698/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3698/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3698/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3698/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3698/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3698/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3698/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3698/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3698,
        "html5_shortname": "",
        "version_id": 30086273,
        "package_names": ["sk.inlogic.motodragracing"]
      },
      {
        "name": "Moto Rider",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Sali in sella alla tua motocicletta e corri più veloce che puoi!",
          "long_description":
              "Manovrabilità agevole, Motociclette veloci, Gameplay emozionante, Grafica bella e semplice, Guida spericolata nella corsia opposta"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3135/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3135/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3135/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3135/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3135/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3135/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3135/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3135/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3135,
        "html5_shortname": "",
        "version_id": 22898803,
        "package_names": ["sk.inlogic.motorider.android"]
      },
      {
        "name": "Parking Madness",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Risolvi i problemi di parcheggio e guida la tua auto fuori dall'area del parcheggio.",
          "long_description":
              "Auto meravigliose, Sistema di suggerimenti, Allenamento della mente, Centinaia di livelli, 4 livelli di difficoltà"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3251/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3251/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3251/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3251/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3251/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3251/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3251/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3251/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3251,
        "html5_shortname": "",
        "version_id": 22782411,
        "package_names": ["sk.inlogic.parkingmadness"]
      },
      {
        "name": "Pharaoh Slot Machine",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description": "Gira i rulli e goditi grandi vincite.",
          "long_description":
              "grandi vincite, doppia possibilità, tema tropicale, gameplay semplice, gioco bonus"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3600/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3600/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3600/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3600/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3600/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3600/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3600/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3600/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3600,
        "html5_shortname": "",
        "version_id": 29340043,
        "package_names": ["sk.inlogic.pharaohslots"]
      },
      {
        "name": "Piano Hero",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Suona il pianoforte toccando le tessere giuste. Allenati per diventare più veloce. Diventa un maestro.\r\n",
          "long_description":
              "2 modalità di gioco, Gameplay semplice, Musica classica, Facile da giocare, Grafica meravigliosa\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3023/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3023/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3023/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3023/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3023/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3023/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3023/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3023/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3023,
        "html5_shortname": "",
        "version_id": 33196614,
        "package_names": ["sk.inlogic.piano"]
      },
      {
        "name": "Pin and Spin",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Spara le palline colorate sui colori corretti sulla ruota senza colpire le altre palline.",
          "long_description":
              "Un fantastico passatempo. Metti alla prova il tuo tempismo e la tua precisione in una nuova esperienza di gioco con una grafica deliziosa."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2782/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2782/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2782/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2782/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2782/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2782/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2782/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2782/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2782,
        "html5_shortname": "",
        "version_id": 32609175,
        "package_names": ["sk.inlogic.spinandpin"]
      },
      {
        "name": "Pizza Ninja 4",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Taglia ingredienti volanti in diverse modalità e dimostra che sei degno di essere un ninja!",
          "long_description":
              "Modalità arcade infinita, Diverse modalità di gioco, 48 livelli nella modalità Memoria, Bonus e ostacoli, Nuova grafica"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3128/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3128/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3128/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3128/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3128/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3128/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3128/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3128/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3128,
        "html5_shortname": "",
        "version_id": 22898661,
        "package_names": ["sk.inlogic.pizzaninja4"]
      },
      {
        "name": "Pizza Ninja Story",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Taglia gli ingredienti con il dito! Spostati nel menu toccando i vari ingredienti.\r\n",
          "long_description":
              "Affetta gli ingredienti per la pizza e fatti strada nel mondo della pizza in tutti gli episodi. Porta a termine compiti diversi in vari livelli."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2742/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2742/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2742/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2742/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2742/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2742/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2742/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2742/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2742,
        "html5_shortname": "",
        "version_id": 32609301,
        "package_names": ["sk.inlogic.pizzaninjastorypremium"]
      },
      {
        "name": "Poke GO",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Unisci coppie di mostri Poke colorati con le linee.",
          "long_description":
              "Mostri Poke, Simple gameplay, Divertimento avvincente, Grafica meravigliosa"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3065/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3065/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3065/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3065/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3065/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3065/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3065/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3065/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3065,
        "html5_shortname": "",
        "version_id": 22898309,
        "package_names": ["sk.inlogic.pokego"]
      },
      {
        "name": "Polar Pong",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Aiuta la piccola foca a raccogliere altre monete e a evitare gli orsi polari affamati!",
          "long_description":
              "Raccogli le monete ed evita gli orsi polari arrabbiati. Più tempo giochi, più orsi polari cercheranno di prenderti!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2757/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2757/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2757/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2757/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2757/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2757/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2757/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2757/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2757,
        "html5_shortname": "",
        "version_id": 21274302,
        "package_names": ["sk.inlogic.polarpong"]
      },
      {
        "name": "Pool Pro 2",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Gioca a Palla 8, il classico del biliardo all'americana, sul tuo cellulare!",
          "long_description":
              "Gameplay realistico, Varie modalità di gioco, Fantastico design, Torneo, Mazze diverse\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3888/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3888/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3888/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3888/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3888/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3888/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3888/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3888/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3888,
        "html5_shortname": "",
        "version_id": 31962585,
        "package_names": ["sk.inlogic.poolpro2"]
      },
      {
        "name": "Pool Pro",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Gioca a Palla 8, il classico del biliardo all'americana, sul tuo cellulare!\r\n",
          "long_description":
              "Grafica e fisica realistica, Varie modalità di gioco e minigiochi, 5 tavoli diversi, 14 personaggi, Anelli e trofei da collezionare"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3333/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3333/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3333/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3333/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3333/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3333/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3333/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3333/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3333,
        "html5_shortname": "",
        "version_id": 24803464,
        "package_names": ["sk.inlogic.poolpro"]
      },
      {
        "name": "Pop Voyage",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Questa avventura fantasy ti porterà in una terra di palloncini colorati da abbinare e far scoppiare!",
          "long_description":
              "Scorrilo in orizzontale o in verticale per creare una tripletta dello stesso colore e accedi alle opzioni di gioco toccando l'icona."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2646/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2646/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2646/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2646/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2646/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2646/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2646/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2646/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2646,
        "html5_shortname": "",
        "version_id": 32609109,
        "package_names": ["sk.inlogic.popvoyage"]
      },
      {
        "name": "Pro Cricket 2018",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Gioca a Cricket in un gioco semplice e avvincente! Ora con nuove meccaniche!",
          "long_description":
              "Ora con nuove meccaniche!, Torneo, Gioca per le squadre più importanti del mondo!, Incantevole grafica vecchia scuola, Gioco arcade facile da imparare"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3386/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3386/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3386/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3386/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3386/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3386/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3386/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3386/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3386,
        "html5_shortname": "",
        "version_id": 25702618,
        "package_names": ["sk.inlogic.procricket2018"]
      },
      {
        "name": "Pro Cricket 2019",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "rating": "2.5",
        "descriptions": {
          "short_description": "Gioca a cricket in modo semplicissimo!",
          "long_description":
              "Grafica stupefacente, Gioca per le squadre più importanti del mondo!, Incantevole grafica vecchia scuola, Gioco arcade facile da imparare"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3931/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3931/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3931/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3931/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3931/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3931/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3931/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3931/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3931,
        "html5_shortname": "",
        "version_id": 33196562,
        "package_names": ["sk.inlogic.cricket2019"]
      },
      {
        "name": "Pro Football 2018",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Gioca a un gioco di calcio stile rétro per una delle squadre nazionali in una delle 3 modalità!\r\n",
          "long_description":
              "Splendida grafica rétro, Gioco di calcio veloce e divertente!, Torneo mondiale, Modalità di allenamento rapido\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3616/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3616/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3616/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3616/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3616/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3616/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3616/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3616/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3616,
        "html5_shortname": "",
        "version_id": 29339891,
        "package_names": ["sk.inlogic.prf2018"]
      },
      {
        "name": "Pro Tennis 2017",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Corri, colpisci la pallina con la racchetta, mandala oltre la rete! Gioca a Pro Tennis 2017!\r\n",
          "long_description":
              "Azione di tennis veloce, Varie superfici su cui mettersi alla prova, Campionato, Grafica dettagliata"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2993/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2993/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2993/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2993/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2993/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2993/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2993/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2993/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2993,
        "html5_shortname": "",
        "version_id": 21274292,
        "package_names": ["sk.inlogic.protennis2017"]
      },
      {
        "name": "Pro Tennis 2018",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Corri, colpisci la pallina con la racchetta, mandala oltre la rete! Gioca a Pro Tennis 2018!",
          "long_description":
              "Azione di tennis veloce, Varie superfici su cui mettersi alla prova, Campionato, Grafica dettagliata"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3382/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3382/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3382/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3382/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3382/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3382/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3382/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3382/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3382,
        "html5_shortname": "",
        "version_id": 25509938,
        "package_names": ["sk.inlogic.protennis2018"]
      },
      {
        "name": "Radicals",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description":
              "Metti alla prova le tue capacità di manovra ad alta velocità tra i muri di una pista infinita!",
          "long_description":
              "Riesci a controllare una navicella aliena? Evita gli ostacoli per ottenere un punteggio alto e piazzati in cima alla classifica dei migliori piloti!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2581/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2581/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2581/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2581/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2581/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2581/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2581/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2581/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2581,
        "html5_shortname": "",
        "version_id": 31962537,
        "package_names": ["sk.inlogic.radicalsa"]
      },
      {
        "name": "Roll that Ball",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Sposta le tessere per far arrivare la pallina alla meta.",
          "long_description":
              "Rompicapi che sfidano le mente, Centinaia di livelli, Varie modalità di gioco, Suggerimenti, Gameplay illimitato"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3012/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3012/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3012/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3012/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3012/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3012/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3012/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3012/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3012,
        "html5_shortname": "",
        "version_id": 21274318,
        "package_names": ["sk.inlogic.rollthatball"]
      },
      {
        "name": "Roll that Ball 2",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Sposta le tessere per far arrivare la pallina alla meta.",
          "long_description":
              "Rompicapi che sfidano le mente, Centinaia di livelli, Varie modalità di gioco, Grafica strabiliante, Ore e ore di divertimento"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3669/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3669/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3669/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3669/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3669/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3669/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3669/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3669/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3669,
        "html5_shortname": "",
        "version_id": 33165951,
        "package_names": ["sk.inlogic.rollthatball2"]
      },
      {
        "name": "Roulette Pro",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "16+",
        "size": "N/A",
        "descriptions": {
          "short_description": "Gira la ruota della lussuria",
          "long_description":
              "Gira la ruota della fortuna ogni giorno, Gira e vinci, Tavolo di gioco lussurioso"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3229/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3229/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3229/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3229/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3229/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3229/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3229/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3229/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3229,
        "html5_shortname": "",
        "version_id": 22782347,
        "package_names": [
          "sk.inlogic.roulettepro.android",
          "sk.inlogic.rouletteprovip.android"
        ]
      },
      {
        "name": "Roulette Vegas Casino",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Gioca alla roulette della lussuria e vinci premi fantastici!",
          "long_description":
              "Vinci grandi premi, Roulette gratis, Tutti i tipi di scommesse, Ruota della fortuna, Grafica di lusso"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3677/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3677/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3677/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3677/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3677/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3677/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3677/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3677/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3677,
        "html5_shortname": "",
        "version_id": 30086109,
        "package_names": ["sk.inlogic.roulettevegascasino.android"]
      },
      {
        "name": "Santa's Xmas Adventure",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Sposta le tessere per tracciare un percorso e portare Babbo Natale alla meta.",
          "long_description":
              "Rompicapi difficili, Centinaia di livelli, Diverse modalità di gioco, Suggerimenti, Gameplay infinito"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3481/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3481/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3481/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3481/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3481/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3481/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3481/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3481/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3481,
        "html5_shortname": "",
        "version_id": 28780599,
        "package_names": ["sk.inlogic.xmasadventure"]
      },
      {
        "name": "Sir Yes Sir",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Abbina e crea guerrieri di tutte le epoche! Combatti e prova tante modalità di gioco nella storia.",
          "long_description":
              "Crea il guerriero di grado più alto possibile abbinando guerrieri adiacenti dello stesso tipo. La difficoltà aumenta ogni volta che crei un guerriero."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2763/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2763/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2763/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2763/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2763/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2763/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2763/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2763/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2763,
        "html5_shortname": "",
        "version_id": 21273686,
        "package_names": ["sk.inlogic.siryessir"]
      },
      {
        "name": "Solitaire Mania",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Un gioco di carte avvincente e facile in cui devi ordinare le carte in pile.",
          "long_description":
              "Gameplay rilassante, Pesca a 1 o a 3 carte, Temi fantastici, Suggerimenti e annullamenti illimitati, Ore e ore di divertimento"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3646/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3646/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3646/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3646/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3646/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3646/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3646/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3646/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3646,
        "html5_shortname": "",
        "version_id": 29755003,
        "package_names": ["sk.inlogic.solitairemania"]
      },
      {
        "name": "Solitaire Pro 2",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Ordina tutte le carte sul tavolo in 4 pile in ordine crescente, in base ai valori e ai semi.",
          "long_description":
              "Solitario è un gioco di carte semplice e molto coinvolgente, in cui il giocatore ordina le carte in pile in base ai valori e ai semi."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2734/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2734/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2734/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2734/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2734/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2734/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2734/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2734/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2734,
        "html5_shortname": "",
        "version_id": 21273698,
        "package_names": ["sk.inlogic.solitairepro2"]
      },
      {
        "name": "Star Drives",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Controlla un'astronave in orbita ed evita le navi nemiche. Sblocca astronavi migliori e raggiungi gradi più alti!\r\n",
          "long_description":
              "Accelera o frena l'astronave in orbita per evitare di schiantarti contro le navi nemiche. Raggiungi il più alto grado di stelle e prova tutte le navi."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2707/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2707/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2707/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2707/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2707/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2707/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2707/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2707/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2707,
        "html5_shortname": "",
        "version_id": 21273712,
        "package_names": ["sk.inlogic.stardrives"]
      },
      {
        "name": "Star Drives 2",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Pilota la tua astronave e sopravvivi più a lungo possibile.",
          "long_description":
              "Battaglie spaziali, Grafica stile cartoon, missioni nello spazio, gameplay avvincente, Astronavi diverse"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3498/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3498/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3498/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3498/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3498/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3498/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3498/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3498/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3498,
        "html5_shortname": "",
        "version_id": 28780671,
        "package_names": ["sk.inlogic.stardrives2"]
      },
      {
        "name": "Stick Freak",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Assicurati che il bastone sia preciso, né troppo lungo né troppo corto.",
          "long_description":
              "Tocca mentre cammini sul bastone per camminare sotto o risalire."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2262/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2262/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2262/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2262/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2262/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2262/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2262/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2262/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2262,
        "html5_shortname": "",
        "version_id": 21273736,
        "package_names": ["sk.inlogic.stickfreak.android"]
      },
      {
        "name": "Street Football 2018",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description": "Indossa gli scarpini e scendi in strada!",
          "long_description":
              "Calcio da strada!, Squadre fantastiche!, Grafica rétro!, Modalità di allenamento rapido!, Modalità Coppa!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3791/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3791/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3791/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3791/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3791/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3791/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3791/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3791/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3791,
        "html5_shortname": "",
        "version_id": 30489159,
        "package_names": ["sk.inlogic.ssc2018"]
      },
      {
        "name": "Street Racer 3D",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Un'emozionante esperienza di guida nel traffico su una strada senza fine.",
          "long_description":
              "Guida agevole, Messa a punto dell'auto, 35 auto veloci, 5 ambienti dettagliati, 5 modalità di gioco"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2995/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2995/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2995/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2995/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2995/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2995/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2995/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2995/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2995,
        "html5_shortname": "",
        "version_id": 33196820,
        "package_names": ["sk.inlogicgames.traffic"]
      },
      {
        "name": "Street Racer Underground",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Corri sulle strade di una metropoli notturna. Fai attenzione, però: gli sbirri non apprezzeranno!\r\n",
          "long_description":
              "Corri sulle strade di una metropoli notturna. Fai attenzione però! Gli sbirri non lo apprezzeranno!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3451/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3451/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3451/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3451/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3451/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3451/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3451/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3451/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3451,
        "html5_shortname": "",
        "version_id": 28780683,
        "package_names": ["sk.inlogicgames.sru"]
      },
      {
        "name": "Subway Dash",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Benvenuti nel mondo dei binari! Corri più che puoi e raggiungi tutti gli obiettivi!",
          "long_description":
              "Obiettivi, Corsa infinita, Migliora i tuoi potenziamenti, Compra nuove skin"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3095/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3095/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3095/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3095/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3095/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3095/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3095/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3095/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3095,
        "html5_shortname": "",
        "version_id": 22898445,
        "package_names": ["sk.inlogic.subwaydash"]
      },
      {
        "name": "Subway Rush",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Runner game infinito dal ritmo frenetico!\r\n\r\n",
          "long_description":
              "Tanti potenziamenti!, Runner infinito!, Amico drone!, Personaggi personalizzabili!, Città colorata\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3892/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3892/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3892/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3892/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3892/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3892/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3892/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3892/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3892,
        "html5_shortname": "",
        "version_id": 31962325,
        "package_names": ["sk.inlogic.subwayrush"]
      },
      {
        "name": "Super Pocket Football 2015",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description": "Ti piace il calcio?",
          "long_description":
              "Impara a passare e a tirare nella modalit? allenamento, trova le mosse tattiche migliori nelle partite amichevoli e vinci il campionato!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2342/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2342/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2342/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2342/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2342/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2342/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2342/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2342/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2342,
        "html5_shortname": "",
        "version_id": 31962353,
        "package_names": ["com.amigogames.superpocketfootball2014.wrap"]
      },
      {
        "name": "INLOGIC Sweet Bubble Story",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2776/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2776/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2776/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2776/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2776/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2776/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2776/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2776/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2776,
        "html5_shortname": "",
        "version_id": 32609217,
        "package_names": ["sk.inlogic.sweetbubblestory"]
      },
      {
        "name": "Sweet Jewel",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Un classico arcade match-3 a turni, con un sacco di livelli impegnativi e modalità diverse!\r\n",
          "long_description":
              "Scambia e abbina i dolcetti preziosi per formare una catena di 3 o più caramelle dello stesso colore. Se ne abbini 4, riceverai potenziamenti."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2706/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2706/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2706/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2706/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2706/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2706/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2706/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2706/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2706,
        "html5_shortname": "",
        "version_id": 21273762,
        "package_names": ["sk.inlogic.jewel"]
      },
      {
        "name": "Tasty Slot Machine",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "16+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Vuoi tentare la sorte? Gira i rulli e incrocia le dita!\r\n",
          "long_description":
              "grandi vincite, doppia possibilità, grafica deliziosa, gameplay semplice, gioco bonus"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3016/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3016/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3016/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3016/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3016/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3016/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3016/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3016/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3016,
        "html5_shortname": "",
        "version_id": 33196684,
        "package_names": ["sk.inlogic.tastyslotmachine"]
      },
      {
        "name": "Temple Rush",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Corri ed evita gli ostacoli. Raccogli le monete, migliora i potenziamenti e raggiungi gli obiettivi.\r\n",
          "long_description":
              "Gameplay infinito, Vari potenziamenti, Miglioramenti, Abiti speciali, Ore e ore di divertimento\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3523/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3523/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3523/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3523/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3523/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3523/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3523/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3523/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3523,
        "html5_shortname": "",
        "version_id": 28832759,
        "package_names": ["sk.inlogic.templerush"]
      },
      {
        "name": "Tower Mania",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Costruisci le torri in un fantastico mondo del futuro, con un semplice clic!",
          "long_description":
              "Costruite le vostre antiche o moderne torri nel fantastico mondo futuristico con il piu semplice modo ad un clic!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2252/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2252/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2252/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2252/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2252/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2252/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2252/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2252/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2252,
        "html5_shortname": "",
        "version_id": 21273654,
        "package_names": ["sk.inlogic.tower"]
      },
      {
        "name": "INLOGIC Tubby Birds 2",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3100/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3100/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3100/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3100/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3100/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3100/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3100/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3100/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3100,
        "html5_shortname": "",
        "version_id": 22898433,
        "package_names": ["sk.inlogic.tubbybirds2"]
      },
      {
        "name": "World Casino",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "18+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Divertiti con i più classici giochi d'azzardo in un lussuoso casinò!\r\n",
          "long_description":
              "3 giochi in 1, Chip illimitate, Giri giornalieri per ottenere bonus, Facile da giocare, Divertimento coinvolgente\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3383/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3383/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3383/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3383/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3383/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3383/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3383/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3383/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3383,
        "html5_shortname": "",
        "version_id": 25509950,
        "package_names": ["sk.inlogic.worldcasino.android"]
      },
      {
        "name": "Western Slot Machine",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description": "Gira i rulli e goditi grandi vincite.\r\n",
          "long_description":
              "grandi vincite, doppia possibilità, tema selvaggio West, gameplay semplice, gioco bonus\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3697/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3697/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3697/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3697/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3697/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3697/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3697/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3697/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3697,
        "html5_shortname": "",
        "version_id": 30086285,
        "package_names": ["sk.inlogic.westernslotmachine"]
      },
      {
        "name": "World Quiz",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Metti alla prova le tue conoscenze mentre visiti tanti luoghi diversi.",
          "long_description":
              "Centinaia di luoghi, Foto meravigliose, Sfide giornaliere, Decine di set livelli, Vari suggerimenti"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3628/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3628/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3628/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3628/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3628/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3628/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3628/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3628/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3628,
        "html5_shortname": "",
        "version_id": 29339967,
        "package_names": ["sk.inlogic.worldquiz"]
      },
      {
        "name": "Zombie Hill Race",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Guida attraverso una landa desolata infettata dagli zombi!",
          "long_description":
              "Fantastica atmosfera apocalittica, Diverse auto killer, Un sacco di potenziamenti per auto, Livelli avvincenti, Fisica ragdoll\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3897/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3897/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3897/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3897/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3897/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3897/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3897/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3897/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3897,
        "html5_shortname": "",
        "version_id": 33165947,
        "package_names": ["sk.Inlogic.ZombieHillRace"]
      },
      {
        "name": "Zombie Rider",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Sarà una battaglia esilarante! Solo la tua auto e centinaia di zombie.\r\n",
          "long_description":
              "Distruzione dei non morti, Guida emozionante, 4 auto diverse, Milioni di zombie assetati di sangue, 4 ambientazioni diverse\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3401/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3401/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3401/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3401/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3401/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3401/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3401/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3401/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3401,
        "html5_shortname": "",
        "version_id": 26307498,
        "package_names": ["sk.inlogic.zombierider.android"]
      },
      {
        "name": "Zoo Quiz",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Metti alla prova la tua conoscenza del regno degli animali. Conosci davvero tutti gli animali?\r\n",
          "long_description":
              "Centinaia di animali, Foto meravigliose, Sfide giornaliere, Decine di set livelli, Vari suggerimenti"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3345/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3345/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3345/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3345/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3345/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3345/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3345/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3345/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3345,
        "html5_shortname": "",
        "version_id": 24898994,
        "package_names": ["com.inlogic.zooquiz"]
      },
      {
        "name": "snakes.io",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Muovi il tuo serpente e mangia i puntini per crescere. Evita i serpenti nemici o cerca di bloccarli.",
          "long_description":
              "Gameplay semplice, Connessione Internet non necessaria, Comandi semplici, Grafica pulita, Musica accattivante"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2984/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2984/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2984/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2984/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2984/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2984/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2984/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2984/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2984,
        "html5_shortname": "",
        "version_id": 21274364,
        "package_names": ["sk.inlogic.snakesio.android"]
      },
      {
        "name": "INLOGIC snakes.io 2",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3474/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3474/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3474/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3474/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3474/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3474/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3474/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3474/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3474,
        "html5_shortname": "",
        "version_id": 28729665,
        "package_names": ["sk.inlogic.snakesio2"]
      },
      {
        "name": "Iron Man 3 Mobile Premium",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "",
        "size": "N/A",
        "rating": "3",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1697/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/1697/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/1697/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1697/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1697/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1697/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1697/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1697/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 1697,
        "html5_shortname": "",
        "version_id": 33196530,
        "package_names": [
          "com.gameloft.android.GAND.GloftIRO3",
          "com.gameloft.android.GloftIRO3",
          "com.gameloft.android.LATAM.GloftIRO3",
          "com.gameloft.android.LATAM.BS46",
          "com.gameloft.android.LATAM.GloftBS19",
          "com.gameloft.android.LATAM.BU05",
          "com.gameloft.android.LATAM.BE02",
          "com.gameloft.android.ALCH.GloftIRO3"
        ]
      },
      {
        "name": "L'era glaciale - Le scrat-venture",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "rating": "3",
        "descriptions": {
          "short_description":
              "Gioca con Scrat in un platform in 2D!\r\nCorri, salta e combatti per trovare la Ghianda grandiosa!",
          "long_description":
              "Viaggia in tre ambienti diversi: il valico ghiacciato, la giungla dei dinosauri e Scratlantide.\r\nGuida Scrat in quest'epica avventura correndo, saltando, arrampicandoti e altro ancora!\r\nIntraprendi divertenti battaglie contro i boss, tra cui un cucciolo di avvoltoio e un dinosauro.\r\nIncontra le creature tipiche de L'ERA GLACIALE, come gli iraci, i ratti e i feroci piranha!\r\nScopri ambienti segreti e raccogli fino all'ultima ghianda!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2026/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/2026/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/2026/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2026/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2026/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2026/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2026/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2026/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 2026,
        "html5_shortname": "",
        "version_id": 33196526,
        "package_names": [
          "com.gameloft.android.GAND.GloftSCRT",
          "com.gameloft.android.ALCH.GloftSCRT",
          "com.gameloft.android.GloftSCRT"
        ]
      },
      {
        "name": "Le 12 fatiche di Ercole II : il Toro di Creta ",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Aiuta Ercole ad addomesticare il grande Toro di Creta! \r\n",
          "long_description":
              "Aiuta Ercole a domare il grande Toro di Creta! Accompagna Ercole in una missione alla ricerca del Toro di Creta!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3008/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3008/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3008/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3008/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3008/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3008/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3008/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3008/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3008,
        "html5_shortname": "",
        "version_id": 33296124,
        "package_names": ["com.jetdogs.hercules2full"]
      },
      {
        "name": "12 Labours of Hercules",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Scopri 12 Labours of Hercules e goditi un epico gioco d'azione!\r\n\r\n",
          "long_description":
              "Assisti Hercules, un eroe dell'antica Grecia. Il malvagio Hades ha rapito sua moglie mentre Hercules dormiva!\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3659/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3659/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3659/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3659/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3659/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3659/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3659/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3659/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3659,
        "html5_shortname": "",
        "version_id": 33295694,
        "package_names": ["com.jetdogs.herculesfull"]
      },
      {
        "name": "12 Labours of Hercules IV: Mother Nature",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Aiuta Hercules e Megara a riparare ai danni degli dei vendicativi!\r\n\r\n",
          "long_description":
              "Aiuta Hercules e Megara a porre riparo ai danni provocati dagli dei vendicativi!\r\n\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3666/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3666/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3666/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3666/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3666/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3666/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3666/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3666/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3666,
        "html5_shortname": "",
        "version_id": 33300388,
        "package_names": ["com.jetdogs.hercules4full"]
      },
      {
        "name": "12 Labours of Hercules III: Girl Power",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Goditi 12 Labours of Hercules III, un eroico gioco d'azione, e dimostra tutto il tuo valore!",
          "long_description":
              "Aiuta Megara a liberare Hercules, poi accompagnalo nelle sue avventure!\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3660/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3660/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3660/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3660/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3660/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3660/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3660/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3660/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3660,
        "html5_shortname": "",
        "version_id": 33295626,
        "package_names": ["com.jetdogs.hercules3full"]
      },
      {
        "name": "Alchemy Jam",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description":
              "Divertiti con la migliore avventura match-3 per cellulari!\r\n\r\n",
          "long_description":
              "Divertiti con la migliore avventura mobile di abbinamenti!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3268/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3268/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3268/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3268/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3268/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3268/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3268/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3268/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3268,
        "html5_shortname": "",
        "version_id": 33295414,
        "package_names": ["com.joybits.alchemyjam"]
      },
      {
        "name": "12 Labours of Hercules V: Kids of Hellas",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Ares chiede a Ercole di guidare il suo esercito!",
          "long_description":
              "Ares, il dio greco della guerra, chiede a Hercules di guidare il suo esercito. Quando l’eroe rifiuta, il dio lancia una maledizione sulla progenie dell'Ellade.\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3686/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3686/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3686/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3686/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3686/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3686/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3686/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3686/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3686,
        "html5_shortname": "",
        "version_id": 33295398,
        "package_names": ["com.jetdogs.hercules5full"]
      },
      {
        "name": "Blast Mania",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "rating": "4.68",
        "descriptions": {
          "short_description":
              "Divertiti con la migliore avventura match-3 per cellulari!",
          "long_description":
              "Divertiti con la migliore avventura match-3 per cellulari!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3237/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3237/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3237/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3237/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3237/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3237/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3237/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3237/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3237,
        "html5_shortname": "",
        "version_id": 33294928,
        "package_names": ["com.joybits.blastmania"]
      },
      {
        "name": "Brick Breaker - Ghostanoid",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "La versione più folle di Arkanoid: tutti hanno un lavoro normale e noi abbiamo case infestate.",
          "long_description":
              "La versione più folle di Arkanoid: tutti hanno un lavoro normale, invece noi abbiamo case infestate."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3209/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3209/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3209/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3209/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3209/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3209/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3209/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3209/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3209,
        "html5_shortname": "",
        "version_id": 33296090,
        "package_names": ["com.Qplaze.Ghostanoid.s5innovations"]
      },
      {
        "name": "Doodle Creatures",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Cosa succede se combini un elefante e un fenicottero?",
          "long_description":
              "Cosa succede se combini un elefante e un fenicottero?"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2986/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2986/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2986/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2986/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2986/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2986/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2986/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2986/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2986,
        "html5_shortname": "",
        "version_id": 33295754,
        "package_names": ["com.joybits.doodlecreatures"]
      },
      {
        "name": "Doodle Devil",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description": "Il Potere della Distruzione è nelle tue mani!",
          "long_description":
              "Se ti piace Doodle God... abbraccia il tuo lato oscuro con Doodle Devil!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3099/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3099/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3099/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3099/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3099/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3099/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3099/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3099/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3099,
        "html5_shortname": "",
        "version_id": 33296046,
        "package_names": ["com.joybits.doodledevil_pay"]
      },
      {
        "name": "Doodle God",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "descriptions": {
          "short_description": "LIBERA LA TUA IMMAGINAZIONE E DIVENTA UN DIO",
          "long_description": "LIBERA LA TUA IMMAGINAZIONE E DIVENTA UN DIO"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2987/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2987/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2987/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2987/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2987/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2987/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2987/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2987/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2987,
        "html5_shortname": "",
        "version_id": 33295946,
        "package_names": ["joybits.doodlegod"]
      },
      {
        "name": "Doodle God: 8-bit Mania",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description": "DOODLE GOD HA UNA NUOVA GRAFICA PIXEL!",
          "long_description": "DOODLE GOD HA UNA NUOVA GRAFICA PIXEL!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3098/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3098/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3098/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3098/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3098/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3098/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3098/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3098/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3098,
        "html5_shortname": "",
        "version_id": 33296132,
        "package_names": ["joybits.doodlegod_pixel"]
      },
      {
        "name": "Doodle God: Good Old Times",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "9+",
        "size": "N/A",
        "rating": "4.75",
        "descriptions": {
          "short_description":
              "Sei un re o un contadino? Entra in un mondo di castelli e cavalieri!\r\n\r\n",
          "long_description":
              "Sei un re o un contadino? Entra in un mondo di castelli e cavalieri!\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3832/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3832/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3832/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3832/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3832/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3832/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3832/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3832/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3832,
        "html5_shortname": "",
        "version_id": 33165949,
        "package_names": ["joybits.doodlegoodoldtimes_alt.nst"]
      },
      {
        "name": "Doodle God: Fantasy World of Magic",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Sta a te creare un mondo di magia e di fantasia. Diventa un vero mago!\r\n\r\n",
          "long_description":
              "Sta a te creare un mondo di magia e di fantasia. Diventa un vero mago!\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3257/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3257/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3257/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3257/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3257/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3257/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3257/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3257/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3257,
        "html5_shortname": "",
        "version_id": 33296052,
        "package_names": ["joybits.doodlegod_magic.alt_android.nst"]
      },
      {
        "name": "Doodle Kingdom",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description": "Crea subito un nuovo regno!",
          "long_description": "Crea subito un nuovo regno!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3011/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3011/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3011/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3011/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3011/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3011/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3011/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3011/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3011,
        "html5_shortname": "",
        "version_id": 33296126,
        "package_names": ["com.joybits.doodlekingdom"]
      },
      {
        "name": "Doodle God: Rocket Scientist",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Scatena l'astrofisico che c'è in te e inventa cose meravigliose! :)",
          "long_description":
              "Scatena l'astrofisico che c'è in te e inventa cose meravigliose! :)"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3258/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3258/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3258/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3258/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3258/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3258/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3258/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3258/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3258,
        "html5_shortname": "",
        "version_id": 33296054,
        "package_names": ["joybits.doodle_rocket_scientist.alt_android.nst"]
      },
      {
        "name": "Doodle Mafia",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description":
              "MENTRE GIOCHI, LA TUA CITTÀ DEL CRIMINE PRENDERÀ VITA DAVANTI AI TUOI OCCHI!",
          "long_description":
              "MENTRE GIOCHI, LA TUA CITTÀ DEL CRIMINE PRENDERÀ VITA DAVANTI AI TUOI OCCHI!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3083/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3083/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3083/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3083/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3083/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3083/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3083/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3083/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3083,
        "html5_shortname": "",
        "version_id": 33295862,
        "package_names": ["joybits.doodlemafia2"]
      },
      {
        "name": "Doodle carri armati",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description": "PROGETTA CARRI ARMATI E VEICOLI CORAZZATI!",
          "long_description": "PROGETTA CARRI ARMATI E VEICOLI CORAZZATI!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3084/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3084/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3084/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3084/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3084/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3084/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3084/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3084/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3084,
        "html5_shortname": "",
        "version_id": 33296130,
        "package_names": ["com.joybits.doodletanks"]
      },
      {
        "name": "Doubleside Mahjong Amazonka 2",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description":
              "Tuffati in un mondo di tribù e spiriti. Questo gioco di Mahjong è familiare e insolito allo stesso tempo.\r\n\r\n",
          "long_description":
              "Nuovi puzzle mahjong ambientati nell'enigmatico mondo dell'Amazzonia.\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3818/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3818/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3818/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3818/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3818/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3818/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3818/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3818/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3818,
        "html5_shortname": "",
        "version_id": 33300406,
        "package_names": ["com.Nomoc.MahjongAmazonka2forpartners"]
      },
      {
        "name": "Doubleside Mahjong Zen",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "",
        "size": "N/A",
        "descriptions": {
          "short_description": "Doubleside Mahjong Zen - Raddoppia l'emozione.",
          "long_description":
              "Un puzzle mahjong che ti porterà in un mondo lontano."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3171/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3171/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3171/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3171/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3171/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3171/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3171/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3171/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3171,
        "html5_shortname": "",
        "version_id": 33295990,
        "package_names": ["com.Qplaze.DoublesideMahjongZen.s5innovations"]
      },
      {
        "name": "Gatto Parlante Emma",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Un animale virtuale che piacerà a tutti: Emma, il gatto parlante!\r\n\r\n",
          "long_description":
              "Un animale virtuale che piacerà a tutti: Emma, il gatto parlante!\r\n\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3421/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3421/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3421/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3421/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3421/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3421/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3421/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3421/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3421,
        "html5_shortname": "",
        "version_id": 33295420,
        "package_names": ["com.mycatemma.virtualpetpro"]
      },
      {
        "name": "Escape from the Plateau",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Divertiti con la migliore avventura match-3 per cellulari!\r\n",
          "long_description":
              "Divertiti con la migliore avventura match-3 per cellulari!\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3475/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3475/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3475/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3475/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3475/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3475/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3475/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3475/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3475,
        "html5_shortname": "",
        "version_id": 33295446,
        "package_names": ["com.js.escapeplateaufull"]
      },
      {
        "name": "Guerriero Galattico",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Dimostra di essere il miglior pilota della galassia!",
          "long_description":
              "Dimostra di essere il miglior pilota della galassia!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3433/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3433/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3433/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3433/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3433/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3433/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3433/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3433/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3433,
        "html5_shortname": "",
        "version_id": 33295628,
        "package_names": ["com.qplaze.galaxywarfighting"]
      },
      {
        "name": "JOYBITS Ice Crystal Adventure",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3479/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3479/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3479/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3479/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3479/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3479/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3479/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3479/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3479,
        "html5_shortname": "",
        "version_id": 33295624,
        "package_names": ["com.js.icecrystalsfull"]
      },
      {
        "name": "Mahjong Secrets",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Immergiti in un avvincente mondo di misteri regali in questo nuovo elettrizzante gioco Mahjong!",
          "long_description":
              "Immergiti in un avvincente mondo di misteri regali, in questo nuovo ed elettrizzante gioco mahjong!\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3267/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3267/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3267/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3267/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3267/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3267/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3267/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3267/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3267,
        "html5_shortname": "",
        "version_id": 33300358,
        "package_names": ["com.dikobraz.mahjongsecrets"]
      },
      {
        "name": "Mind Games",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Un gioco per rilassarsi... e allo stesso tempo far lavorare la mente!\r\n\r\n",
          "long_description":
              "Al momento, ci sono 17 tipi diversi di puzzle, e più di 180 livelli.\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3437/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3437/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3437/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3437/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3437/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3437/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3437/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3437/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3437,
        "html5_shortname": "",
        "version_id": 33295970,
        "package_names": ["com.oxothuk.worldpuzzlenmc"]
      },
      {
        "name": "JOYBITS My Talking Dog - Virtual Pet",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3431/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3431/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3431/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3431/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3431/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3431/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3431/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3431/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3431,
        "html5_shortname": "",
        "version_id": 33294952,
        "package_names": ["com.mytalkingdogvirtualpet.puppygamespro"]
      },
      {
        "name": "GIOCHI RETRO REALI",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description": "I giochi retrò sono l'anima dei videogiochi.",
          "long_description":
              "Real Retro Games è una raccolta dei migliori giochi della console più popolare degli anni '90: i giochi di mattoncini."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3170/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3170/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3170/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3170/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3170/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3170/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3170/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3170/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3170,
        "html5_shortname": "",
        "version_id": 33295918,
        "package_names": ["com.Qplaze.RealRetroGames.s5innovations"]
      },
      {
        "name": "Real Retro Games 2",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "I giochi retrò sono l'anima dei videogiochi.\r\n\r\n",
          "long_description":
              "Real Retro Games II è una raccolta dei migliori giochi per la console più popolare degli anni '90: i giochi di mattoncini.\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3436/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3436/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3436/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3436/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3436/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3436/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3436/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3436/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3436,
        "html5_shortname": "",
        "version_id": 33296070,
        "package_names": ["com.Qplaze.RealRetroGames2.s5innovations"]
      },
      {
        "name": "Carri armati veri",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Prendi la cartuccia arancione, inseriscila nella console e divertiti con i cari, vecchi carri armati.",
          "long_description": "Torna alla tua infanzia, con Real Tanks!\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3438/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3438/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3438/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3438/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3438/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3438/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3438/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3438/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3438,
        "html5_shortname": "",
        "version_id": 33295872,
        "package_names": ["com.Qplaze.RealTanks.s5innovations"]
      },
      {
        "name": "Rescue Lucy",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Colleziona una varietà di oggetti combinandoli per aprire le porte.",
          "long_description":
              "Colleziona una varietà di oggetti combinandoli per aprire le porte."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3439/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3439/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3439/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3439/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3439/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3439/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3439/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3439/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3439,
        "html5_shortname": "",
        "version_id": 33295926,
        "package_names": ["air.RescueLucy"]
      },
      {
        "name": "Solitaire Mystery: Four Seasons",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Ripara l'orologio magico e riporta la tranquillità nella tua città!\r\n\r\n",
          "long_description":
              "Ripara l'orologio magico e riporta la tranquillità nella tua città!\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3265/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3265/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3265/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3265/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3265/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3265/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3265/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3265/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3265,
        "html5_shortname": "",
        "version_id": 33296066,
        "package_names": ["com.dikobraz.magiccards2"]
      },
      {
        "name": "Solitaire Mystery: Stolen Power",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Salva il mondo divertendoti in un avvincente mix di due giochi estremamente popolari!\r\n",
          "long_description":
              "Salva il mondo divertendoti con un avvincente mix di gioco: ricerca di oggetti nascosti e solitario!\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3270/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3270/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3270/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3270/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3270/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3270/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3270/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3270/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3270,
        "html5_shortname": "",
        "version_id": 33300360,
        "package_names": ["com.dikobraz.magiccards1"]
      },
      {
        "name": "Splash Rush",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description":
              "Divertiti con la migliore avventura match-3 per cellulari!",
          "long_description":
              "Divertiti con la migliore avventura match-3 per cellulari!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3236/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3236/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3236/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3236/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3236/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3236/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3236/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3236/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3236,
        "html5_shortname": "",
        "version_id": 33295678,
        "package_names": ["com.joybits.splashrush"]
      },
      {
        "name": "The Great Unknown: Houdini's Castle",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "La tua luna di miele viene interrotta quando il tuo aereo precipita su un'isola inesplorata.",
          "long_description":
              "La tua luna di miele viene interrotta quando il tuo aereo precipita su un'isola inesplorata."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3271/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3271/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3271/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3271/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3271/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3271/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3271/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3271/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3271,
        "html5_shortname": "",
        "version_id": 33295648,
        "package_names": ["com.dikobraz.houdini"]
      },
      {
        "name": "JOYBITS Triple Saga",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "rating": "4.9",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3238/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3238/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3238/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3238/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3238/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3238/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3238/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3238/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3238,
        "html5_shortname": "",
        "version_id": 33300354,
        "package_names": ["com.joybits.triplesaga"]
      },
      {
        "name": "The Surprising Adventures of Munchausen",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description":
              "Immergiti in un mondo bellissimo, pieno di avventure e intrighi!\r\n",
          "long_description": "Aiuta Munchausen a salvare il regno!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3277/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3277/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3277/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3277/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3277/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3277/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3277/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3277/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3277,
        "html5_shortname": "",
        "version_id": 33300362,
        "package_names": ["com.dikobraz.munchhausen"]
      },
      {
        "name": "Kingdoms and Lords Mobile Premium",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "",
        "size": "N/A",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1652/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/1652/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/1652/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1652/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1652/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1652/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1652/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1652/default/screenshot/4.jpg"
          ]
        },
        "product_id": 1652,
        "html5_shortname": "",
        "version_id": 33831234,
        "package_names": [
          "com.gameloft.android.GAND.GloftKLMP",
          "com.gameloft.android.GAND.GloftKLMF",
          "com.gameloft.android.ALCH.GloftKLMP"
        ]
      },
      {
        "name": "Zombie Raid: Survival",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "12+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Cosa faresti se dovesse scoppiare un'improvvisa apocalisse degli zombi e vicino a te ci fosse una base militare?\r\n\r\n\r\n",
          "long_description":
              "Afferra un'ascia e inizia una vera e propria incursione attraverso orde di zombi.\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3817/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3817/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3817/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3817/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3817/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3817/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3817/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3817/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3817,
        "html5_shortname": "",
        "version_id": 33295952,
        "package_names": ["com.qplaze.zombieraid"]
      },
      {
        "name": "Midnight Pool",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "",
        "size": "N/A",
        "rating": "4.62",
        "descriptions": {
          "short_description":
              "Il gioco di biliardo più realistico, con colpi speciali, una modalità Carriera e partite online!",
          "long_description":
              "Il gioco di biliardo più realistico!\r\nDivertiti e mettiti alla prova col più realistico gioco di biliardo di sempre.\r\nAffronta la nuova modalità Carriera con 100 missioni e 7 boss da battere.\r\nSfida gli amici online e dimostra al mondo intero chi è il migliore.\r\nVisita tanti locali e sfida i giocatori migliori in un'atmosfera anni '80 unica.\r\nColleziona moltissime stecche, ognuna con caratteristiche e stile unici."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2305/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/2305/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/2305/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2305/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2305/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2305/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2305/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2305/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 2305,
        "html5_shortname": "",
        "version_id": 33196540,
        "package_names": [
          "com.gameloft.android.GAND.GloftMPMP",
          "com.gameloft.android.ALCH.GloftMPMP"
        ]
      },
      {
        "name": "Little Big City 2",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "9+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Trasforma la tua isola tropicale in un'effervescente e bellissima metropoli! Collaborerai con il sindaco e i suoi bizzarri assistenti per costruire la città dei tuoi sogni: una potenza tecnologica e industriale, ma anche un'oasi ecologica!",
          "long_description":
              "Espandi i confini e incoraggia nuovi cittadini a stabilirsi nella tua città!\r\nProduci risorse uniche per ottenere fondi per la tua città!\r\nScopri i tesori nascosti dell'isola e usali a tuo vantaggio!\r\nStudia la strategia migliore per sviluppare la tua città!\r\nRisolvi tanti problemi diversi affinché tutto vada per il meglio in città!\r\nScegli se puntare sullo sviluppo culturale, industriale o tecnologico!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2721/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/2721/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/2721/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2721/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2721/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2721/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2721/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2721/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 2721,
        "html5_shortname": "",
        "version_id": 33196536,
        "package_names": [
          "com.gameloft.android.GAND.GloftLB2P",
          "com.gameloft.android.ALCH.GloftLB2P"
        ]
      },
      {
        "name": "Monsters University",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "",
        "size": "N/A",
        "rating": "5",
        "descriptions": {
          "short_description":
              "Il gioco dell'attesissimo film \"Monsters University\" della Disney/Pixar è arrivato!",
          "long_description":
              "Immergiti nel mondo da brivido di Monsters University\r\nGioca nei panni di Mike e Sulley superando gli ostacoli col lavoro di squadra\r\nProcedi attraverso 10 livelli ambientati in 5 ambientazioni nel campus\r\nCorri, rema e spaventa a più non posso in questa ruggente avventura\r\nSblocca i bonus e affronta nemici mostruosi e divertentissimi"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1743/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/1743/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/1743/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1743/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1743/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1743/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1743/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1743/default/screenshot/4.jpg"
          ]
        },
        "product_id": 1743,
        "html5_shortname": "",
        "version_id": 33831232,
        "package_names": [
          "com.gameloft.android.GAND.GloftMOUN",
          "com.gameloft.android.GloftMOUN",
          "com.gameloft.android.ALCH.GloftMOUN"
        ]
      },
      {
        "name": "Modern Combat 4: Zero Hour",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "",
        "size": "N/A",
        "rating": "2.84",
        "descriptions": {
          "short_description":
              "Salva i leader mondiali da un gruppo di terroristi!\r\n",
          "long_description":
              "Una storia avvincente scritta da sceneggiatori professionisti\r\nDai la caccia al nemico in tutto il mondo, dal Sudafrica a Barcellona\r\nGrande varietà di gioco: eventi rapidi, uccisioni furtive, droni e molto altro!\r\n2 nuove modalità: Sopravvivenza e Infiltrazione\r\nUn mix di sequenze a scorrimento laterale, alla guida di veicoli e da cecchino\r\nScopri 9 livelli esplosivi\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1660/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/1660/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/1660/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1660/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1660/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1660/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1660/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1660/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 1660,
        "html5_shortname": "",
        "version_id": 33196544,
        "package_names": [
          "com.gameloft.android.GAND.GloftMC4M",
          "com.gameloft.android.LATAM.BK25",
          "com.gameloft.android.GloftMC4M",
          "com.gameloft.android.LATAM.BS47",
          "com.gameloft.android.GAND.GloftSCRT",
          "com.gameloft.android.ALCH.GloftMC4M",
          "com.gameloft.android.HEP.GloftMZHP"
        ]
      },
      {
        "name": "Motocross : Trial Extreme Mobile Premium",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "",
        "size": "N/A",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1268/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/1268/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/1268/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1268/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1268/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1268/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1268/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1268/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 1268,
        "html5_shortname": "",
        "version_id": 33196548,
        "package_names": [
          "com.gameloft.android.GAND.GloftMOTR",
          "com.gameloft.android.BFLL.GloftMOTR",
          "com.gameloft.android.GloftMOTR"
        ]
      },
      {
        "name": "N.O.V.A. 3",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Combatti per la sopravvivenza della razza umana nel più grande sparatutto per cellulari!",
          "long_description":
              "Combatti per la sopravvivenza della razza umana nel più grande sparatutto per cellulari!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1627/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/1627/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/1627/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1627/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1627/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1627/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1627/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1627/default/screenshot/4.jpg"
          ]
        },
        "product_id": 1627,
        "html5_shortname": "",
        "version_id": 33227446,
        "package_names": [
          "com.gameloft.android.GAND.GloftNV3M",
          "com.gameloft.android.GAND.GloftN3CY",
          "com.gameloft.android.GloftNV3M",
          "com.gameloft.android.ALCH.GloftNV3M"
        ]
      },
      {
        "name": "Mini Metro",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Traccia linee tra le stazioni e fai partire i tuoi treni.\r\n",
          "long_description":
              "Espansione casuale, così ogni partita è unica. 15 città del mondo reale ti permettono di mettere alla prova le tue capacità. Una varietà di aggiornamenti ti consente di personalizzare la tua rete.\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3702/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3702/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3702/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3702/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3702/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3702/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3702/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3702/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3702,
        "html5_shortname": "",
        "version_id": 33295454,
        "package_names": ["nz.co.codepoint.minimetro"]
      },
      {
        "name": "N.O.V.A. Legacy",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "",
        "size": "N/A",
        "rating": "4.77",
        "descriptions": {
          "short_description":
              "N.O.V.A. Legacy è un FPS fantascientifico in una versione compatta da 20 MB.",
          "long_description":
              "Una qualità da console su cellulare.\r\nSconfiggi le forze aliene in varie modalità di gioco.\r\nMettiti alla prova nelle arene multigiocatore.\r\nMigliora le armi, come potenti fucili d'assalto e devastanti fucili al plasma.\r\nIl divertimento del N.O.V.A. originale, con un gameplay e una grafica migliori.\r\nTutto in una versione compatta da 20 MB."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2331/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/2331/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/2331/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2331/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2331/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2331/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2331/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2331/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 2331,
        "html5_shortname": "",
        "version_id": 33196546,
        "package_names": [
          "com.gameloft.android.GAND.GloftNOMP",
          "com.gameloft.android.GloftNOMP",
          "com.gameloft.android.ALCH.GloftNOMP"
        ]
      },
      {
        "name": "Dash Masters",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description":
              "Le regole sono semplici. Tocca per saltare in direzioni alternate.\r\n",
          "long_description":
              "Un gameplay fresco e unico, 64 missioni impressionanti e un esoscheletro da indossare per una maggiore efficacia.\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3312/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3312/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3312/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3312/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3312/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3312/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3312/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3312/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3312,
        "html5_shortname": "",
        "version_id": 33295904,
        "package_names": ["com.playmous.dashmasters"]
      },
      {
        "name": "God of light",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Gioca a questa pietra miliare, nuovo paradigma nel genere dei rompicapi fisici.",
          "long_description":
              "Esplora 6 diversi mondi di gioco e 150 livelli.\r\nUsa specchi, prismi, divisori, buchi neri e filtri per controllare i raggi di energia luminosa."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3243/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3243/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3243/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3243/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3243/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3243/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3243/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3243/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3243,
        "html5_shortname": "",
        "version_id": 33295956,
        "package_names": ["com.playmous.godoflightXendex"]
      },
      {
        "name": "Paint the Frog",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description":
              "Effettua combo epiche per diventare il più grande pittore di rane dell'universo.\r\n\r\n",
          "long_description":
              "Combo epiche, vari livelli e potenziamenti super.\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3348/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3348/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3348/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3348/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3348/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3348/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3348/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3348/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3348,
        "html5_shortname": "",
        "version_id": 33296068,
        "package_names": ["com.playmous.paintthefrog"]
      },
      {
        "name": "PLAYMOUS Ride with the Frog",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3511/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3511/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3511/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3511/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3511/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3511/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3511/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3511/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3511,
        "html5_shortname": "",
        "version_id": 33295890,
        "package_names": ["com.playmous.ridewiththefrog"]
      },
      {
        "name": "Tap the Frog Faster",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Cra! La rana leggendaria è tornata con una trama tutta nuova e avvincente!",
          "long_description":
              "Padroneggia 15 minigiochi raneschi. Nuovi fantastici livelli da raggiungere e condividere con gli amici. Colleziona e potenzia carte speciali per accedere a nuove abilità.\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3605/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3605/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3605/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3605/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3605/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3605/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3605/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3605/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3605,
        "html5_shortname": "",
        "version_id": 33296074,
        "package_names": ["com.playmous.ttf3.o"]
      },
      {
        "name": "Tap the Frog",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Il gioco Tap the Frog ha compiuto 5 anni! Unisciti a 50.000.000 di giocatori a Tap the Frog!",
          "long_description":
              "28 ran-tastici minigiochi! Multigiocatore per divertirsi in famiglia! Sfide divertenti da condividere con gli amici!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3022/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3022/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3022/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3022/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3022/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3022/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3022/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3022/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3022,
        "html5_shortname": "",
        "version_id": 33296128,
        "package_names": [
          "com.playmous.ttfHD.offstore",
          "com.dailymotion.dailymotion"
        ]
      },
      {
        "name": "Tap the Frog: Doodle",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Gioca a Tap the Frog e scopri DECINE di MINIGIOCHI in una sola app! Unisciti a 12 milioni di giocatori in tutto il mondo!\r\n\r\n",
          "long_description":
              "40 LIVELLI pieni di AZIONE, dei distintivi e molto altro ancora! 12.000.000 di giocatori di Tap the Frog nel mondo! IL GIOCO N. 1 IN MOLTI PAESI.\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3286/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3286/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3286/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3286/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3286/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3286/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3286/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3286/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3286,
        "html5_shortname": "",
        "version_id": 33296032,
        "package_names": ["com.playmous.ttfdoodleoffstore"]
      },
      {
        "name": "Platinum Solitaire 3",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Ti presentiamo Platinum Solitaire 3, la nuovissima versione del gioco di carte più famoso.",
          "long_description":
              "Gioca a Solitario per ore e cerca di battere il tuo record\r\nDivertimento assicurato con 17 giochi di carte come Spider e FreeCell\r\nTutorial facili e consigli in gioco per divertirti senza aspettare\r\nVinci grosse somme di denaro e diventa famoso a Rio, Dubai, Parigi e altre città\r\nSblocca un sacco di nuovi contenuti"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1163/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/1163/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/1163/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1163/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1163/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1163/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1163/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1163/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 1163,
        "html5_shortname": "",
        "version_id": 33831230,
        "package_names": [
          "com.gameloft.android.GAND.GloftPSO3.ML",
          "com.gameloft.android.LATAM.GloftPSO3",
          "com.gameloft.android.GAND.GloftPSO3.CH",
          "com.gameloft.android.GloftPSO3",
          "com.gameloft.android.BFLL.GloftTHM2",
          "com.gameloft.android.GAND.GloftPSO3",
          "com.gameloft.android.SFCL.GloftPSO3"
        ]
      },
      {
        "name": "Puzzle Pets Mobile Premium",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1740/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/1740/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/1740/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1740/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1740/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1740/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1740/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1740/default/screenshot/4.jpg"
          ]
        },
        "product_id": 1740,
        "html5_shortname": "",
        "version_id": 33831226,
        "package_names": [
          "com.gameloft.android.GAND.GloftDBMP",
          "com.gameloft.android.ALCH.GloftDBMP",
          "com.gameloft.android.GAND.GloftDBMF"
        ]
      },
      {
        "name": "Rival Wheels: Moto Drag Racing",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "rating": "4.34",
        "descriptions": {
          "short_description":
              "Preparati per il gioco di corse in moto su rettilineo più competitivo e avvincente su cellulare!",
          "long_description":
              "Più di 15 moto su licenza dei costruttori più famosi.\r\nComandi semplici: tocca per cambiare marcia, ma fallo al momento giusto!\r\nDimensioni ridotte per permettere a tutti di scaricare il divertimento!\r\nUna qualità grafica estrema: ogni moto sembra un vero bolide da corsa!\r\nGareggia a Tokyo, L'Avana e New York."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3497/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/3497/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/3497/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3497/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3497/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3497/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3497/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3497/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 3497,
        "html5_shortname": "",
        "version_id": 33196538,
        "package_names": ["com.gameloft.android.GAND.GloftRWMP"]
      },
      {
        "name": "Altered Beast",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "12+",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description":
              "Entra nel mondo sotterraneo del leggendario picchiaduro di SEGA Altered Beast, ora per cellulari.",
          "long_description":
              "Avventurati nel mondo sotterraneo del leggendario picchiaduro di SEGA Altered Beast e combatti le orde dell'inferno!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3864/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3864/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3864/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3864/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3864/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3864/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3864/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3864/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3864,
        "html5_shortname": "",
        "version_id": 33300412,
        "package_names": ["com.sega.alteredbeast"]
      },
      {
        "name": "Comix Zone",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "9+",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description":
              "Entra in Comix Zone: tutta l'azione e l'avventura dei tuoi fumetti preferiti!\r\n",
          "long_description":
              "Entra in Comix Zone, il picchiaduro classico in stile arcade di SEGA! Gioca e preparati per il primo fumetto interattivo della storia!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3895/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3895/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3895/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3895/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3895/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3895/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3895/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3895/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3895,
        "html5_shortname": "",
        "version_id": 33300420,
        "package_names": ["com.sega.comixzone"]
      },
      {
        "name": "Golden Axe",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "12+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Lancia potenti magie e sbaraglia le truppe di Death Adder nel classico fantasy di SEGA, Golden Axe!\r\n",
          "long_description":
              "Lancia potenti magie e sbaraglia le truppe di Death Adder nel classico fantasy di SEGA, Golden Axe!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3833/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3833/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3833/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3833/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3833/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3833/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3833/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3833/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3833,
        "html5_shortname": "",
        "version_id": 33295600,
        "package_names": ["com.sega.goldenaxe"]
      },
      {
        "name": "Sonic The Hedgehog™ ",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Il gioco di Sonic che ha dato il via a tutti gli altri è ora ottimizzato per dispositivi portatili!\r\n",
          "long_description":
              "Sfreccia alla velocità della luce in sette zone classiche con Sonic the Hedgehog."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3910/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3910/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3910/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3910/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3910/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3910/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3910/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3910/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3910,
        "html5_shortname": "",
        "version_id": 33300456,
        "package_names": ["com.sega.sonic1"]
      },
      {
        "name": "Animal Park Tycoon Christmas",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Gestisci il tuo zoo per creare un parco magico per i tuoi visitatori durante le vacanze.",
          "long_description":
              "Quest'anno, prima di fare una fermata a casa tua, Babbo Natale farà un salto ad Animal Park Tycoon!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1672/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/1672/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/1672/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1672/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1672/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1672/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1672/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1672/default/screenshot/4.jpg"
          ]
        },
        "product_id": 1672,
        "html5_shortname": "",
        "version_id": 33295636,
        "package_names": ["com.shinypix.apt_winter"]
      },
      {
        "name": "Animal Park Tycoon Halloween",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Zombie, mutanti e mostri giganti: scopri uno zoo a tema Halloween con Animal Park Tycoon!",
          "long_description":
              "Zombie, mutanti e mostri giganti: scopri uno zoo a tema Halloween con Animal Park Tycoon! Gestisci lo zoo e crea il parco più spaventoso!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1643/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/1643/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/1643/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1643/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1643/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1643/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1643/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1643/default/screenshot/4.jpg"
          ]
        },
        "product_id": 1643,
        "html5_shortname": "",
        "version_id": 33295838,
        "package_names": ["com.shinypix.apt_halloween"]
      },
      {
        "name": "Dungeon Fever",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Sconfiggili con i tuoi goblin, le tue creature speciali e i tuoi potenti incantesimi!",
          "long_description":
              "Riprenditi le tue creature malvagie! Gli eroi del bene (umani, elfi, ecc.) cercheranno di fermarti o di rubare i tuoi tesori!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2653/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2653/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2653/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2653/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2653/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2653/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2653/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2653/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2653,
        "html5_shortname": "",
        "version_id": 33300478,
        "package_names": ["com.shinypix.dungeonfever.glp"]
      },
      {
        "name": "Dog Park Tycoon",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description":
              "Colleziona tanti bei cagnolini e costruisci per loro il parco più bello!\r\n\r\n",
          "long_description":
              "Colleziona 40 razze diverse, come rottweiler, golden retriever, carlini, shih tzu e molte altre!\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3804/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3804/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3804/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3804/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3804/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3804/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3804/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3804/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3804,
        "html5_shortname": "",
        "version_id": 33296012,
        "package_names": ["com.shinypix.parktycoon.dog.gl"]
      },
      {
        "name": "SHINYPIX Horse Park Tycoon",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "rating": "4.9",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3899/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3899/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3899/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3899/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3899/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3899/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3899/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3899/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3899,
        "html5_shortname": "",
        "version_id": 33295984,
        "package_names": ["com.shinypix.parktycoon.horse.gl"]
      },
      {
        "name": "Cappuccetto Rosso",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Riscopri la famosa favola di Cappuccetto Rosso.\r\n",
          "long_description": "Riscopri la famosa favola di Cappuccetto Rosso"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3279/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3279/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3279/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3279/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3279/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3279/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3279/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3279/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3279,
        "html5_shortname": "",
        "version_id": 33300364,
        "package_names": ["com.shinypix.thelittleredridinghoodgl"]
      },
      {
        "name": "Pudding Land",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Quando affronti gli oltre trenta livelli di Pudding Land, il divertimento è garantito!",
          "long_description":
              "Quanti di quei divertenti e variegati budini riuscirai a fare sparire in questo divertente gioco? Inoltre, puoi investire i tuoi crediti di gioco negli extra che troverai nel negozio."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2591/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2591/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2591/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2591/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2591/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2591/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2591/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2591/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2591,
        "html5_shortname": "",
        "version_id": 33300462,
        "package_names": ["puddingland.softgames.zed"]
      },
      {
        "name": "Sonic Advance™ ",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Gioca al classico Sonic Advance™ sul tuo dispositivo e prova la leggendaria velocità di Sonic!\r\n",
          "long_description":
              "Sonic il riccio è tornato con tutto il fascino retro della versione classica GBA\r\nAzioni in volo: Accelerazione, corsa rotante, attacco rotante doppio ecc...\r\nRotola per 4 mondi: Neo Green Hill, Secret Base, Angel Island, Casino Paradise\r\nUnisciti agli amici di Sonic: Tails, Knuckles e Amy Rose, e batti Eggman.\r\nScarica Sonic Advance™ e gioca in mod. storia o Contro il tempo"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1269/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/1269/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/1269/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1269/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1269/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1269/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1269/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1269/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 1269,
        "html5_shortname": "",
        "version_id": 33169652,
        "package_names": [
          "com.gameloft.android.GAND.GloftSOAD",
          "com.gameloft.android.BFLL.GloftSOAD",
          "com.gameloft.android.GAND.GloftBLB3",
          "com.gameloft.android.ALCH.GloftSOAD"
        ]
      },
      {
        "name": "Sonic Runners Adventure",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "rating": "4.68",
        "descriptions": {
          "short_description":
              "Sonic e i suoi amici sono tornati in un nuovissimo gioco runner! Rivivi le sue avventure supersoniche saltando, scattando e volando in strepitosi livelli per combattere Eggman in 4 aree leggendarie.",
          "long_description":
              "Salva l'universo di Sonic in un nuovo gioco runner senza fine!\r\nSblocca e potenzia tantissimi personaggi di Sonic come Tails e Knuckles!\r\n4 aree leggendarie e una modalità bonus con una nuova grafica spettacolare!\r\nBrani famosi dei giochi di Sonic precedenti per corse divertenti e nostalgiche!\r\nUna storia intensa e tanti obiettivi per divertirsi all'infinito!\r\nSistema di gioco basato sulle combo e una modalità di corsa automatica!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2729/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/2729/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/2729/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2729/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2729/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2729/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2729/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2729/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 2729,
        "html5_shortname": "",
        "version_id": 33196534,
        "package_names": [
          "com.gameloft.android.GAND.GloftSOMP",
          "com.gameloft.android.GAND.GloftSONQ",
          "com.gameloft.android.ALCH.GloftSOMP",
          "com.gameloft.android.GloftSOMP"
        ]
      },
      {
        "name": "Spider-Man: Ultimate Power ",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "12+",
        "size": "N/A",
        "rating": "4.38",
        "descriptions": {
          "short_description":
              "Il Goblin, L'Uomo Sabbia e Venom hanno unito le forze e rapito Mary Jane! Combatti per i tetti e le strade di Manhattan dondolandoti con la ragnatela per salvare la tua amata... e New York!",
          "long_description":
              "Un gioco dell'Uomo Ragno tra picchiaduro e arcade! Combatti a Manhattan!\r\n\"\r\nDai la caccia ai malfattori, sconfiggili e manda in fumo i loro piani malvagi!\"\r\n\" \r\nCombatti e dondolati in modo frenetico e divertente con due tocchi delle dita!\"\r\nSblocca versioni leggendarie dell'Uomo Ragno, come Miles Morales e Iron Spider!\r\nPotenziamenti basati sulle abilità dell'Uomo Ragno, come gli scudi di ragnatela!\r\n\"\r\nUsa il costume nero dell'Uomo Ragno per danni micidiali e record incredibili!\"\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1763/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/1763/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/1763/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1763/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1763/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1763/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1763/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1763/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 1763,
        "html5_shortname": "",
        "version_id": 33196532,
        "package_names": [
          "com.gameloft.android.GAND.GloftSMIM",
          "com.gameloft.android.GloftSMIF",
          "com.gameloft.android.GloftSMIM",
          "com.gameloft.android.ALCH.GloftSMIM"
        ]
      },
      {
        "name": "The Amazing Spider-Man 2 Mobile Premium",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "12+",
        "size": "N/A",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1871/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/1871/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/1871/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1871/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1871/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1871/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1871/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1871/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 1871,
        "html5_shortname": "",
        "version_id": 33831222,
        "package_names": [
          "com.gameloft.android.GAND.GloftAS2M",
          "com.gameloft.android.GloftAS2M"
        ]
      },
      {
        "name": "The Love Boat: Puzzle Cruise – Your Match 3 Crush!",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Il capitano Stubing è pronto a salpare di nuovo e ti ha ingaggiato come suo ufficiale di felicità! Aiuta l'equipaggio originale a decorare e progettare la nave in un'avventura romantica tutta nuova! Abbina oggetti gonfiabili nei rompicapo match-3!",
          "long_description":
              "Abbina oggetti gonfiabili colorati in un entusiasmante rompicapo match-3!\r\nNuove fantastiche funzionalità match-3 come la divertentissima ruota dei colori.\r\nIl telefilm tanto amato prosegue con momenti romantici e comici.\r\nOttieni le stelle per progredire nei livelli e nelle missioni.\r\nScegli decorazioni e arredi unici per condividere il tuo stile sulla nave!\r\nUna storia travolgente con ospiti stellari e l'equipaggio originale!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3579/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3579/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3579/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3579/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3579/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3579/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3579/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3579/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3579,
        "html5_shortname": "",
        "version_id": 33831218,
        "package_names": [
          "com.gameloft.android.GAND.GloftPCMP",
          "com.gameloft.android.GAND.GloftDI41"
        ]
      },
      {
        "name": "The Avengers - Il gioco per cellulari",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "12+",
        "size": "N/A",
        "rating": "2.5",
        "descriptions": {
          "short_description":
              "I Vendicatori uniti per fermare l'invasione aliena di Loki!",
          "long_description":
              "Gioca con 4 Vendicatori (Hulk, Thor, Capitan America e Iron Man)\r\nOgni Vendicatore ha i propri attacchi e poteri speciali\r\nCombatti contro un esercito di nemici e boss\r\nGiocabilità varia con livelli in volo, combo, attacchi a distanza e potenziamenti da sbloccare\r\nLuoghi e ambientazioni ricreati fedelmente dal film"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1504/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/1504/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/1504/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1504/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1504/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1504/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1504/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1504/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 1504,
        "html5_shortname": "",
        "version_id": 32570315,
        "package_names": [
          "com.gameloft.android.GAND.GloftAVEN",
          "com.gameloft.android.GloftAVEN",
          "com.gameloft.android.LATAM.GloftAVEN",
          "com.gameloft.android.LATAM.BB62",
          "com.gameloft.android.LATAM.BF87",
          "com.gameloft.android.LATAM.BS45",
          "com.gameloft.android.SFCL.GloftAVEN",
          "com.gameloft.android.ALCH.GloftAVEN"
        ]
      },
      {
        "name": "Total Conquest ",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Unisciti a legioni di giocatori online nella battaglia per il controllo dell'impero romano!",
          "long_description":
              "Costruisci e gestisci la tua città-stato.\r\nAssolda, addestra e potenzia 10 tipi diversi di unità, ognuna con un ruolo specifico.\r\nCombatti contro giocatori di tutto il mondo e porta il tuo esercito alla vittoria.\r\nCrea un'alleanza o unisciti a una già esistente e ottieni rinforzi dagli altri membri.\r\nCoordinati con i tuoi alleati per vincere le guerre tra alleanze."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1770/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/1770/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/1770/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1770/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1770/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1770/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1770/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1770/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 1770,
        "html5_shortname": "",
        "version_id": 33831214,
        "package_names": ["com.gameloft.android.GAND.GloftTCMP"]
      },
      {
        "name": "Wonder Zoo",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Esplora la natura per trovare gli animali, salvarli e portarli nel tuo Wonder Zoo personale!",
          "long_description":
              "Gestisci il tuo zoo e incoraggia i visitatori.\r\nAdotta animali e crea nuove specie per far diventare famoso il tuo zoo.\r\nPersonalizza lo zoo con i vari ambienti ed edifici disponibili nel negozio.\r\nGioca a 3 minigiochi: Piatti bestiali, Coppie scoppiate e Genitori cercansi.\r\nCompleta le missioni dei visitatori, passa di livello e sblocca contenuti."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1502/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/1502/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/1502/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1502/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1502/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1502/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1502/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1502/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 1502,
        "html5_shortname": "",
        "version_id": 33831210,
        "package_names": [
          "com.gameloft.android.GAND.GloftWOOP",
          "com.gameloft.android.SFCL.GloftWOOP",
          "com.gameloft.android.SFCL.GloftZOOM",
          "com.gameloft.android.GAND.GloftWZAR",
          "com.gameloft.android.LATAM.GloftZOOM",
          "com.gameloft.android.GAND.GloftZOOM",
          "com.gameloft.android.GAND.GloftWZMR",
          "com.gameloft.android.BFLL.GloftWOOP"
        ]
      },
      {
        "name": "World at Arms - Combatti per la tua nazione!",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Combatti per la tua nazione contro la diabolica ARC!",
          "long_description":
              "Affronta innovative battaglie intorno al mondo, in diversi ambienti.\r\nRaccogli risorse, costruisci e potenzia edifici, e unisci unità.\r\nCombatti a terra e nei cieli con tante unità diverse!\r\nImmergiti in una campagna lunga, ricca e dinamica!\r\nSblocca tantissimi trofei!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1718/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/1718/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/1718/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1718/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1718/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1718/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1718/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1718/default/screenshot/4.jpg"
          ]
        },
        "product_id": 1718,
        "html5_shortname": "",
        "version_id": 33831206,
        "package_names": [
          "com.gameloft.android.GAND.GloftWAMP",
          "com.gameloft.android.GloftWAMP",
          "com.gameloft.android.ALCH.GloftWAMP"
        ]
      },
      {
        "name": "Cut The Rope",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Risolvi difficili rompicapo e goditi il Natale con Babbo Natale.",
          "long_description":
              "Il Natale è in pericolo!\r\nUnisciti a Om Nom e Om Nelle per aiutare Babbo Natale! Risolvi difficili rompicapo e goditi il Natale con Babbo Natale."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3002/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3002/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3002/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3002/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3002/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3002/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3002/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3002/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3002,
        "html5_shortname": "",
        "version_id": 33295464,
        "package_names": ["com.zeptolab.ctror.subscr.tier"]
      },
      {
        "name": "Cut the Rope 2",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "La seconda parte della leggendaria serie di enigmi di logica Cut the Rope. Ottienila gratuitamente!",
          "long_description":
              "CHE DOLCE! Gli scherzi di Om Nom continuano in Cut the Rope 2! INCONTRA I NUOVI AMICI DI OM NOM, I NOMMIES! NUOVE AVVENTURE PER OM NOM!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3029/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3029/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3029/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3029/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3029/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3029/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3029/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3029/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3029,
        "html5_shortname": "",
        "version_id": 33294920,
        "package_names": ["com.zeptolab.ctr2.subscr.tier"]
      },
      {
        "name": "Cut the Rope: Experiments",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Fai mangiare le caramelle a Om Nom in una serie di esperimenti divertenti!",
          "long_description":
              "Fai mangiare le caramelle a Om Nom in una serie di esperimenti emozionanti! 8 pacchetti con 200 livelli, meccanica di gioco con fisica innovativa e un personaggio adorabile."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3015/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3015/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3015/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3015/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3015/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3015/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3015/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3015/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3015,
        "html5_shortname": "",
        "version_id": 33295644,
        "package_names": ["com.zeptolab.ctrex.subscr.tier"]
      },
      {
        "name": "Cut the Rope: Holiday Gift",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Holiday Gift combina una fisica incredibile con dei livelli impegnativi e un look natalizio.",
          "long_description":
              "Holiday Gift include una fisica notevole, livelli diabolicamente difficili e colori vivaci ispirati alle festività invernali."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3062/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3062/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3062/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3062/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3062/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3062/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3062/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3062/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3062,
        "html5_shortname": "",
        "version_id": 33300454,
        "package_names": ["com.zeptolab.holiday.premium.tier"]
      },
      {
        "name": "Cut the Rope: Magic",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Incontra il Camaleonte: la sua lunghissima lingua è sempre a caccia di qualcosa di dolce.",
          "long_description":
              "Incontra il Camaleonte: la sua lunghissima lingua appiccicosa è sempre a caccia di qualcosa di dolce."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3013/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3013/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3013/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3013/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3013/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3013/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3013/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3013/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3013,
        "html5_shortname": "",
        "version_id": 33294932,
        "package_names": ["com.zeptolab.ctrm"]
      },
      {
        "name": "Cut the Rope: Time Travel",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "rating": "5",
        "descriptions": {
          "short_description": "Viaggia indietro nel tempo insieme a Om Nom!",
          "long_description":
              "Grafica accattivante ispirata a diverse epoche storiche. Cortometraggi animati del delizioso Om Nom corrispondenti a ciascuna epoca!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3014/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3014/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3014/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3014/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3014/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3014/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3014/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3014/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3014,
        "html5_shortname": "",
        "version_id": 33300446,
        "package_names": ["com.zeptolab.ctrtt.subscr.tier"]
      },
      {
        "name": "King of Thieves",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "rating": "4.9",
        "descriptions": {
          "short_description":
              "Eludi le trappole e ruba l'oro, in questo gioco che è un mix tra platform e PVP!",
          "long_description":
              "RUBA LE COSE CHE LUCCICANO! Ottieni oro e gemme dagli altri giocatori per diventare il ladro più ricco del mondo!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3009/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3009/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3009/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3009/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3009/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3009/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3009/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3009/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3009,
        "html5_shortname": "",
        "version_id": 33290474,
        "package_names": ["com.zeptolab.thieves.tiertwo"]
      },
      {
        "name": "Pudding Monsters",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Appiccicosi, curiosi e... DECISI A DIVENTARE PIÙ GRANDI!",
          "long_description":
              "Forme di mostro uniche, personalità e attributi stravaganti. Un gioco divertente e appassionante adatto a tutte le età."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3019/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3019/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/3019/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3019/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3019/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3019/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3019/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3019/default/screenshot/4.jpg"
          ]
        },
        "product_id": 3019,
        "html5_shortname": "",
        "version_id": 33295748,
        "package_names": ["com.zeptolab.monsters.paid.oregon"]
      }
    ]
  },
  {"name": "Ready, set, go!", "type": "manual", "products": []},
  {
    "name": "New Releases",
    "type": "manual",
    "products": [
      {
        "name": "Real Football 2019",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "12+",
        "size": "N/A",
        "rating": "4.24",
        "descriptions": {
          "short_description":
              "Lotta per la gloria nella Coppa Internazionale 2018! Affronta i tuoi amici e dai il massimo!",
          "long_description":
              "Scopri il gioco di calcio più emozionante di sempre!\r\nVinci la Coppa Internazionale 2018 e diventa una leggenda!\r\nSfida giocatori da tutto il mondo!\r\nGestisci la squadra dei tuo sogni!\r\nRivivi partite leggendarie nella nuova modalità Sfida storica!\r\nMigliora le strutture del tuo club e sblocca il potenziale della tua squadra!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3452/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/3452/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/3452/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3452/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3452/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3452/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3452/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3452/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 3452,
        "html5_shortname": "",
        "version_id": 33196542,
        "package_names": ["com.gameloft.android.GAND.GloftR19F"]
      },
      {
        "name": "Asphalt Nitro",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "rating": "3.66",
        "descriptions": {
          "short_description":
              "Fai mangiare la polvere agli avversari! Divertiti con la miglior esperienza di guida per cellulari!\r\n",
          "long_description":
              "Guida lussuose auto e spingile oltre i loro limiti!\r\nSali sulle rampe ed esegui delle manovre in aria mentre fai delle acrobazie\r\nSfida gli avversari in differenti ed eccitanti modalità di gioco!\r\nLa modalità \"Guardie\" è di ritorno. Mettiti sulle tracce dei fuorilegge!\r\nAmmira gli incredibili paesaggi, realizzati con una grafica spettacolare! \r\nMettiti alla ricerca delle tante scorciatoie e taglia per primo il traguardo!\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2036/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/2036/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/2036/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2036/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2036/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2036/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2036/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2036/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 2036,
        "html5_shortname": "",
        "version_id": 33196524,
        "package_names": [
          "com.gameloft.android.FVGL.GloftANIM",
          "com.gameloft.android.FVGL.GloftDA97",
          "com.gameloft.android.FVGL.GloftDA40",
          "com.gameloft.android.GAND.GloftRF16",
          "com.gameloft.android.GloftANIM",
          "com.gameloft.android.GAND.GloftANIM",
          "com.gameloft.android.ALCH.GloftANIM"
        ]
      },
      {
        "name": "Cars - Sfide ruggenti",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "rating": "4.63",
        "descriptions": {
          "short_description":
              "Saetta McQueen e i suoi amici sono tornati per correre intorno al mondo in un torneo spettacolare!",
          "long_description":
              "Il gioco ufficiale di Cars della DisneyPixar, con tutto il divertimento dei film!\r\nCorse veloci, emozionanti e piene di adrenalina con divertenti comandi tattili!\r\nSblocca, potenzia e corri con 15 personaggi, tra cui Francesco, Holley, Cricchetto e Saetta McQueen!\r\nDivertimento infinito! Gioca in modalità Classica o affronta i boss in modalità Storia!\r\n5 capitoli, 70 eventi e varie tipologie di gioco, tra cui il minigioco dei trattori di Cricchetto!\r\nCorri su 29 piste diverse a Radiator Springs, a Londra e in Italia, con scorciatoie nascoste!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1732/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/1732/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/1732/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1732/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1732/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1732/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1732/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1732/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 1732,
        "html5_shortname": "",
        "version_id": 33196506,
        "package_names": [
          "com.gameloft.android.GAND.GloftCRSM",
          "com.gameloft.android.GloftCRSM",
          "com.gameloft.android.GAND.GloftCRPH",
          "com.gameloft.android.ALCH.GloftCRSM"
        ]
      },
      {
        "name": "Diamond Twister 2",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "",
        "size": "N/A",
        "rating": "2.67",
        "descriptions": {
          "short_description":
              "Torna a divertirti scambiando le gemme in 10 fantastici modi, con potenziamenti e una storia da star.\r\n",
          "long_description":
              "Giocabilità semplice e appassionante sia in modalità Storia o Storia infinita\r\n120 livelli ambientati in 8 luoghi da star\r\nIncontra nuovi personaggi lungo la strada mentre avanzi nella storia\r\n10 nuove meccaniche di gioco per scambiare le gemme e sbarazzarti della nebbia\r\nNuovi potenziamenti quali Spirale e Stasi utili per ottenere sempre più punti\r\nEffetti speciali grandiosi come esplosioni, tornado e altro ancora"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1215/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/1215/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/1215/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1215/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1215/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1215/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1215/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1215/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 1215,
        "html5_shortname": "",
        "version_id": 33196508,
        "package_names": [
          "com.gameloft.android.EU.GloftDIT2.ML",
          "com.gameloft.android.GAND.GloftGMHP",
          "com.gameloft.android.GAND.GloftDIT2",
          "com.gameloft.android.LATAM.GloftDIT2",
          "com.gameloft.android.GloftDIT2",
          "com.gameloft.android.SAMSUNG.GloftDIT2",
          "com.gameloft.android.LATAM.GloftX330"
        ]
      },
      {
        "name": "Disney Magic Kingdoms",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "rating": "4.53",
        "descriptions": {
          "short_description":
              "Crea il parco Disney dei tuoi sogni e condividi momenti unici con gli amatissimi personaggi Disney!",
          "long_description":
              "Attrazioni ispirate ai parchi a tema Disney straordinarie e senza tempo\r\nPersonaggi tratti da più di 90 anni di Disney\r\nUn'avvincente storia di eroi e cattivi\r\nRivivi i momenti più magici dei parchi con allegre e spettacolari parate\r\nCentinaia di missioni animate e divertenti, che danno vita al tuo regno!\r\nRaccogli oggetti a tema Disney e riporta nel regno i personaggi spariti"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2429/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/2429/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/2429/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2429/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2429/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2429/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2429/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2429/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 2429,
        "html5_shortname": "",
        "version_id": 33196518,
        "package_names": [
          "com.gameloft.android.GAND.GloftDMKP",
          "com.gameloft.android.ALCH.GloftDMKP"
        ]
      },
      {
        "name": "GT Racing 2 - Un'esperienza di guida totale",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "rating": "3.5",
        "descriptions": {
          "short_description":
              "Il famoso simulatore di corse torna con più auto e piste, e con una grafica migliorata.\r\n",
          "long_description":
              "Il seguito del più completo simulatore di corse su cellulare\r\nScegli tra 25 auto famose dell'universo di GT, tra cui Ferrari e Ford\r\n9 piste avvincenti in ambienti diversi: città, deserti, neve, eccetera \r\nUn'entusiasmante modalità Carriera ricca di eventi\r\nProva tante modalità di gioco, dalla gara classica a quella a eliminazione\r\nUn'esperienza di guida realistica, grazie al nuovo motore grafico\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1679/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/1679/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/1679/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1679/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1679/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1679/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1679/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1679/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 1679,
        "html5_shortname": "",
        "version_id": 33196520,
        "package_names": [
          "com.gameloft.android.GAND.GloftGT2M",
          "com.gameloft.android.BFLL.GloftGT2M",
          "com.gameloft.android.GloftGT2M"
        ]
      },
      {
        "name": "Gameloft Classics: Arcade",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "",
        "size": "N/A",
        "rating": "4.75",
        "descriptions": {
          "short_description":
              "Divertiti con questi 5 titoli classici Gameloft di successo, per la prima volta sugli smartphone!",
          "long_description":
              "Tutto il divertimento dei giochi originali, ora su smartphone!\r\nBLOCK BREAKER DELUXE 2 - Rompi-mattoncini con battaglie boss e altro ancora\r\nBUBBLE BASH 2 - Un coloratissimo scoppiabolle tropicale\r\nDIAMOND RUSH - Trova tesori nascosti tra rovine e templi antichi\r\nBRAIN CHALLENGE 3: IL NUOVO ALLENA-MENTE! - Allena la tua mente divertendoti\r\nMIAMI NIGHTS 2: THE CITY IS YOURS! - Trova fama, fortuna e amore"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3389/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/3389/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/3389/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3389/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3389/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3389/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3389/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3389/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 3389,
        "html5_shortname": "",
        "version_id": 33196512,
        "package_names": ["com.gameloft.android.GAND.GloftGLPH"]
      },
      {
        "name": "L'era glaciale - Le scrat-venture",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "rating": "3",
        "descriptions": {
          "short_description":
              "Gioca con Scrat in un platform in 2D!\r\nCorri, salta e combatti per trovare la Ghianda grandiosa!",
          "long_description":
              "Viaggia in tre ambienti diversi: il valico ghiacciato, la giungla dei dinosauri e Scratlantide.\r\nGuida Scrat in quest'epica avventura correndo, saltando, arrampicandoti e altro ancora!\r\nIntraprendi divertenti battaglie contro i boss, tra cui un cucciolo di avvoltoio e un dinosauro.\r\nIncontra le creature tipiche de L'ERA GLACIALE, come gli iraci, i ratti e i feroci piranha!\r\nScopri ambienti segreti e raccogli fino all'ultima ghianda!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2026/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/2026/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/2026/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2026/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2026/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2026/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2026/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2026/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 2026,
        "html5_shortname": "",
        "version_id": 33196526,
        "package_names": [
          "com.gameloft.android.GAND.GloftSCRT",
          "com.gameloft.android.ALCH.GloftSCRT",
          "com.gameloft.android.GloftSCRT"
        ]
      },
      {
        "name": "Iron Man 3 Mobile Premium",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "",
        "size": "N/A",
        "rating": "3",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1697/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/1697/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/1697/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1697/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1697/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1697/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1697/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1697/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 1697,
        "html5_shortname": "",
        "version_id": 33196530,
        "package_names": [
          "com.gameloft.android.GAND.GloftIRO3",
          "com.gameloft.android.GloftIRO3",
          "com.gameloft.android.LATAM.GloftIRO3",
          "com.gameloft.android.LATAM.BS46",
          "com.gameloft.android.LATAM.GloftBS19",
          "com.gameloft.android.LATAM.BU05",
          "com.gameloft.android.LATAM.BE02",
          "com.gameloft.android.ALCH.GloftIRO3"
        ]
      },
      {
        "name": "Little Big City 2",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "9+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Trasforma la tua isola tropicale in un'effervescente e bellissima metropoli! Collaborerai con il sindaco e i suoi bizzarri assistenti per costruire la città dei tuoi sogni: una potenza tecnologica e industriale, ma anche un'oasi ecologica!",
          "long_description":
              "Espandi i confini e incoraggia nuovi cittadini a stabilirsi nella tua città!\r\nProduci risorse uniche per ottenere fondi per la tua città!\r\nScopri i tesori nascosti dell'isola e usali a tuo vantaggio!\r\nStudia la strategia migliore per sviluppare la tua città!\r\nRisolvi tanti problemi diversi affinché tutto vada per il meglio in città!\r\nScegli se puntare sullo sviluppo culturale, industriale o tecnologico!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2721/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/2721/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/2721/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2721/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2721/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2721/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2721/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2721/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 2721,
        "html5_shortname": "",
        "version_id": 33196536,
        "package_names": [
          "com.gameloft.android.GAND.GloftLB2P",
          "com.gameloft.android.ALCH.GloftLB2P"
        ]
      },
      {
        "name": "Midnight Pool",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "",
        "size": "N/A",
        "rating": "4.62",
        "descriptions": {
          "short_description":
              "Il gioco di biliardo più realistico, con colpi speciali, una modalità Carriera e partite online!",
          "long_description":
              "Il gioco di biliardo più realistico!\r\nDivertiti e mettiti alla prova col più realistico gioco di biliardo di sempre.\r\nAffronta la nuova modalità Carriera con 100 missioni e 7 boss da battere.\r\nSfida gli amici online e dimostra al mondo intero chi è il migliore.\r\nVisita tanti locali e sfida i giocatori migliori in un'atmosfera anni '80 unica.\r\nColleziona moltissime stecche, ognuna con caratteristiche e stile unici."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2305/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/2305/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/2305/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2305/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2305/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2305/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2305/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2305/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 2305,
        "html5_shortname": "",
        "version_id": 33196540,
        "package_names": [
          "com.gameloft.android.GAND.GloftMPMP",
          "com.gameloft.android.ALCH.GloftMPMP"
        ]
      },
      {
        "name": "Modern Combat 4: Zero Hour",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "",
        "size": "N/A",
        "rating": "2.84",
        "descriptions": {
          "short_description":
              "Salva i leader mondiali da un gruppo di terroristi!\r\n",
          "long_description":
              "Una storia avvincente scritta da sceneggiatori professionisti\r\nDai la caccia al nemico in tutto il mondo, dal Sudafrica a Barcellona\r\nGrande varietà di gioco: eventi rapidi, uccisioni furtive, droni e molto altro!\r\n2 nuove modalità: Sopravvivenza e Infiltrazione\r\nUn mix di sequenze a scorrimento laterale, alla guida di veicoli e da cecchino\r\nScopri 9 livelli esplosivi\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1660/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/1660/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/1660/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1660/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1660/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1660/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1660/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1660/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 1660,
        "html5_shortname": "",
        "version_id": 33196544,
        "package_names": [
          "com.gameloft.android.GAND.GloftMC4M",
          "com.gameloft.android.LATAM.BK25",
          "com.gameloft.android.GloftMC4M",
          "com.gameloft.android.LATAM.BS47",
          "com.gameloft.android.GAND.GloftSCRT",
          "com.gameloft.android.ALCH.GloftMC4M",
          "com.gameloft.android.HEP.GloftMZHP"
        ]
      },
      {
        "name": "Motocross : Trial Extreme Mobile Premium",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "",
        "size": "N/A",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1268/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/1268/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/1268/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1268/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1268/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1268/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1268/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1268/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 1268,
        "html5_shortname": "",
        "version_id": 33196548,
        "package_names": [
          "com.gameloft.android.GAND.GloftMOTR",
          "com.gameloft.android.BFLL.GloftMOTR",
          "com.gameloft.android.GloftMOTR"
        ]
      },
      {
        "name": "N.O.V.A. Legacy",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "",
        "size": "N/A",
        "rating": "4.77",
        "descriptions": {
          "short_description":
              "N.O.V.A. Legacy è un FPS fantascientifico in una versione compatta da 20 MB.",
          "long_description":
              "Una qualità da console su cellulare.\r\nSconfiggi le forze aliene in varie modalità di gioco.\r\nMettiti alla prova nelle arene multigiocatore.\r\nMigliora le armi, come potenti fucili d'assalto e devastanti fucili al plasma.\r\nIl divertimento del N.O.V.A. originale, con un gameplay e una grafica migliori.\r\nTutto in una versione compatta da 20 MB."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2331/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/2331/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/2331/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2331/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2331/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2331/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2331/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2331/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 2331,
        "html5_shortname": "",
        "version_id": 33196546,
        "package_names": [
          "com.gameloft.android.GAND.GloftNOMP",
          "com.gameloft.android.GloftNOMP",
          "com.gameloft.android.ALCH.GloftNOMP"
        ]
      },
      {
        "name": "Rival Wheels: Moto Drag Racing",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "rating": "4.34",
        "descriptions": {
          "short_description":
              "Preparati per il gioco di corse in moto su rettilineo più competitivo e avvincente su cellulare!",
          "long_description":
              "Più di 15 moto su licenza dei costruttori più famosi.\r\nComandi semplici: tocca per cambiare marcia, ma fallo al momento giusto!\r\nDimensioni ridotte per permettere a tutti di scaricare il divertimento!\r\nUna qualità grafica estrema: ogni moto sembra un vero bolide da corsa!\r\nGareggia a Tokyo, L'Avana e New York."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3497/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/3497/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/3497/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3497/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3497/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3497/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3497/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3497/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 3497,
        "html5_shortname": "",
        "version_id": 33196538,
        "package_names": ["com.gameloft.android.GAND.GloftRWMP"]
      },
      {
        "name": "Spider-Man: Ultimate Power ",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "12+",
        "size": "N/A",
        "rating": "4.38",
        "descriptions": {
          "short_description":
              "Il Goblin, L'Uomo Sabbia e Venom hanno unito le forze e rapito Mary Jane! Combatti per i tetti e le strade di Manhattan dondolandoti con la ragnatela per salvare la tua amata... e New York!",
          "long_description":
              "Un gioco dell'Uomo Ragno tra picchiaduro e arcade! Combatti a Manhattan!\r\n\"\r\nDai la caccia ai malfattori, sconfiggili e manda in fumo i loro piani malvagi!\"\r\n\" \r\nCombatti e dondolati in modo frenetico e divertente con due tocchi delle dita!\"\r\nSblocca versioni leggendarie dell'Uomo Ragno, come Miles Morales e Iron Spider!\r\nPotenziamenti basati sulle abilità dell'Uomo Ragno, come gli scudi di ragnatela!\r\n\"\r\nUsa il costume nero dell'Uomo Ragno per danni micidiali e record incredibili!\"\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1763/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/1763/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/1763/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1763/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1763/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1763/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1763/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1763/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 1763,
        "html5_shortname": "",
        "version_id": 33196532,
        "package_names": [
          "com.gameloft.android.GAND.GloftSMIM",
          "com.gameloft.android.GloftSMIF",
          "com.gameloft.android.GloftSMIM",
          "com.gameloft.android.ALCH.GloftSMIM"
        ]
      },
      {
        "name": "The Avengers - Il gioco per cellulari",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "12+",
        "size": "N/A",
        "rating": "2.5",
        "descriptions": {
          "short_description":
              "I Vendicatori uniti per fermare l'invasione aliena di Loki!",
          "long_description":
              "Gioca con 4 Vendicatori (Hulk, Thor, Capitan America e Iron Man)\r\nOgni Vendicatore ha i propri attacchi e poteri speciali\r\nCombatti contro un esercito di nemici e boss\r\nGiocabilità varia con livelli in volo, combo, attacchi a distanza e potenziamenti da sbloccare\r\nLuoghi e ambientazioni ricreati fedelmente dal film"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1504/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/1504/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/1504/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1504/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1504/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1504/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1504/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1504/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 1504,
        "html5_shortname": "",
        "version_id": 32570315,
        "package_names": [
          "com.gameloft.android.GAND.GloftAVEN",
          "com.gameloft.android.GloftAVEN",
          "com.gameloft.android.LATAM.GloftAVEN",
          "com.gameloft.android.LATAM.BB62",
          "com.gameloft.android.LATAM.BF87",
          "com.gameloft.android.LATAM.BS45",
          "com.gameloft.android.SFCL.GloftAVEN",
          "com.gameloft.android.ALCH.GloftAVEN"
        ]
      }
    ]
  },
  {
    "name": "Bring some action",
    "type": "manual",
    "products": [
      {
        "name": "Iron Man 3 Mobile Premium",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "",
        "size": "N/A",
        "rating": "3",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1697/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/1697/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/1697/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1697/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1697/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1697/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1697/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1697/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 1697,
        "html5_shortname": "",
        "version_id": 33196530,
        "package_names": [
          "com.gameloft.android.GAND.GloftIRO3",
          "com.gameloft.android.GloftIRO3",
          "com.gameloft.android.LATAM.GloftIRO3",
          "com.gameloft.android.LATAM.BS46",
          "com.gameloft.android.LATAM.GloftBS19",
          "com.gameloft.android.LATAM.BU05",
          "com.gameloft.android.LATAM.BE02",
          "com.gameloft.android.ALCH.GloftIRO3"
        ]
      },
      {
        "name": "Dungeon Hunter Curse of Heaven",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Tuffati nell'RPG d'azione fantasy più intenso e magico!",
          "long_description":
              "Batti orde di mostri magici, guerrieri con spade, ombre magiche e tanto altro.\r\n24 missioni per scoprire di più sui tuoi poteri e sulla morte dei tuoi cari.\r\nAttraversa portali magici verso il mondo delle ombre, pieno di sfide e premi.\r\nCrea un esercito, potenzia la fortezza e batti i nemici in modalità Conquista.\r\nAffronta i dungeon giornalieri per ricevere nuove sfide e premi ogni giorno.\r\nOttieni più di 300 spade, aste, balestre e bastoni unici e usane 2 alla volta!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2952/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/2952/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/2952/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2952/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2952/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2952/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2952/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2952/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 2952,
        "html5_shortname": "",
        "version_id": 33196514,
        "package_names": [
          "com.gameloft.android.GAND.GloftDUMP",
          "com.gameloft.android.GAND.GloftDG73"
        ]
      },
      {
        "name": "Modern Combat 4: Zero Hour",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "",
        "size": "N/A",
        "rating": "2.84",
        "descriptions": {
          "short_description":
              "Salva i leader mondiali da un gruppo di terroristi!\r\n",
          "long_description":
              "Una storia avvincente scritta da sceneggiatori professionisti\r\nDai la caccia al nemico in tutto il mondo, dal Sudafrica a Barcellona\r\nGrande varietà di gioco: eventi rapidi, uccisioni furtive, droni e molto altro!\r\n2 nuove modalità: Sopravvivenza e Infiltrazione\r\nUn mix di sequenze a scorrimento laterale, alla guida di veicoli e da cecchino\r\nScopri 9 livelli esplosivi\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1660/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/1660/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/1660/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1660/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1660/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1660/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1660/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1660/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 1660,
        "html5_shortname": "",
        "version_id": 33196544,
        "package_names": [
          "com.gameloft.android.GAND.GloftMC4M",
          "com.gameloft.android.LATAM.BK25",
          "com.gameloft.android.GloftMC4M",
          "com.gameloft.android.LATAM.BS47",
          "com.gameloft.android.GAND.GloftSCRT",
          "com.gameloft.android.ALCH.GloftMC4M",
          "com.gameloft.android.HEP.GloftMZHP"
        ]
      },
      {
        "name": "N.O.V.A. Legacy",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "",
        "size": "N/A",
        "rating": "4.77",
        "descriptions": {
          "short_description":
              "N.O.V.A. Legacy è un FPS fantascientifico in una versione compatta da 20 MB.",
          "long_description":
              "Una qualità da console su cellulare.\r\nSconfiggi le forze aliene in varie modalità di gioco.\r\nMettiti alla prova nelle arene multigiocatore.\r\nMigliora le armi, come potenti fucili d'assalto e devastanti fucili al plasma.\r\nIl divertimento del N.O.V.A. originale, con un gameplay e una grafica migliori.\r\nTutto in una versione compatta da 20 MB."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2331/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/2331/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/2331/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2331/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2331/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2331/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2331/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2331/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 2331,
        "html5_shortname": "",
        "version_id": 33196546,
        "package_names": [
          "com.gameloft.android.GAND.GloftNOMP",
          "com.gameloft.android.GloftNOMP",
          "com.gameloft.android.ALCH.GloftNOMP"
        ]
      },
      {
        "name": "Spider-Man: Ultimate Power ",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "12+",
        "size": "N/A",
        "rating": "4.38",
        "descriptions": {
          "short_description":
              "Il Goblin, L'Uomo Sabbia e Venom hanno unito le forze e rapito Mary Jane! Combatti per i tetti e le strade di Manhattan dondolandoti con la ragnatela per salvare la tua amata... e New York!",
          "long_description":
              "Un gioco dell'Uomo Ragno tra picchiaduro e arcade! Combatti a Manhattan!\r\n\"\r\nDai la caccia ai malfattori, sconfiggili e manda in fumo i loro piani malvagi!\"\r\n\" \r\nCombatti e dondolati in modo frenetico e divertente con due tocchi delle dita!\"\r\nSblocca versioni leggendarie dell'Uomo Ragno, come Miles Morales e Iron Spider!\r\nPotenziamenti basati sulle abilità dell'Uomo Ragno, come gli scudi di ragnatela!\r\n\"\r\nUsa il costume nero dell'Uomo Ragno per danni micidiali e record incredibili!\"\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1763/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/1763/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/1763/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1763/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1763/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1763/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1763/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1763/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 1763,
        "html5_shortname": "",
        "version_id": 33196532,
        "package_names": [
          "com.gameloft.android.GAND.GloftSMIM",
          "com.gameloft.android.GloftSMIF",
          "com.gameloft.android.GloftSMIM",
          "com.gameloft.android.ALCH.GloftSMIM"
        ]
      },
      {
        "name": "The Avengers - Il gioco per cellulari",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "12+",
        "size": "N/A",
        "rating": "2.5",
        "descriptions": {
          "short_description":
              "I Vendicatori uniti per fermare l'invasione aliena di Loki!",
          "long_description":
              "Gioca con 4 Vendicatori (Hulk, Thor, Capitan America e Iron Man)\r\nOgni Vendicatore ha i propri attacchi e poteri speciali\r\nCombatti contro un esercito di nemici e boss\r\nGiocabilità varia con livelli in volo, combo, attacchi a distanza e potenziamenti da sbloccare\r\nLuoghi e ambientazioni ricreati fedelmente dal film"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1504/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/1504/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/1504/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1504/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1504/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1504/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1504/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1504/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 1504,
        "html5_shortname": "",
        "version_id": 32570315,
        "package_names": [
          "com.gameloft.android.GAND.GloftAVEN",
          "com.gameloft.android.GloftAVEN",
          "com.gameloft.android.LATAM.GloftAVEN",
          "com.gameloft.android.LATAM.BB62",
          "com.gameloft.android.LATAM.BF87",
          "com.gameloft.android.LATAM.BS45",
          "com.gameloft.android.SFCL.GloftAVEN",
          "com.gameloft.android.ALCH.GloftAVEN"
        ]
      }
    ]
  },
  {
    "name": "Editor's picks",
    "type": "manual",
    "products": [
      {
        "name": "Diamond Twister 2",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "",
        "size": "N/A",
        "rating": "2.67",
        "descriptions": {
          "short_description":
              "Torna a divertirti scambiando le gemme in 10 fantastici modi, con potenziamenti e una storia da star.\r\n",
          "long_description":
              "Giocabilità semplice e appassionante sia in modalità Storia o Storia infinita\r\n120 livelli ambientati in 8 luoghi da star\r\nIncontra nuovi personaggi lungo la strada mentre avanzi nella storia\r\n10 nuove meccaniche di gioco per scambiare le gemme e sbarazzarti della nebbia\r\nNuovi potenziamenti quali Spirale e Stasi utili per ottenere sempre più punti\r\nEffetti speciali grandiosi come esplosioni, tornado e altro ancora"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1215/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/1215/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/1215/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1215/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1215/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1215/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1215/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1215/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 1215,
        "html5_shortname": "",
        "version_id": 33196508,
        "package_names": [
          "com.gameloft.android.EU.GloftDIT2.ML",
          "com.gameloft.android.GAND.GloftGMHP",
          "com.gameloft.android.GAND.GloftDIT2",
          "com.gameloft.android.LATAM.GloftDIT2",
          "com.gameloft.android.GloftDIT2",
          "com.gameloft.android.SAMSUNG.GloftDIT2",
          "com.gameloft.android.LATAM.GloftX330"
        ]
      },
      {
        "name": "Disney Magic Kingdoms",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "rating": "4.53",
        "descriptions": {
          "short_description":
              "Crea il parco Disney dei tuoi sogni e condividi momenti unici con gli amatissimi personaggi Disney!",
          "long_description":
              "Attrazioni ispirate ai parchi a tema Disney straordinarie e senza tempo\r\nPersonaggi tratti da più di 90 anni di Disney\r\nUn'avvincente storia di eroi e cattivi\r\nRivivi i momenti più magici dei parchi con allegre e spettacolari parate\r\nCentinaia di missioni animate e divertenti, che danno vita al tuo regno!\r\nRaccogli oggetti a tema Disney e riporta nel regno i personaggi spariti"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2429/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/2429/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/2429/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2429/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2429/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2429/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2429/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2429/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 2429,
        "html5_shortname": "",
        "version_id": 33196518,
        "package_names": [
          "com.gameloft.android.GAND.GloftDMKP",
          "com.gameloft.android.ALCH.GloftDMKP"
        ]
      },
      {
        "name": "Dungeon Hunter Curse of Heaven",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Tuffati nell'RPG d'azione fantasy più intenso e magico!",
          "long_description":
              "Batti orde di mostri magici, guerrieri con spade, ombre magiche e tanto altro.\r\n24 missioni per scoprire di più sui tuoi poteri e sulla morte dei tuoi cari.\r\nAttraversa portali magici verso il mondo delle ombre, pieno di sfide e premi.\r\nCrea un esercito, potenzia la fortezza e batti i nemici in modalità Conquista.\r\nAffronta i dungeon giornalieri per ricevere nuove sfide e premi ogni giorno.\r\nOttieni più di 300 spade, aste, balestre e bastoni unici e usane 2 alla volta!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2952/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/2952/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/2952/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2952/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2952/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2952/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2952/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2952/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 2952,
        "html5_shortname": "",
        "version_id": 33196514,
        "package_names": [
          "com.gameloft.android.GAND.GloftDUMP",
          "com.gameloft.android.GAND.GloftDG73"
        ]
      },
      {
        "name": "GT Racing 2 - Un'esperienza di guida totale",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "rating": "3.5",
        "descriptions": {
          "short_description":
              "Il famoso simulatore di corse torna con più auto e piste, e con una grafica migliorata.\r\n",
          "long_description":
              "Il seguito del più completo simulatore di corse su cellulare\r\nScegli tra 25 auto famose dell'universo di GT, tra cui Ferrari e Ford\r\n9 piste avvincenti in ambienti diversi: città, deserti, neve, eccetera \r\nUn'entusiasmante modalità Carriera ricca di eventi\r\nProva tante modalità di gioco, dalla gara classica a quella a eliminazione\r\nUn'esperienza di guida realistica, grazie al nuovo motore grafico\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1679/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/1679/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/1679/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1679/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1679/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1679/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1679/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1679/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 1679,
        "html5_shortname": "",
        "version_id": 33196520,
        "package_names": [
          "com.gameloft.android.GAND.GloftGT2M",
          "com.gameloft.android.BFLL.GloftGT2M",
          "com.gameloft.android.GloftGT2M"
        ]
      },
      {
        "name": "Gameloft Classics: Arcade",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "",
        "size": "N/A",
        "rating": "4.75",
        "descriptions": {
          "short_description":
              "Divertiti con questi 5 titoli classici Gameloft di successo, per la prima volta sugli smartphone!",
          "long_description":
              "Tutto il divertimento dei giochi originali, ora su smartphone!\r\nBLOCK BREAKER DELUXE 2 - Rompi-mattoncini con battaglie boss e altro ancora\r\nBUBBLE BASH 2 - Un coloratissimo scoppiabolle tropicale\r\nDIAMOND RUSH - Trova tesori nascosti tra rovine e templi antichi\r\nBRAIN CHALLENGE 3: IL NUOVO ALLENA-MENTE! - Allena la tua mente divertendoti\r\nMIAMI NIGHTS 2: THE CITY IS YOURS! - Trova fama, fortuna e amore"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3389/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/3389/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/3389/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3389/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3389/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3389/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3389/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3389/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 3389,
        "html5_shortname": "",
        "version_id": 33196512,
        "package_names": ["com.gameloft.android.GAND.GloftGLPH"]
      },
      {
        "name": "L'era glaciale - Le scrat-venture",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "rating": "3",
        "descriptions": {
          "short_description":
              "Gioca con Scrat in un platform in 2D!\r\nCorri, salta e combatti per trovare la Ghianda grandiosa!",
          "long_description":
              "Viaggia in tre ambienti diversi: il valico ghiacciato, la giungla dei dinosauri e Scratlantide.\r\nGuida Scrat in quest'epica avventura correndo, saltando, arrampicandoti e altro ancora!\r\nIntraprendi divertenti battaglie contro i boss, tra cui un cucciolo di avvoltoio e un dinosauro.\r\nIncontra le creature tipiche de L'ERA GLACIALE, come gli iraci, i ratti e i feroci piranha!\r\nScopri ambienti segreti e raccogli fino all'ultima ghianda!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2026/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/2026/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/2026/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2026/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2026/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2026/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2026/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2026/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 2026,
        "html5_shortname": "",
        "version_id": 33196526,
        "package_names": [
          "com.gameloft.android.GAND.GloftSCRT",
          "com.gameloft.android.ALCH.GloftSCRT",
          "com.gameloft.android.GloftSCRT"
        ]
      },
      {
        "name": "Motocross : Trial Extreme Mobile Premium",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "",
        "size": "N/A",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1268/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/1268/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/1268/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1268/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1268/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1268/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1268/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1268/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 1268,
        "html5_shortname": "",
        "version_id": 33196548,
        "package_names": [
          "com.gameloft.android.GAND.GloftMOTR",
          "com.gameloft.android.BFLL.GloftMOTR",
          "com.gameloft.android.GloftMOTR"
        ]
      },
      {
        "name": "Sonic Runners Adventure",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "3+",
        "size": "N/A",
        "rating": "4.68",
        "descriptions": {
          "short_description":
              "Sonic e i suoi amici sono tornati in un nuovissimo gioco runner! Rivivi le sue avventure supersoniche saltando, scattando e volando in strepitosi livelli per combattere Eggman in 4 aree leggendarie.",
          "long_description":
              "Salva l'universo di Sonic in un nuovo gioco runner senza fine!\r\nSblocca e potenzia tantissimi personaggi di Sonic come Tails e Knuckles!\r\n4 aree leggendarie e una modalità bonus con una nuova grafica spettacolare!\r\nBrani famosi dei giochi di Sonic precedenti per corse divertenti e nostalgiche!\r\nUna storia intensa e tanti obiettivi per divertirsi all'infinito!\r\nSistema di gioco basato sulle combo e una modalità di corsa automatica!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2729/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/2729/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/2729/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2729/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2729/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2729/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2729/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2729/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 2729,
        "html5_shortname": "",
        "version_id": 33196534,
        "package_names": [
          "com.gameloft.android.GAND.GloftSOMP",
          "com.gameloft.android.GAND.GloftSONQ",
          "com.gameloft.android.ALCH.GloftSOMP",
          "com.gameloft.android.GloftSOMP"
        ]
      }
    ]
  },
  {
    "name": "Games under 25Mb",
    "type": "manual",
    "products": [
      {
        "name": "Diamond Twister 2",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "",
        "size": "N/A",
        "rating": "2.67",
        "descriptions": {
          "short_description":
              "Torna a divertirti scambiando le gemme in 10 fantastici modi, con potenziamenti e una storia da star.\r\n",
          "long_description":
              "Giocabilità semplice e appassionante sia in modalità Storia o Storia infinita\r\n120 livelli ambientati in 8 luoghi da star\r\nIncontra nuovi personaggi lungo la strada mentre avanzi nella storia\r\n10 nuove meccaniche di gioco per scambiare le gemme e sbarazzarti della nebbia\r\nNuovi potenziamenti quali Spirale e Stasi utili per ottenere sempre più punti\r\nEffetti speciali grandiosi come esplosioni, tornado e altro ancora"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1215/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/1215/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/1215/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1215/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1215/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1215/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1215/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1215/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 1215,
        "html5_shortname": "",
        "version_id": 33196508,
        "package_names": [
          "com.gameloft.android.EU.GloftDIT2.ML",
          "com.gameloft.android.GAND.GloftGMHP",
          "com.gameloft.android.GAND.GloftDIT2",
          "com.gameloft.android.LATAM.GloftDIT2",
          "com.gameloft.android.GloftDIT2",
          "com.gameloft.android.SAMSUNG.GloftDIT2",
          "com.gameloft.android.LATAM.GloftX330"
        ]
      },
      {
        "name": "Gameloft Classics: Arcade",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "",
        "size": "N/A",
        "rating": "4.75",
        "descriptions": {
          "short_description":
              "Divertiti con questi 5 titoli classici Gameloft di successo, per la prima volta sugli smartphone!",
          "long_description":
              "Tutto il divertimento dei giochi originali, ora su smartphone!\r\nBLOCK BREAKER DELUXE 2 - Rompi-mattoncini con battaglie boss e altro ancora\r\nBUBBLE BASH 2 - Un coloratissimo scoppiabolle tropicale\r\nDIAMOND RUSH - Trova tesori nascosti tra rovine e templi antichi\r\nBRAIN CHALLENGE 3: IL NUOVO ALLENA-MENTE! - Allena la tua mente divertendoti\r\nMIAMI NIGHTS 2: THE CITY IS YOURS! - Trova fama, fortuna e amore"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3389/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/3389/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/3389/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3389/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3389/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3389/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3389/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3389/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 3389,
        "html5_shortname": "",
        "version_id": 33196512,
        "package_names": ["com.gameloft.android.GAND.GloftGLPH"]
      },
      {
        "name": "Little Big City 2",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "9+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Trasforma la tua isola tropicale in un'effervescente e bellissima metropoli! Collaborerai con il sindaco e i suoi bizzarri assistenti per costruire la città dei tuoi sogni: una potenza tecnologica e industriale, ma anche un'oasi ecologica!",
          "long_description":
              "Espandi i confini e incoraggia nuovi cittadini a stabilirsi nella tua città!\r\nProduci risorse uniche per ottenere fondi per la tua città!\r\nScopri i tesori nascosti dell'isola e usali a tuo vantaggio!\r\nStudia la strategia migliore per sviluppare la tua città!\r\nRisolvi tanti problemi diversi affinché tutto vada per il meglio in città!\r\nScegli se puntare sullo sviluppo culturale, industriale o tecnologico!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2721/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/2721/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/2721/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2721/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2721/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2721/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2721/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2721/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 2721,
        "html5_shortname": "",
        "version_id": 33196536,
        "package_names": [
          "com.gameloft.android.GAND.GloftLB2P",
          "com.gameloft.android.ALCH.GloftLB2P"
        ]
      },
      {
        "name": "Midnight Pool",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "",
        "size": "N/A",
        "rating": "4.62",
        "descriptions": {
          "short_description":
              "Il gioco di biliardo più realistico, con colpi speciali, una modalità Carriera e partite online!",
          "long_description":
              "Il gioco di biliardo più realistico!\r\nDivertiti e mettiti alla prova col più realistico gioco di biliardo di sempre.\r\nAffronta la nuova modalità Carriera con 100 missioni e 7 boss da battere.\r\nSfida gli amici online e dimostra al mondo intero chi è il migliore.\r\nVisita tanti locali e sfida i giocatori migliori in un'atmosfera anni '80 unica.\r\nColleziona moltissime stecche, ognuna con caratteristiche e stile unici."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2305/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/2305/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/2305/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2305/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2305/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2305/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2305/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2305/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 2305,
        "html5_shortname": "",
        "version_id": 33196540,
        "package_names": [
          "com.gameloft.android.GAND.GloftMPMP",
          "com.gameloft.android.ALCH.GloftMPMP"
        ]
      },
      {
        "name": "Modern Combat 4: Zero Hour",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "",
        "size": "N/A",
        "rating": "2.84",
        "descriptions": {
          "short_description":
              "Salva i leader mondiali da un gruppo di terroristi!\r\n",
          "long_description":
              "Una storia avvincente scritta da sceneggiatori professionisti\r\nDai la caccia al nemico in tutto il mondo, dal Sudafrica a Barcellona\r\nGrande varietà di gioco: eventi rapidi, uccisioni furtive, droni e molto altro!\r\n2 nuove modalità: Sopravvivenza e Infiltrazione\r\nUn mix di sequenze a scorrimento laterale, alla guida di veicoli e da cecchino\r\nScopri 9 livelli esplosivi\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1660/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/1660/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/1660/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1660/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1660/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1660/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1660/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1660/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 1660,
        "html5_shortname": "",
        "version_id": 33196544,
        "package_names": [
          "com.gameloft.android.GAND.GloftMC4M",
          "com.gameloft.android.LATAM.BK25",
          "com.gameloft.android.GloftMC4M",
          "com.gameloft.android.LATAM.BS47",
          "com.gameloft.android.GAND.GloftSCRT",
          "com.gameloft.android.ALCH.GloftMC4M",
          "com.gameloft.android.HEP.GloftMZHP"
        ]
      },
      {
        "name": "Motocross : Trial Extreme Mobile Premium",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "",
        "size": "N/A",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1268/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/1268/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/1268/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1268/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1268/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1268/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1268/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1268/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 1268,
        "html5_shortname": "",
        "version_id": 33196548,
        "package_names": [
          "com.gameloft.android.GAND.GloftMOTR",
          "com.gameloft.android.BFLL.GloftMOTR",
          "com.gameloft.android.GloftMOTR"
        ]
      },
      {
        "name": "N.O.V.A. Legacy",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "",
        "size": "N/A",
        "rating": "4.77",
        "descriptions": {
          "short_description":
              "N.O.V.A. Legacy è un FPS fantascientifico in una versione compatta da 20 MB.",
          "long_description":
              "Una qualità da console su cellulare.\r\nSconfiggi le forze aliene in varie modalità di gioco.\r\nMettiti alla prova nelle arene multigiocatore.\r\nMigliora le armi, come potenti fucili d'assalto e devastanti fucili al plasma.\r\nIl divertimento del N.O.V.A. originale, con un gameplay e una grafica migliori.\r\nTutto in una versione compatta da 20 MB."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2331/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/2331/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/2331/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2331/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2331/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2331/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2331/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2331/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 2331,
        "html5_shortname": "",
        "version_id": 33196546,
        "package_names": [
          "com.gameloft.android.GAND.GloftNOMP",
          "com.gameloft.android.GloftNOMP",
          "com.gameloft.android.ALCH.GloftNOMP"
        ]
      },
      {
        "name": "Rival Wheels: Moto Drag Racing",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "rating": "4.34",
        "descriptions": {
          "short_description":
              "Preparati per il gioco di corse in moto su rettilineo più competitivo e avvincente su cellulare!",
          "long_description":
              "Più di 15 moto su licenza dei costruttori più famosi.\r\nComandi semplici: tocca per cambiare marcia, ma fallo al momento giusto!\r\nDimensioni ridotte per permettere a tutti di scaricare il divertimento!\r\nUna qualità grafica estrema: ogni moto sembra un vero bolide da corsa!\r\nGareggia a Tokyo, L'Avana e New York."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3497/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/3497/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/3497/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3497/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3497/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3497/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3497/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3497/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 3497,
        "html5_shortname": "",
        "version_id": 33196538,
        "package_names": ["com.gameloft.android.GAND.GloftRWMP"]
      }
    ]
  },
  {
    "name": "More games",
    "type": "manual",
    "products": [
      {
        "name": "Real Football 2017",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "12+",
        "size": "N/A",
        "rating": "2.25",
        "descriptions": {
          "short_description":
              "Novità incredibili per la stagione 2017, come le strutture del club e la modalità PvP.",
          "long_description":
              "Crea una squadra da sogno: ingaggia delle stelle e potenzia le loro abilità!\r\nGrafica stupefacente e curata nei dettagli, per ricreare un'atmosfera unica!\r\nIA migliorata: giocatori più intelligenti rendono l'esperienza più realistica!\r\nSfida gli amici oppure entra nella modalità PvP Arena mondiale!\r\nGuida la tua squadra in cima alle classifiche mondali!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2617/default/wap/packshots/200/1.png",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2617/1080/1.png",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/gdc/images/2617/1080/1.png",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2617/default/screenshot/0.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2617/default/screenshot/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2617/default/screenshot/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2617/default/screenshot/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2617/default/screenshot/4.jpg"
          ]
        },
        "product_id": 2617,
        "html5_shortname": "",
        "version_id": 33831194,
        "package_names": [
          "com.gameloft.android.GAND.GloftRF17",
          "com.gameloft.android.ALCH.GloftRF17"
        ]
      },
      {
        "name": "Disney Magic Kingdoms",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "4+",
        "size": "N/A",
        "rating": "4.53",
        "descriptions": {
          "short_description":
              "Crea il parco Disney dei tuoi sogni e condividi momenti unici con gli amatissimi personaggi Disney!",
          "long_description":
              "Attrazioni ispirate ai parchi a tema Disney straordinarie e senza tempo\r\nPersonaggi tratti da più di 90 anni di Disney\r\nUn'avvincente storia di eroi e cattivi\r\nRivivi i momenti più magici dei parchi con allegre e spettacolari parate\r\nCentinaia di missioni animate e divertenti, che danno vita al tuo regno!\r\nRaccogli oggetti a tema Disney e riporta nel regno i personaggi spariti"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2429/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/2429/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/2429/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2429/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2429/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2429/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2429/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2429/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 2429,
        "html5_shortname": "",
        "version_id": 33196518,
        "package_names": [
          "com.gameloft.android.GAND.GloftDMKP",
          "com.gameloft.android.ALCH.GloftDMKP"
        ]
      },
      {
        "name": "Dungeon Hunter Curse of Heaven",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Tuffati nell'RPG d'azione fantasy più intenso e magico!",
          "long_description":
              "Batti orde di mostri magici, guerrieri con spade, ombre magiche e tanto altro.\r\n24 missioni per scoprire di più sui tuoi poteri e sulla morte dei tuoi cari.\r\nAttraversa portali magici verso il mondo delle ombre, pieno di sfide e premi.\r\nCrea un esercito, potenzia la fortezza e batti i nemici in modalità Conquista.\r\nAffronta i dungeon giornalieri per ricevere nuove sfide e premi ogni giorno.\r\nOttieni più di 300 spade, aste, balestre e bastoni unici e usane 2 alla volta!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2952/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/2952/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/2952/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2952/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2952/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2952/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2952/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2952/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 2952,
        "html5_shortname": "",
        "version_id": 33196514,
        "package_names": [
          "com.gameloft.android.GAND.GloftDUMP",
          "com.gameloft.android.GAND.GloftDG73"
        ]
      },
      {
        "name": "GT Racing 2 - Un'esperienza di guida totale",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "rating": "3.5",
        "descriptions": {
          "short_description":
              "Il famoso simulatore di corse torna con più auto e piste, e con una grafica migliorata.\r\n",
          "long_description":
              "Il seguito del più completo simulatore di corse su cellulare\r\nScegli tra 25 auto famose dell'universo di GT, tra cui Ferrari e Ford\r\n9 piste avvincenti in ambienti diversi: città, deserti, neve, eccetera \r\nUn'entusiasmante modalità Carriera ricca di eventi\r\nProva tante modalità di gioco, dalla gara classica a quella a eliminazione\r\nUn'esperienza di guida realistica, grazie al nuovo motore grafico\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1679/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/1679/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/1679/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1679/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1679/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1679/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1679/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1679/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 1679,
        "html5_shortname": "",
        "version_id": 33196520,
        "package_names": [
          "com.gameloft.android.GAND.GloftGT2M",
          "com.gameloft.android.BFLL.GloftGT2M",
          "com.gameloft.android.GloftGT2M"
        ]
      },
      {
        "name": "Gameloft Classics: Arcade",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "",
        "size": "N/A",
        "rating": "4.75",
        "descriptions": {
          "short_description":
              "Divertiti con questi 5 titoli classici Gameloft di successo, per la prima volta sugli smartphone!",
          "long_description":
              "Tutto il divertimento dei giochi originali, ora su smartphone!\r\nBLOCK BREAKER DELUXE 2 - Rompi-mattoncini con battaglie boss e altro ancora\r\nBUBBLE BASH 2 - Un coloratissimo scoppiabolle tropicale\r\nDIAMOND RUSH - Trova tesori nascosti tra rovine e templi antichi\r\nBRAIN CHALLENGE 3: IL NUOVO ALLENA-MENTE! - Allena la tua mente divertendoti\r\nMIAMI NIGHTS 2: THE CITY IS YOURS! - Trova fama, fortuna e amore"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/3389/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/3389/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/3389/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/3389/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3389/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3389/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3389/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/3389/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 3389,
        "html5_shortname": "",
        "version_id": 33196512,
        "package_names": ["com.gameloft.android.GAND.GloftGLPH"]
      },
      {
        "name": "L'era glaciale - Le scrat-venture",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "ALL",
        "size": "N/A",
        "rating": "3",
        "descriptions": {
          "short_description":
              "Gioca con Scrat in un platform in 2D!\r\nCorri, salta e combatti per trovare la Ghianda grandiosa!",
          "long_description":
              "Viaggia in tre ambienti diversi: il valico ghiacciato, la giungla dei dinosauri e Scratlantide.\r\nGuida Scrat in quest'epica avventura correndo, saltando, arrampicandoti e altro ancora!\r\nIntraprendi divertenti battaglie contro i boss, tra cui un cucciolo di avvoltoio e un dinosauro.\r\nIncontra le creature tipiche de L'ERA GLACIALE, come gli iraci, i ratti e i feroci piranha!\r\nScopri ambienti segreti e raccogli fino all'ultima ghianda!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2026/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/2026/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/2026/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2026/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2026/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2026/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2026/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2026/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 2026,
        "html5_shortname": "",
        "version_id": 33196526,
        "package_names": [
          "com.gameloft.android.GAND.GloftSCRT",
          "com.gameloft.android.ALCH.GloftSCRT",
          "com.gameloft.android.GloftSCRT"
        ]
      },
      {
        "name": "Little Big City 2",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "9+",
        "size": "N/A",
        "descriptions": {
          "short_description":
              "Trasforma la tua isola tropicale in un'effervescente e bellissima metropoli! Collaborerai con il sindaco e i suoi bizzarri assistenti per costruire la città dei tuoi sogni: una potenza tecnologica e industriale, ma anche un'oasi ecologica!",
          "long_description":
              "Espandi i confini e incoraggia nuovi cittadini a stabilirsi nella tua città!\r\nProduci risorse uniche per ottenere fondi per la tua città!\r\nScopri i tesori nascosti dell'isola e usali a tuo vantaggio!\r\nStudia la strategia migliore per sviluppare la tua città!\r\nRisolvi tanti problemi diversi affinché tutto vada per il meglio in città!\r\nScegli se puntare sullo sviluppo culturale, industriale o tecnologico!"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2721/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/2721/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/2721/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2721/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2721/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2721/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2721/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2721/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 2721,
        "html5_shortname": "",
        "version_id": 33196536,
        "package_names": [
          "com.gameloft.android.GAND.GloftLB2P",
          "com.gameloft.android.ALCH.GloftLB2P"
        ]
      },
      {
        "name": "Midnight Pool",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "",
        "size": "N/A",
        "rating": "4.62",
        "descriptions": {
          "short_description":
              "Il gioco di biliardo più realistico, con colpi speciali, una modalità Carriera e partite online!",
          "long_description":
              "Il gioco di biliardo più realistico!\r\nDivertiti e mettiti alla prova col più realistico gioco di biliardo di sempre.\r\nAffronta la nuova modalità Carriera con 100 missioni e 7 boss da battere.\r\nSfida gli amici online e dimostra al mondo intero chi è il migliore.\r\nVisita tanti locali e sfida i giocatori migliori in un'atmosfera anni '80 unica.\r\nColleziona moltissime stecche, ognuna con caratteristiche e stile unici."
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/2305/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/2305/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/2305/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/2305/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2305/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2305/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2305/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/2305/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 2305,
        "html5_shortname": "",
        "version_id": 33196540,
        "package_names": [
          "com.gameloft.android.GAND.GloftMPMP",
          "com.gameloft.android.ALCH.GloftMPMP"
        ]
      },
      {
        "name": "Modern Combat 4: Zero Hour",
        "freemium": false,
        "paymium": false,
        "premium": true,
        "html5": false,
        "age": "",
        "size": "N/A",
        "rating": "2.84",
        "descriptions": {
          "short_description":
              "Salva i leader mondiali da un gruppo di terroristi!\r\n",
          "long_description":
              "Una storia avvincente scritta da sceneggiatori professionisti\r\nDai la caccia al nemico in tutto il mondo, dal Sudafrica a Barcellona\r\nGrande varietà di gioco: eventi rapidi, uccisioni furtive, droni e molto altro!\r\n2 nuove modalità: Sopravvivenza e Infiltrazione\r\nUn mix di sequenze a scorrimento laterale, alla guida di veicoli e da cecchino\r\nScopri 9 livelli esplosivi\r\n"
        },
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1660/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/1660/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/1660/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1660/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1660/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1660/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1660/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1660/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 1660,
        "html5_shortname": "",
        "version_id": 33196544,
        "package_names": [
          "com.gameloft.android.GAND.GloftMC4M",
          "com.gameloft.android.LATAM.BK25",
          "com.gameloft.android.GloftMC4M",
          "com.gameloft.android.LATAM.BS47",
          "com.gameloft.android.GAND.GloftSCRT",
          "com.gameloft.android.ALCH.GloftMC4M",
          "com.gameloft.android.HEP.GloftMZHP"
        ]
      },
      {
        "name": "Motocross : Trial Extreme Mobile Premium",
        "freemium": false,
        "paymium": false,
        "premium": false,
        "html5": false,
        "age": "",
        "size": "N/A",
        "assets": {
          "icon":
              "https://media06-gl-ssl.gameloft.com/products/1268/default/wap/icons/1440/big.jpg",
          "splash":
              "https://media06-gl-ssl.gameloft.com/products/1268/default/wap/banners/1440/main.jpg",
          "recommended":
              "https://media06-gl-ssl.gameloft.com/products/1268/default/wap/banners/625/product.jpg",
          "screenshots": [
            "https://media06-gl-ssl.gameloft.com/products/1268/default/wap/screenshots/full/1440/1.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1268/default/wap/screenshots/full/1440/2.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1268/default/wap/screenshots/full/1440/3.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1268/default/wap/screenshots/full/1440/4.jpg",
            "https://media06-gl-ssl.gameloft.com/products/1268/default/wap/screenshots/full/1440/5.jpg"
          ]
        },
        "product_id": 1268,
        "html5_shortname": "",
        "version_id": 33196548,
        "package_names": [
          "com.gameloft.android.GAND.GloftMOTR",
          "com.gameloft.android.BFLL.GloftMOTR",
          "com.gameloft.android.GloftMOTR"
        ]
      }
    ]
  }
];
